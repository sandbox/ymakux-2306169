<?php

function theme_dcart_dashboard($variables) {
  if ($items = theme('dcart_menu_links', $variables)) {
    $rows = theme('dcart_split_columns', array('items' => $items, 'columns' => $variables['columns']));
    return theme('table', array('rows' => $rows, 'attributes' => array('id' => 'dcart-dashboard')));
  }
  return t('You do not have any administrative items');
}

function theme_dcart_menu_links($variables) {

  $items = array();
  foreach ($variables['links'] as $link) {
    if ($variables['exclude_hidden'] && !empty($link['link']['hidden'])) {
      continue;
    }

    $item = '<div class="dashboard-item">';
    $title = $link['link']['link_title'];
    $path = $link['link']['link_path'];

    $icon_name = str_replace('/', '-', $path) . '.png';
    $icon_path = drupal_get_path('module', 'dcart') . '/images/dashboard';
    $icon = $icon_path . '/' . $icon_name;

    $item .= '<div class="parent">';

    if (file_exists($icon)) {
      $item .= theme('image', array('path' => $icon, 'title' => $title, 'alt' => $title, 'height' => '32px', 'width' => '32px'));
    }

    $item .= '<div class="text">';
    $item .= l($title, $path);

    $description = '';
    if (!empty($link['link']['description'])) {
      $description = $link['link']['description'];
    }
    elseif (!empty($link['link']['options']['attributes']['title'])) {
      $description = $link['link']['options']['attributes']['title'];
    }

    if ($description) {
      $item .= '<div class="description">' . check_plain($description) . '</div>';
    }

    $item .= '</div>';
    $item .= '</div>';

    if ($variables['show_children'] && !empty($link['below'])) {
      $children = array();
      foreach ($link['below'] as $child) {
        if ($variables['exclude_hidden'] && !empty($child['link']['hidden'])) {
          continue;
        }
        $children[] = l($child['link']['link_title'], $child['link']['link_path']);
      }
      if ($children) {
        $item .= theme('item_list', array('items' => $children));
      }
    }

    $item .= '</div>';
    $items[] = $item;
  }
  return $items;
}

function theme_dcart_split_columns($variables) {
  $rows = array();
  if($items = $variables['items']) {
    $columns = $variables['columns'];
  
    $row_indexes = array();
    $num_rows = floor(count($items) / $columns);
    $remainders = count($items) % $columns;
    $row = $col = 0;
  
    foreach ($items as $count => $item) {
      $rows[$row][$col] = $item;
      $row_indexes[$row][$col] = $count;
      $row++;

      if (!$remainders && $row == $num_rows) {
        $row = 0;
        $col++;
      }
      elseif ($remainders && $row == $num_rows + 1) {
        $row = 0;
        $col++;
        $remainders--;
      }
    }
  
    for ($i = 0; $i < count($rows[0]); $i++) {
      if (!isset($rows[count($rows) - 1][$i])) {
        $rows[count($rows) - 1][$i] = '';
      }
    }
  }
  return $rows;
}

function _dcart_sort_attributes($a, $b) {
  $a_sku = empty($a['sku']['#value']);
  $b_sku = empty($b['sku']['#value']);

  if ($a_sku == $b_sku) {
    return 0;
  }
  return ($a_sku < $b_sku) ? -1 : 1;
}

function theme_dcart_widget_exposed($variables) {

  $output = '';
  $element = $variables['element'];
  $node = $element['node']['#value'];
  $existing_properties = !empty($element['attributes'][0]['data']['properties']) ? $element['attributes'][0]['data']['properties'] : array();

  $attribute_field = dcart_attribute_field($node);
  $currency_code = isset($attribute_field[0]['currency_code']) ? $attribute_field[0]['currency_code'] : dcart_default_currency();
  
  $header = array(t('SKU'), t('Price, @currency_code', array('@currency_code' => $currency_code)), t('Combination'));
  uasort($element['attributes'], '_dcart_sort_attributes');
  $attributes = element_children($element['attributes']);
  $attributes_count = count($attributes);
  
  if ($attributes_count > 1) {
    $header[] = t('Active');
  }

  if ($existing_properties) {
    $properties = element_children($existing_properties);
    foreach ($properties as $property_name) {
      $header[] = $element['properties']['#value'][$property_name]['title'];
    }
  }
  
  if (!empty($properties) && isset($node->nid)){
    $header[] = t('Operations');
  }
  
  foreach ($attributes as $key) {
    $rows[$key] = array(
      'data' => array(
        render($element['attributes'][$key]['sku']),
        render($element['attributes'][$key]['amount']),
        render($element['attributes'][$key]['text']),
      ),
    );
    
     if($attributes_count > 1) {
        $rows[$key]['data'][] = render($element['attributes'][$key]['status']);
     }

     $rows[$key]['class'] = ($key === 0) ? array('attribute-main') : array('attribute-additional');
    
    if ($key !== 0 && $element['attributes'][$key]['sku']['#value']
    && empty($element['attributes'][$key]['status']['#value'])) {
        $rows[$key]['class'][] = 'attribute-inactive';
    }
    
    if(!empty($element['attributes'][$key]['status']['#value'])) {
      $rows[$key]['class'][] = 'attribute-active';
    }

    if (!empty($element['attributes'][$key]['data']['properties'])) {
      $properties = $element['attributes'][$key]['data']['properties'];
      foreach (element_children($properties) as $property) {
        $rows[$key]['data'][] = array('data' => render($element['attributes'][$key]['data']['properties'][$property]), 'class' => array('attribute-property'));
      }
    }
    
    if(isset($node->nid)) {
      $sku = $element['attributes'][$key]['sku']['#value'];
      $rows[$key]['data'][] = $sku ? l(t('Properties'), 'node/' . $node->nid . '/attributes/' . $sku, array('query' => array('destination' => $_GET['q']))) : '';    
    }
  }
  
  $main = $rows[0];
  unset($rows[0]);
  array_unshift($rows, $main);

  $output .= render($element['currency_code']);
  $output .= '<br />';
  
  $output .= theme('table', array(
    'rows' => $rows,
    'header' => $header,
    'empty' => t('No attributes'),
    'attributes' => array('id' => 'widget-attributes-exposed')));

  return $output;
}

function theme_dcart_widget_line_items($variables) {
  
  $output = '';
  $total = $variables['element']['total']['total'];
  $recalculate_button = $variables['element']['total']['recalculate'];
  unset($variables['element']['total']);
  
  $rows = array();
  foreach (element_children($variables['element']) as $element) {
    $amount = render($variables['element'][$element]['_amount']);
    $rows[$element] = array(
      'data' => array(
        render($variables['element'][$element]['_type']),
        render($variables['element'][$element]['_info']),
        render($variables['element'][$element]['weight']),
        render($variables['element'][$element]['delete']),
      ),
      'class' => array('draggable', 'line-item-' . $element),
    );
  }
  
  $header = array(t('Type'), t('Information'), t('Weight'), t('Actions'));
  
  $table_id = 'widget-line-items-table';
  $output .= theme('table', array(
    'rows' => $rows,
    'header' => $header,
    'empty' => t('The order has no items'),
    'attributes' => array('id' => $table_id),
  ));

  $output .= '<div class="order-total-wrapper">' . render($recalculate_button) . render($total) . '</div>';
  $output .= drupal_render_children($variables['element']);
  
  drupal_add_library('system', 'ui.autocomplete');
  drupal_add_tabledrag($table_id, 'order', 'sibling', 'line-item-weight');
  return $output;
}

function theme_dcart_sku($variables) {
  return '<span class="dcart-sku">' . $variables['sku'] . '</span>';
}

function theme_dcart_token_list($variables) {

  $output = '';
  $types = (array) $variables['types'];
  $tokens = token_info();

  foreach($types as $type) {
    $token_list = array();
    if (!empty($tokens['tokens'][$type])) {
      foreach (array_keys($tokens['tokens'][$type]) as $token) {
        $token_list[] = '[' . $type . ':' . check_plain($token) . '] ' . '<div class="description">' . filter_xss_admin($tokens['tokens'][$type][$token]['description']) . '</div>';
      }
    }

    $columns = 2;
    $rows = theme('dcart_split_columns', array('items' => $token_list, 'columns' => $columns));
    $caption = $tokens['types'][$type]['name'];
    $output .= theme('table', array(
      'caption' => $caption,
      'rows' => $rows,
      'attributes' => array('class' => array('token-list'))));
  }

  return $output;
}

function theme_dcart_radios_image($variables) {
  
  $element = $variables['element'];
  $keys = array_keys($element['#options']);
  $type = $element['#type'];

  $output = '<div class="image-radios">';

  foreach ($keys as $key) {
    $output .= theme('dcart_radio_image', $element[$key]);
  }

  $output .= '</div>';
  return $output;
}

function theme_dcart_radio_image($variables) {
  
  $element = $variables['element'];

  $output = '<input type="radio" ';
  $output .= 'id="' . $element['#id'] . '" ';
  $output .= 'name="' . $element['#name'] . '" ';
  $output .= 'value="' . $element['#return_value'] . '" ';
  $output .= (check_plain($element['#value']) == $element['#return_value']) ? ' checked="checked" ' : ' ';
  $output .= drupal_attributes($element['#attributes']) . ' />';
  
  
  //fixme: NOT valid, we can't use block html elements inside inline <label>
  if (!is_null($element['#title'])) {
    $output = '<label class="option image" for="' . $element['#id'] . '"><div class="wrapper">' . theme('image_style', array(
      'style_name' => 'thumbnail',
      'path' => $element['#title'])) . '  ' . $output . '</div></label>';
  }
 
  unset($element['#title']);
  return $output;
}

function theme_dcart_radio_option($variables) {
  $output = '<div class="wrapper">';
  
  if ($variables['image']) {
    $output .= '<div class="image">' . theme('image', array('path' => $variables['image'])) . '</div>';
  }

  $output .= '<div class="title">' . $variables['title'] . '</div>';
  if ($variables['description']) {
    $output .= '<div class="description">' . $variables['description'] . '</div>';
  }

  $output .= '</div>';
  return $output;
}


function theme_dcart_token_browser($variables) {
  drupal_add_library('system', 'ui.dialog');
  return l(t('Browse tokens'), 'operations/token-list', array(
    'query' => array('types' => $variables['types']),
    'attributes' => array(
      'class' => array('open-dialog'),
      'title' => t('Available order tokens'),
    )));
}

function theme_dcart_widget_attribute_combination($variables) {
  return implode('/', $variables['combination']['labels']);
}

function theme_dcart_widget_default_attribute_combination($variables) {
  if(!empty($variables['combination'])) {
    $build_combitations = _dcart_build_attributes_combination($variables['combination']);
    $build = reset($build_combitations);
    return theme('dcart_widget_attribute_combination', array('combination' => array('labels' => $build['labels'])));
  }
}

function theme_dcart_attribute_property_combination($variables) {
  return ''; // CODE it !!!
}

function theme_dcart_attribute_colors_formatter($variables) {

  $node = $variables['node'];
  $instance = $variables['instance'];
  $attributes = dcart_attribute_field($node);

  $colors = array($variables['items'][0]['rgb']);
  foreach($attributes as $delta => $attribute) {
    if(!empty($attribute['data']['fields'])) {
      foreach($attribute['data']['fields'] as $field_name => $field_value) {
        if($field_name == $instance['field_name']) {
          $colors[] = '#' . $field_value;
          break;
        }
      }
    }
  }
  
  $output = '<div class="colors">';
  foreach(array_unique($colors) as $color) {
    $output .= '<div class="color" style="height:12px;width:12px;background-color:' . $color . ';"></div>';
  }
  $output .= '</div>';
  return $output;
}

function theme_dcart_address($variables) {
  $countries = country_get_list();
  $address[] = check_plain(variable_get('dcart_address_street', '')) . ', ' . check_plain(variable_get('dcart_address_office', ''));
  $address[] = check_plain(variable_get('dcart_address_city', ''));
  $address[] = check_plain(variable_get('dcart_address_zip', ''));
  $address[] = $countries[variable_get('site_default_country', '')];

  if($phone = variable_get('dcart_address_phone', '')) {
    $address[] = t('Phone: @phone', array('@phone' => $phone));
  }
  if($fax = variable_get('dcart_address_fax', '')) {
    $address[] = t('Fax: @fax', array('@fax' => $fax));
  }
  if(array_filter($address)) {
    return theme('item_list', array('items' => $address));
  }
}


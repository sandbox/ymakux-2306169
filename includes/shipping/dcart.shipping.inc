<?php

function dcart_dcart_shipping_service_info() {
  return array(
    'pickup' => array(
      'title' => t('Pickup'),
      'display_title' => t('Pickup'),
      'callback' => 'dcart_shipping_service_pickup',
    ),
  );
}

function dcart_shipping_service_pickup_rate() {
  $shipping_service = dcart_shipping_services_get('pickup');
  $amount = NULL;
  if(isset($shipping_service['amount'])) {
    $amount = (int) $shipping_service['amount'];
    $currency_code = $shipping_service['currency_code'];
    if($currency_code != dcart_default_currency()) {
      $amount = dcart_currency_convert($amount, $currency_code);
    }
  }

  return $amount;
}

function dcart_shipping_services() {
  $shipping_services = &drupal_static(__FUNCTION__);
  if (!isset($shipping_services)) {
    $shipping_services = module_invoke_all('dcart_shipping_service_info');

    $settings = dcart_shipping_services_settings();
    
    $weight = 0;
    foreach($shipping_services as $service_name => &$service_info) {
      
      if(empty($service_info['name'])) {
        $service_info['name'] = $service_name;
      }
      
      if(!isset($service_info['weight'])) {
        $service_info['weight'] = $weight;
      }
      
      if(!isset($service_info['active'])) {
        $service_info['active'] = TRUE;
      }
      
      if(!empty($settings[$service_name])) {
        $shipping_services[$service_name] = $settings[$service_name] + $shipping_services[$service_name];
      }
      
      $weight += 10;
    }
    
    drupal_alter('dcart_shipping_service_info', $shipping_services);
    uasort($shipping_services, 'drupal_sort_weight');
  }
  return $shipping_services;
}

function dcart_shipping_services_get($service_id = NULL, $active = FALSE) {
  $shipping_services = dcart_shipping_services();
  
  if(isset($service_id)) {
    return isset($shipping_services[$service_id]) ? $shipping_services[$service_id] : array();
  }
  
  foreach($shipping_services as $name => $service) {
    if ($active && empty($service['active'])) {
      unset($shipping_services[$name]);
    }
  }
  return $shipping_services;
}

function dcart_shipping_load($service_id) {
  return dcart_shipping_services_get($service_id);
}

function dcart_shipping_title($shipping_service, $op) {
  switch($op) {
    case 'edit' :
      return t('Edit shipping service @name', array('@name' => $shipping_service['title']));
      break;
  }
}

function dcart_shipping_service_options($service_id = NULL, $active = FALSE) {
  $options = array();
  foreach (dcart_shipping_services_get($service_id, $active) as $id => $service) {
    $options[$id] = $service['title'];
  }
  return $options;
}

function dcart_shipping_service_rate_options($themed = TRUE) {
  $options = array();
  foreach(dcart_shipping_services_get(NULL, TRUE) as $service_id => $service_info) {
    if ($rate_callback = dcart_hook_callback($service_info, 'rate')) {
      $amount = (int) $rate_callback();
      $title = !empty($service_info['display_title']) ? check_plain($service_info['display_title']) : check_plain($service_info['title']);
      $options[$service_id] = $title;
        
      if ($themed) {
        $options[$service_id] = theme('dcart_radio_option', array(
          'title' => $title,
          'price' => theme('dcart_price', array('amount' => $amount)),
          'image' => !empty($service_info['image']) ? $service_info['image'] : '',
          'description' => !empty($service_info['description']) ? $service_info['description'] : '',
        ));
      }
    }
  }
  drupal_alter('dcart_checkout_shipping_service_options', $options);
  return $options;
}

function dcart_shipping_services_settings($service_name = NULL) {
  $return = array();
  $query = db_select('dcart_shipping_settings', 's')->fields('s');
  if(isset($service_name)) {
    $query->condition('name', $service_name);
  }
  if($results = $query->execute()->fetchAll()) {
    foreach($results as $data) {
      $return[$data->name] = (array) $data;
      $return[$data->name]['settings'] = unserialize($return[$data->name]['settings']);
    }
    if(isset($service_name)) {
      $return = reset($return);
    }
  }
  return $return;
}

function dcart_shipping_service_settings_save($service_name, $settings) {
  
  $settings += array(
    'active' => 1,
    'amount' => 0,
    'currency_code' => dcart_default_currency(),
    'settings' => array(),
    'weight' => 0,
  );
  
  $settings['settings'] = serialize($settings['settings']);
  
  return db_merge('dcart_shipping_settings')
    ->key(array('name' => $service_name))
    ->fields($settings)
    ->execute();
}

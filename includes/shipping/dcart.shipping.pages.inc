<?php

function dcart_shipping_form($form, $form_state) {
  
  $form['#tree'] = TRUE;
  foreach(dcart_shipping_services() as $service_id => $service_info) {
    if (empty($service_info['callback'])) {
      continue;
    }
    $settings = dcart_shipping_services_settings($service_id);

    $form['shipping_services'][$service_id]['weight'] = array(
      '#type' => 'weight',
      '#delta' => 10,
      '#default_value' => isset($settings['weight']) ? $settings['weight'] : 0,
      '#attributes' => array('class' => array('shipping-service-weight')),
    );
    $form['shipping_services'][$service_id]['active'] = array(
      '#type' => 'checkbox',
      '#default_value' => isset($settings['active']) ? $settings['active'] : $service_info['active'],
    );
    $form['shipping_services'][$service_id]['settings'] = array(        
       '#type' => 'link',
       '#title' => t('Settings'),
       '#href' => 'admin/dcart/settings/shipping/' . $service_id . '/settings',
       '#options' => array('query' => array('destination' => $_GET['q'])),
    );
  }
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#access' => !empty($form['shipping_services']),
  );
  return $form;
}

function dcart_shipping_form_submit($form, $form_state) {
  $settings = dcart_shipping_services_settings();
  foreach($form_state['values']['shipping_services'] as $service_name => $service_data) {
    $existing_settings = !empty($settings[$service_name]) ? $settings[$service_name] : array();
    $save = $service_data + $existing_settings;
    dcart_shipping_service_settings_save($service_name, $save);
  }
  drupal_set_message(t('The configuration options have been saved.'));
}

function theme_dcart_shipping_form($variables) {

  $form = $variables['form'];
  $shipping_services = dcart_shipping_services();
  $rows = array();
  foreach (element_children($form['shipping_services']) as $service_id) {
    $rows[$service_id] = array(
      'data' => array(
        check_plain($shipping_services[$service_id]['title']),
        render($form['shipping_services'][$service_id]['active']),
        render($form['shipping_services'][$service_id]['weight']),
        render($form['shipping_services'][$service_id]['settings']),
      ),
      'class' => array('draggable'),
    );
  }
  
  $header = array(t('Shipping service'), t('Active'), t('Weight'), t('Actions'));
  
  $table_id = 'shipping-services-table';
  $output = theme('table', array(
    'rows' => $rows,
    'header' => $header,
    'empty' => t('No shipping services'),
    'attributes' => array('id' => $table_id),
  )) . drupal_render_children($form);
  
  drupal_add_tabledrag($table_id, 'order', 'sibling', 'shipping-service-weight');
  return $output;
}


function dcart_shipping_service_form($form, $form_state, $shipping_service) {

  $form_state['shipping_service'] = $shipping_service;
  
  $form['active'] = array(
    '#title' => t('Active'),
    '#type' => 'checkbox',
    '#default_value' => isset($shipping_service['active']) ? $shipping_service['active'] : $shipping_service['active'],
  );
  
  $form['amount'] = array(
    '#title' => t('Amount'),
    '#type' => 'textfield',
    '#size' => 4,
    '#default_value' => isset($shipping_service['amount']) ? $shipping_service['amount'] : '',
    '#element_validate' => array('element_validate_number'),
    '#pre_render' => array('dcart_element_amount_to_decimal'),
  );
  
  $form['currency_code'] = array(
    '#title' => t('Currency'),
    '#type' => 'select',
    '#options' => dcart_currency_options(TRUE),
    '#default_value' => isset($shipping_service['currency_code']) ? $shipping_service['currency_code'] : dcart_default_currency(),
  );

  $form['settings'] = array(
    '#type' => 'container',
    '#weight' => 99,
    '#tree' => TRUE,
  );

  if ($settings_form_callback = dcart_hook_callback($shipping_service, 'settings_form')) {
    if($settings_elements = $settings_form_callback($form, $form_state)) {
      $form['settings'] += $settings_elements;
    }
  }
  
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#submit' => array('dcart_shipping_service_form_submit'),
    '#validate' => array('dcart_shipping_service_form_validate'),
  );
  
  return $form;
}

function dcart_shipping_service_form_validate($form, &$form_state) {
  form_state_values_clean($form_state);
  if ($validate_callback = dcart_hook_callback($form_state['shipping_service'], 'settings_form_validate')) {
    $validate_callback($form, $form_state);
  }
  
  $amount = $form_state['values']['amount'];
  $form_state['values']['amount'] = dcart_price_decimal_to_amount($amount);
}

function dcart_shipping_service_form_submit($form, &$form_state) {
  $service_name = $form_state['shipping_service']['name'];
  if ($submit_callback = dcart_hook_callback($form_state['shipping_service'], 'settings_form_submit')) {
    $submit_callback($form, $form_state);
  }
  dcart_shipping_service_settings_save($service_name, $form_state['values']);
  drupal_set_message(t('The configuration options have been saved.'));
}


<?php

function dcart_customer_orders($customer) {
  return theme('dcart_customer_orders', array('user' => $customer));
}

function theme_dcart_customer_orders($variables) {
  
  $output = t('You have no orderds');
  $user = $variables['user'];
  $limit = $variables['limit'];
  $pager = $variables['pager'];

  $rows = array();
  
  $statuses = array();
  if(variable_get('dcart_customer_hide_incomplete_orders', 1)) {
    $statuses = array_keys(dcart_order_statuses_get(FALSE));
  }
  
  if ($orders = dcart_customer_orders_get($user->uid, TRUE, $pager, $limit, $statuses)) {
    
    $order_statuses = dcart_order_statuses();
    foreach($orders as $order_id => $order) {
    
      $order_label = t('Order #@order_id, @created', array(
        '@order_id' => $order->order_id,
        '@created' => format_date($order->created, 'custom', 'd/m/Y'),
      ));
      
      if(dcart_order_access('view', $order)) {
        $order_label = l($order_label, 'user/' . $user->uid . '/orders/' . $order_id . '/view') 
        . ' ' . theme('mark', array('type' => dcart_order_mark($order_id, $order->changed)));
      }
      
      $rows[] = array(
        $order_label,
        !empty($order_statuses[$order->status]['display_title']) ? check_plain($order_statuses[$order->status]['display_title']) : check_plain($order_statuses[$order->status]['title']),
        theme('dcart_price', array('amount' => $order->total)),
      );
    }
  }

  if($rows) {
    $header = array('#', t('Status'), t('Total'));
    $output = theme('table', array('rows' => $rows, 'header' => $header, 'sticky' => FALSE, 'attributes' => array('class' => array('customer-orders'))));
  
    if($pager) {
      $output .= theme('pager');
    }
    else {
      $output .= l(t('View all'), 'user/' . $user->uid . '/orders', array('class' => array('view-all')));
    }
  }
  return $output;
}


function dcart_customer_overview_form($form, $form_state) {
  $form['#tree'] = TRUE;
  $parameters = drupal_get_query_parameters();

  if($parameters) {
    $_SESSION['dcart_customer_search'] = $parameters;
  }
  
  if(!empty($_SESSION['dcart_customer_search']) && array_diff($_SESSION['dcart_customer_search'], $parameters)) {
    drupal_goto(current_path(), array('query' => $_SESSION['dcart_customer_search']));
  }
  
  $form['#attached']['library'][] = array('system', 'ui.datepicker');
  $form['#attached']['css'][] = array(
    'data' => 'http://code.jquery.com/ui/1.9.1/themes/smoothness/jquery-ui.css',
    'type' => 'external',
  );
  
  $form['search'] = array(
    '#type' => 'fieldset',
    '#theme_wrappers' => array('dcart_customer_search_form'),
    '#weight' => -99,
  );
  
  $form['search']['name'] = array(
    '#type' => 'textfield',
    '#default_value' => isset($parameters['search']['name']) ? $parameters['search']['name'] : '',
    '#size' => 20,
    '#autocomplete_path' => 'user/autocomplete',
  );
  
  $form['search']['mail'] = array(
    '#type' => 'textfield',
    '#default_value' => isset($parameters['search']['mail']) ? $parameters['search']['mail'] : '',
    '#size' => 10,
  );
  
  $form['search']['created_from'] = array(
    '#type' => 'textfield',
    '#default_value' => !empty($parameters['search']['created_from']) ? date('m\/d\/Y', $parameters['search']['created_from']) : '',
    '#attributes' => array('class' => array('datepicker-min')),
    '#size' => 4,
  );
  
  $form['search']['created_to'] = array(
    '#type' => 'textfield',
    '#default_value' => !empty($parameters['search']['created_to']) ? date('m\/d\/Y', $parameters['search']['created_to']) : '',
    '#attributes' => array('class' => array('datepicker-max')),
    '#size' => 4,
  );
  
  $form['search']['access_from'] = array(
    '#type' => 'textfield',
    '#default_value' => '',
    '#default_value' => !empty($parameters['search']['access_from']) ? date('m\/d\/Y', $parameters['search']['access_from']) : '',
    '#attributes' => array('class' => array('datepicker-min')),
    '#size' => 4,
  );
  
  $form['search']['access_to'] = array(
    '#type' => 'textfield',
    '#default_value' => !empty($parameters['search']['access_to']) ? date('m\/d\/Y', $parameters['search']['access_to']) : '',
    '#attributes' => array('class' => array('datepicker-max')),
    '#size' => 4,
  );

  $form['search']['status'] = array(
    '#type' => 'checkbox',
    '#default_value' => isset($parameters['search']['status']) ? $parameters['search']['status'] : 1,
  );
  
  $form['search']['search'] = array(
    '#type' => 'submit',
    '#value' => t('Search'),
    '#submit' => array('dcart_customer_search_submit'),
    '#validate' => array('dcart_customer_search_validate'),
    '#limit_validation_errors' => array(array('search')),
    '#parents' => array(),
  );
  
  $form['search']['reset'] = array(
    '#type' => 'submit',
    '#value' => t('Reset'),
    '#validate' => array(),
    '#submit' => array('dcart_customer_search_reset_submit'),
    '#limit_validation_errors' => array(array('search')),
    '#parents' => array(),
    '#access' => isset($parameters['search']),
  );
  
  $header = array(
    'name' => array(
      'data' => t('Name'),
      'specifier' => 'name',
      'type' => 'property',
    ),
    'mail' => array(
      'data' => t('E-mail'),
      'specifier' => 'mail',
      'type' => 'property',
    ),
    'status' => array(
      'data' => t('Status'),
      'specifier' => 'status',
      'type' => 'property',
    ),
    'created' => array(
      'data' => t('Registered'),
      'specifier' => 'created',
      'type' => 'property',
      'sort' => 'desc',
    ),
    'access' => array(
      'data' => t('Last access'),
      'specifier' => 'access',
      'type' => 'property',
    ),
    'operations' => t('Operations'),
  );

  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'user');
  
  if (isset($parameters['search']['name'])) {
    $query->propertyCondition('name', rawurldecode($parameters['search']['name']), 'CONTAINS');
  }
  
  if (isset($parameters['search']['mail'])) {
    $query->propertyCondition('mail', rawurldecode($parameters['search']['mail']), 'CONTAINS');
  }
  
  if (isset($parameters['search']['status'])){
    $query->propertyCondition('status', $parameters['search']['status']);
  }
  
  
  if (isset($parameters['search']['created_from']) && isset($parameters['search']['created_to'])) {
    $range = array((int) $parameters['search']['created_from'], (int) $parameters['search']['created_to']);
    $query->propertyCondition('created', $range, 'BETWEEN');
  }
  elseif (isset($parameters['search']['created_from']) && !isset($parameters['search']['created_to'])) {
    $query->propertyCondition('created', (int) $parameters['search']['created_from'], '>=');
  }
  elseif (!isset($parameters['search']['created_from']) && isset($parameters['search']['created_to'])) {
    $query->propertyCondition('created', (int) $parameters['search']['created_to'], '<=');
  }
  
  if (isset($parameters['search']['access_from']) && isset($parameters['search']['access_to'])) {
    $range = array((int) $parameters['search']['access_from'], (int) $parameters['search']['access_to']);
    $query->propertyCondition('created', $range, 'BETWEEN');
  }
  elseif (isset($parameters['search']['access_from']) && !isset($parameters['search']['access_to'])) {
    $query->propertyCondition('created', (int) $parameters['search']['access_from'], '>=');
  }
  elseif (!isset($parameters['search']['access_from']) && isset($parameters['search']['access_to'])) {
    $query->propertyCondition('created', (int) $parameters['search']['access_to'], '<=');
  }

  $query->pager(variable_get('dcart_admin_customers_limit', 10));
  $query->tableSort($header);
  $result = $query->execute();

  if (!empty($result['user'])) {
    $ids = array_keys($result['user']);
    $users = entity_load('user', $ids);
  }
  
  $options = array();
  if(!empty($users)) {
    foreach($users as $user) {
      if($user->uid <= 1) {
        continue;
      }
      $links = array();
      if(user_access('access user profiles')) {
      $links[] = array(
          'title' => t('View'),
          'href' => 'user/' . $user->uid,
          'query' => array('dialog' => TRUE),
          'attributes' => array('class' => array('user-view-dialog'), 'title' => check_plain($user->name)),
        );
      }
        
      if(user_access('administer users')) {
        $links[] = array(
          'title' => t('Edit'),
          'href' => 'user/' . $user->uid . '/edit',
          'query' => array('destination' => 'admin/dcart/users'),
        );
      
        $links[] = array(
          'title' => t('Cancel'),
          'href' => 'user/' . $user->uid . '/cancel',
          'query' => array('destination' => 'admin/dcart/users'),
        );
      }

      if((user_access('dcart administer orders') || user_access('dcart view any order')) && dcart_customer_orders_get($user->uid)) {
        $links[] = array(
          'title' => t('Orders'),
          'href' => 'user/' . $user->uid . '/orders',
          'query' => array('destination' => 'admin/dcart/users'),
        );
      } 
      
      $options[$user->uid] = array(
        'name' => l($user->name, 'user/' . $user->uid),
        'mail' => check_plain($user->mail),
        'status' => $user->status ? t('active') : t('blocked'),
        'created' => format_date($user->created, 'custom', 'Y-m-d H:i:s'),
        'access' => format_date($user->access, 'custom', 'Y-m-d H:i:s'),
        'operations' => theme('links', array('links' => $links, 'attributes' => array('class' => array('links', 'inline')))),
      );
    }
  }
  
  $form['items'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $options,
    '#empty' => t('No customers to display'),
    '#suffix' => theme('pager'),
    '#weight' => -50,
    '#attached' => array(
      'library' => array(
        array('system', 'ui.dialog'),
      ),
    ),
  );
  return $form;
}

function dcart_customer_search_validate($form, &$form_state) {
  $form_state['values']['search'] = array_filter(array_map('trim', $form_state['values']['search']));

  if (isset($form_state['values']['search']['created_from']) && $created_from = strtotime($form_state['values']['search']['created_from'])) {
    $form_state['values']['search']['created_from'] = $created_from;
  }
  
  if (isset($form_state['values']['search']['created_to']) && $created_to = strtotime($form_state['values']['search']['created_to'])) {
    $form_state['values']['search']['created_to'] = $created_to;
  }

  if (isset($form_state['values']['search']['access_from']) && $created_from = strtotime($form_state['values']['search']['access_from'])) {
    $form_state['values']['search']['access_from'] = $created_from;
  }
  
  if (isset($form_state['values']['search']['access_to']) && $created_to = strtotime($form_state['values']['search']['access_to'])) {
    $form_state['values']['search']['access_to'] = $created_to;
  }
  
  if(isset($form_state['values']['search']['mail'])) {
    $form_state['values']['search']['mail'] = rawurlencode($form_state['values']['search']['mail']);
  }
  
  if(isset($form_state['values']['search']['name'])) {
    $form_state['values']['search']['name'] = rawurlencode($form_state['values']['search']['name']);
  }

  $form_state['values']['search']['status'] = !empty($form_state['values']['search']['status']);
}

function dcart_customer_search_submit($form, &$form_state) {
  $query = drupal_get_query_parameters();
  if (!empty($form_state['values']['search'])) {
    $query  = array_replace($query, array('search' => $form_state['values']['search']));
  }
  $_SESSION['dcart_customer_search'] = $query;
  $form_state['redirect'] = array(current_path(), array('query' => array($query)));
}
  
function dcart_customer_search_reset_submit($form, &$form_state) {
  unset($_SESSION['dcart_customer_search']);
  $form_state['redirect'] = current_path();
}

function theme_dcart_customer_search_form($variables) {
  $element = $variables['element'];

  $rows[] = array(
    render($element['name']),
    render($element['mail']),
    array(
      'data' => '<span>' . t('from') . '</span> '  . render($element['created_from']) . '<span>' . t('to') . '</span>' . render($element['created_to']),
      'class' => array('fields-inline'),
    ),
    array(
      'data' => '<span>' . t('from') . '</span> '  . render($element['access_from']) . '<span>' . t('to') . '</span>' . render($element['access_to']),
      'class' => array('fields-inline'),
    ),
    render($element['status']),
    render($element['search']) . render($element['reset']),
  );

  $header = array(
    t('Username'),
    t('E-mail'),
    t('Created'),
    t('Last access'),
    t('Status'),
    '',
  );

  $output = theme('table', array('rows' => $rows, 'header' => $header));
  return $output;
}

function dcart_customer_settings_form($form, $form_state) {

  $form['dcart_customer_hide_incomplete_orders'] = array(
    '#title' => t('Hide incomplete orders'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('dcart_customer_hide_incomplete_orders', 1),
    '#description' => t('Do not show to customer orders which are still in checkout.'),
  );
  return system_settings_form($form);

}


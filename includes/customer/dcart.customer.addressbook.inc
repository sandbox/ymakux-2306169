<?php

function dcart_customer_addresses($customer) {
  if(isset($_GET['action'])) {
    switch($_GET['action']) {
      case 'edit':
      case 'add':
        $record_id = isset($_GET['id']) ? intval($_GET['id']) : FALSE;
        return drupal_get_form('dcart_customer_address_edit_form', $customer, $record_id);
        break;
      case 'delete':
        dcart_customer_addressbook_record_delete(intval($_GET['id']));
        drupal_goto(current_path());
        break;
      case 'default':
        dcart_customer_addressbook_record_set_default(intval($_GET['id']));
        drupal_goto(current_path());
        break;
    }
  }
  return theme('dcart_customer_addresses', array('user' => $customer));
}

function dcart_customer_address_edit_form($form, $form_state, $customer, $record_id) {

  $record = dcart_customer_addressbook_record_new();
  if($record_id) {
    $record = dcart_customer_addressbook_record_get($customer->uid, NULL, (int) $record_id);
  }

  $form_state['addressbook_record'] = $record;

  $order = new stdClass();
  $form_state['checkout_fields'] = dcart_order_checkout_fields(TRUE);
  foreach($form_state['checkout_fields'] as $field_name => $instance) {
    field_attach_form('dcart_order', $order, $form, $form_state, NULL, array('field_name' => $field_name));
    $default_value = isset($record['data']['address'][$field_name]) ? $record['data']['address'][$field_name] : '';
    $form[$field_name][LANGUAGE_NONE][0]['value']['#default_value'] = check_plain($default_value);
  }

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#validate' => array('dcart_customer_address_edit_form_validate'),
    '#submit' => array('dcart_customer_address_edit_form_submit'),
  );
  
  $form['actions']['cancel'] = array(
    '#type' => 'link',
    '#title' => t('Cancel'),
    '#href' => 'user/' . $customer->uid . '/addresses',
  );
  
  return $form;
}

function dcart_customer_address_edit_form_validate($form, &$form_state) {
  foreach($form_state['checkout_fields'] as $field_name => $instance) {
    $order = new stdClass();
    field_attach_form_validate('dcart_order', $order, $form, $form_state, array('field_name' => $field_name));
  }
  _dcart_order_fields_extra_validation($form_state['checkout_fields'], $form, $form_state);
}

function dcart_customer_address_edit_form_submit($form, &$form_state) {
  
  $address = array();
  foreach($form_state['checkout_fields'] as $field_name => $instance) {
    if(isset($form_state['values'][$field_name][LANGUAGE_NONE][0]['value'])) {
      $address[$field_name] = $form_state['values'][$field_name][LANGUAGE_NONE][0]['value'];
    }
  }

  $record = $form_state['addressbook_record'];
  $record['data']['address'] = $address;

  if(!dcart_customer_addressbook_record_get($record['uid'], DCART_ADDRESSBOOK_DEFAULT)) {
    $record['status'] = DCART_ADDRESSBOOK_DEFAULT;
  }

  dcart_customer_addressbook_record_save($record);
  $form_state['redirect'] = $_GET['q'];
}

function theme_dcart_customer_addresses($variables) {
  $output = t('You have no saved addresses');
  if($records = dcart_customer_addressbook_record_get($variables['user']->uid)) {

    $default = reset($records);
    $key = key($records);
    unset($records[$key]);

    $default_address = theme('dcart_customer_address', array('address' => $default));
    
    if($default_address) {
      $output = '<div class="address-record default">' . $default_address . '</div>';
      if($records) {
        foreach($records as $record) {
          $output .= '<div class="address-record">' . theme('dcart_customer_address', array('address' => $record)) . '</div>';
        }
      }
    }
  }
  $output .= '<div class="add-address clearfix">' . l(t('Add address'), $_GET['q'], array(
    'query' => array('action' => 'add'))) . '</div>';
  return $output;
}

function theme_dcart_customer_address($variables) {
  $output = '';
  $address = $variables['address'];
  if(!empty($address['data']['address'])) {
    $instances = field_info_instances('dcart_order', 'dcart_order');
    $rows = array();
    foreach($address['data']['address'] as $field_name => $field_value) {
      if(isset($instances[$field_name])) {
        $rows[] = array(
          array(
            'data' => check_plain($instances[$field_name]['label']),
            'class' => array('label'),
          ),
          array(
            'data' => check_plain($field_value),
            'class' => array('value'),
          ),
        );
      }
    }
  
    $links[] = array(
      'title' => '<i class="fa fa-pencil-square"></i><span>' . t('Edit') . '</span>',
      'href' => $_GET['q'],
      'query' => array('action' => 'edit', 'id' => $address['id']),
      'attributes' => array('title' => t('Edit'), 'class' => array('edit')),
      'html' => TRUE,
    );
  
    $links[] = array(
      'title' => '<i class="fa fa-trash-o"></i><span>' . t('Delete') . '</span>',
      'href' => $_GET['q'],
      'query' => array('action' => 'delete', 'id' => $address['id']),
      'attributes' => array('title' => t('Delete. No confirmation!'), 'class' => array('delete')),
      'html' => TRUE,
    );
  
    if($address['status'] != DCART_ADDRESSBOOK_DEFAULT) {
      $links[] = array(
        'title' => '<i class="fa fa-star-o"></i><span>' . t('Set default') . '</span>',
        'href' => $_GET['q'],
        'query' => array('action' => 'default', 'id' => $address['id']),
        'attributes' => array('title' => t('Make this address default, i.e primary'), 'class' => array('set-default')),
        'html' => TRUE,
      );
    }
    else {
      $links[] = array(
        'title' => '<i class="fa fa-star"></i><span>' . t('Is default address') . '</span>',
        'href' => $_GET['q'],
        'fragment' => ' ',
        'attributes' => array('title' => t('This is your default address'), 'class' => array('is-default')),
        'html' => TRUE,
      );
    }

    $output = '<div class="links">' . theme('links', array(
      'links' => $links,
      'attributes' => array('class' => array('links', 'inline')),
    )) . '</div>';
    
    $output .= '<div class="content">' . theme('table', array('rows' => $rows)) . '</div>';
    
  }
  return $output;
}

function theme_dcart_customer_address_string($variables) {

  $form = array();
  $form_state = array();
  $order = new stdClass();
  field_attach_form('dcart_order', $order, $form, $form_state);
  uasort($form, 'element_sort');
  $address = $variables['address'];
  
  $address_sorted = array();
  foreach(element_children($form) as $element) {
    if(!empty($address['data']['address'][$element])) {
      $address_sorted[$element] = '<span class="' . $element . '">' . check_plain($address['data']['address'][$element]) . '</span>';
    }
  }
  return implode('<span class="sep">,</span>', $address_sorted);
}


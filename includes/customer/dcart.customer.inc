<?php

function dcart_customer_get($cookie_name = 'dcart_cart') {

  global $user;
  if ($user->uid) {
    $customer = $user->uid;
  }
  elseif($cookie = dcart_cookie_get($cookie_name)) {
    $customer = $cookie;
  }
  else {
    $customer = NULL;
  }
  return $customer;
}

function dcart_customer_orders_get($uid, $load = FALSE, $pager = FALSE, $limit = NULL, $status = array()) {
  $orders = array();

  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'dcart_order');
  $query->propertyCondition('uid', $uid);
  
  if(isset($limit)) {
    $query->range(0, (int) $limit);
  }

  if($pager) {
    $query->pager(variable_get('dcart_customer_orders_limit', 5));
  }
  
  if($status) {
    $query->propertyCondition('status', (array) $status, 'IN');
  }
  
  $query->propertyOrderBy('order_id', 'DESC');

  $result = $query->execute();
  if (!empty($result['dcart_order'])) {
    $orders = array_keys($result['dcart_order']);
    if($load) {
      $orders = entity_load('dcart_order', $orders);
    }
  }
  return $orders;
}

function dcart_user_logout($account) {
  dcart_checkout_cookie_clear();
}

function dcart_user_login(&$edit, $account) {

  $new_uid = $account->uid;
  dcart_cart_clear_cache($new_uid);
  if($old_uid = dcart_cookie_get('dcart_cart')) {
    if($anonymous_cart_items = dcart_cart_items_get($old_uid)) {
       dcart_cart_update(array_keys($anonymous_cart_items), array('uid' => $new_uid));
       dcart_cart_clear_cache($old_uid);
    }
  }
  
  $order_id = dcart_cookie_get_order_id();
  dcart_checkout_cookie_clear();

  if(isset($_GET['order_id'])) {
    $order_id = (int) $_GET['order_id'];
  }

  if(!empty($order_id) && ($order = dcart_order_load($order_id))) {
    $order->uid = $new_uid;
    dcart_order_save($order);
    dcart_checkout_cookie_set($order_id);
  }
  
  // Make sure we're staying on the current page
  $edit['redirect'] = isset($_GET['destination']) ? $_GET['destination'] : current_path();
}

function dcart_customer_addressbook_record_new() {
  return array(
    'id' => NULL,
    'addressbook_id' => 0,
    'status' => DCART_ADDRESSBOOK_ACTIVE,
    'data' => array(),
    'created' => REQUEST_TIME,
    'uid' => intval(dcart_customer_get()),
  );
}

function dcart_customer_addressbook_record_save($record = array()) {
  
  $record += dcart_customer_addressbook_record_new();
  foreach (module_implements('dcart_addressbook_record_presave') as $module) {
    $function = $module . '_dcart_addressbook_record_presave';
    $function($record);
  }

  $record['data'] = serialize($record['data']);
  $query = db_merge('dcart_addressbook')
    ->key(array('id' => $record['id']))
    ->fields($record)
    ->execute();

  dcart_customer_addressbook_rehash($record['uid']);
  return $query;
}

function dcart_customer_addressbook_rehash($uid) {
  if ($max_records = variable_get('dcart_addressbook_max_records', 5)) {
    $max_records -= 1; // Minus 1 because we presave default record
    $existing_records = db_select('dcart_addressbook', 'a')
      ->fields('a')
      ->condition('a.uid', $uid)
      ->condition('status', DCART_ADDRESSBOOK_DEFAULT, '<>')
      ->orderBy('created', 'ASC')
      ->execute()
      ->fetchAllAssoc('id');

    $difference = count($existing_records) - $max_records;

    if ($difference > 0) {
      $ids = array_keys($existing_records);
      array_splice($ids, $difference);
      dcart_customer_addressbook_record_delete($ids);
    }
     
    if($existing_records) {
      foreach($existing_records as &$record) {
       if(isset($ids) && in_array($record->id, $ids)) {
         unset($existing_records[$record->id]);
       }
         
        $record = (array) $record;
        $record['data'] = unserialize($record['data']);
      }
    }
     return $existing_records;
  }
}

function dcart_customer_addressbook_record_get($uid = NULL, $status = NULL, $id = NULL, $addressbook = 0) {
  
  $return = array();
  $query = db_select('dcart_addressbook', 'a')->fields('a');
  
  if(isset($uid)) $query->condition('uid', $uid);
  if(isset($status)) $query->condition('status', $status);
  if(isset($id)) $query->condition('id', $id);
  $query->condition('addressbook_id', $addressbook);
  
  $query->orderBy('status', 'DESC');
  $query->orderBy('created', 'ASC');
  
  if($result = $query->execute()->fetchAll()) {
    foreach($result as $data) {
      $return[$data->id] = (array) $data;
      $return[$data->id]['data'] = unserialize($return[$data->id]['data']);
    }
  
    if ($status == DCART_ADDRESSBOOK_DEFAULT || isset($id)) {
      return reset($return);
    }
  }
  return $return;
}

function dcart_customer_addressbook_address_get($record_id) {
  $record = dcart_customer_addressbook_record_get(NULL, NULL, $record_id);
  return isset($record['data']['address']) ? (array) $record['data']['address'] : array();
}

function dcart_customer_addressbook_record_delete($record_id) {
  
   foreach (module_implements('dcart_addressbook_record_delete') as $module) {
     $function = $module . '_dcart_addressbook_record_delete';
     $function($record_id);
   }
  
  global $user;
  db_delete('dcart_addressbook')->condition('id', (array) $record_id, 'IN')->execute();
  
  if($remaining = dcart_customer_addressbook_record_get($user->uid)) {
    $default = reset($remaining);
    dcart_customer_addressbook_record_set_default($default['id']);
  }
}

function dcart_customer_addressbook_record_set_default($record_id) {
  
   foreach (module_implements('dcart_addressbook_record_default') as $module) {
     $function = $module . '_dcart_addressbook_record_default';
     $function($record_id);
   }
  
  if($record = dcart_customer_addressbook_record_get(NULL, NULL, $record_id)) {

    $record['status'] = DCART_ADDRESSBOOK_DEFAULT;
    dcart_customer_addressbook_record_save($record);
    
    $records = dcart_customer_addressbook_record_get($record['uid']);
    unset($records[$record_id]);
    
    if($records) {
      db_update('dcart_addressbook')
        ->fields(array('status' => DCART_ADDRESSBOOK_ACTIVE))
        ->condition('id', array_keys($records), 'IN')
        ->execute();
    }
  }
}

function dcart_customer_addressbook_options($uid) {
  $options = array();
  if ($addresses = dcart_customer_addressbook_record_get($uid)) {
    foreach($addresses as $address) {
      $is_default = ($address['status'] == DCART_ADDRESSBOOK_DEFAULT);
      $options[$address['id']] = theme('dcart_customer_address_string', array(
        'address' => $address, 'default' => $is_default));
    }
  }
  drupal_alter('dcart_addressbook_options', $options, $uid, $addresses);
  return $options;
}

function dcart_customer_addressbook_record_from_order($order) {
  if($address_fields = dcart_order_checkout_fields_get($order, TRUE)) {
     $record['data']['address'] = $address_fields;
     $default_record = dcart_customer_addressbook_record_get($order->uid, DCART_ADDRESSBOOK_DEFAULT);
     $record['status'] = $default_record ? DCART_ADDRESSBOOK_ACTIVE : DCART_ADDRESSBOOK_DEFAULT;
     dcart_customer_addressbook_record_save($record);
  }
}

function dcart_customer_can_create_account() {
  return (variable_get('user_register', USER_REGISTER_VISITORS_ADMINISTRATIVE_APPROVAL) == USER_REGISTER_VISITORS);
}

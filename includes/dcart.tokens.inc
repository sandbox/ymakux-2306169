<?php

/**
 * Implements hook_token_info().
 */
function dcart_token_info() {

  $info['types']['dcart-order'] = array(
    'name' => t('Dcart order'),
    'description' => t('Tokens for the Dcart order'),
    'needs-data' => 'dcart_order',
  );
  
  $info['types']['dcart'] = array(
    'name' => t('Dcart'),
    'description' => t('Common tokens for Dcart'),
  );
  
  $info['tokens']['dcart']['cart-url'] = array(
    'name' => t('Cart URL'),
    'description' => t('Absolute URL of shopping cart'),
    'type' => 'dcart',
  );
  
  $info['tokens']['dcart']['checkout-url'] = array(
    'name' => t('Checkout URL'),
    'description' => t('Absolute URL of checkout page'),
    'type' => 'dcart',
  );
  
  $info['tokens']['dcart']['wishlist-url'] = array(
    'name' => t('Wishlist URL'),
    'description' => t('Absolute URL of wishlist form'),
    'type' => 'dcart',
  );
  
  $info['tokens']['dcart-order']['content'] = array(
    'name' => t('Dcart order content'),
    'description' => t('Content of order in table including products, line items, shipping address...'),
    'type' => 'dcart_order',
  );
  
  $info['tokens']['dcart-order']['content-plain'] = array(
    'name' => t('Dcart order content (plain text)'),
    'description' => t('Content of order in plain text, line items, shipping address... Good for email'),
    'type' => 'dcart_order',
  );

  $info['tokens']['dcart-order']['id'] = array(
    'name' => t('Order ID'),
    'description' => t('Number of Dcart order'),
    'type' => 'dcart_order',
  );
  
  $info['tokens']['dcart-order']['status'] = array(
    'name' => t('Order status'),
    'description' => t('Status of order'),
    'type' => 'dcart_order',
  );
  
  $info['tokens']['dcart-order']['status-url'] = array(
    'name' => t('Order status URL'),
    'description' => t('URL with unique ID to check order status for anonymous'),
    'type' => 'dcart_order',
  );
  
  $info['tokens']['dcart-order']['admin-view-url'] = array(
    'name' => t('Admin view order URL'),
    'description' => t('URL to admin view of order (orders/ORDER_ID)'),
    'type' => 'dcart_order',
  );
  
  $info['tokens']['dcart-order']['admin-edit-url'] = array(
    'name' => t('Admin edit order URL'),
    'description' => t('URL to order edit form (orders/ORDER_ID/edit)'),
    'type' => 'dcart_order',
  );
  
  $info['tokens']['dcart-order']['customer-view-url'] = array(
    'name' => t('Customer order URL'),
    'description' => t('URL of customer\'s order (user/USER_ID/orders/ORDER_ID)'),
    'type' => 'dcart_order',
  );
  
  $info['tokens']['dcart-order']['created'] = array(
    'name' => t('Order creation time'),
    'description' => t('When order was created'),
    'type' => 'dcart_order',
  );

  $info['tokens']['dcart-order']['amount-formatted'] = array(
    'name' => t('Order amount formatted'),
    'description' => t('Total order amount with decimal separator and currency symbol'),
    'type' => 'dcart_order',
  );
  
  $info['tokens']['dcart-order']['customer-email'] = array(
    'name' => t('Customer E-mail'),
    'description' => t('E-mail of customer who placed order'),
    'type' => 'dcart_order',
  );

  return $info;
}

/**
 * Implements hook_tokens().
 */
function dcart_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();
  foreach ($tokens as $name => $original) {
    if ($type == 'dcart-order' && !empty($data['dcart_order'])) {
      $order = $data['dcart_order'];
      switch ($name) {
        case 'content':
          $replacements[$original] = theme('dcart_order_review', array('order' => $order));
          break;
        case 'content-plain':
          $replacements[$original] = theme('dcart_order_summary_plain_text', array('order' => $order));
          break;
        case 'id':
          $replacements[$original] = $order->order_id;
          break;
        case 'status':
          $statuses = dcart_order_statuses();
          $status = '';
          if(isset($statuses[$order->status])) {
            $status = isset($statuses[$order->status]['display_title']) ? check_plain($statuses[$order->status]['display_title']) : check_plain($statuses[$order->status]['title']);
          }
          $replacements[$original] = $status;
          break;
        case 'created':
          $replacements[$original] = date('d\/m\/Y', $order->created);
          break;
        case 'customer-email':
          $replacements[$original] = check_plain($order->mail);
          break;
        case 'status-url':
          $url = $GLOBALS['base_url'] . '/order-status/' . rawurlencode($order->uid) . '/' . $order->order_id;
          $replacements[$original] = is_numeric($order->uid) ? '' : $url;
          break;
        case 'customer-view-url':
          $replacements[$original] = is_numeric($order->uid) ? $GLOBALS['base_url'] . '/user/' . $order->uid . '/orders/' . $order->order_id . '/view' : '';
          break; 
        case 'admin-view-url':
          $replacements[$original] = is_numeric($order->uid) ? $GLOBALS['base_url'] . '/order/' . $order->order_id . '/view' : '';
          break;
        case 'admin-edit-url':
          $replacements[$original] = is_numeric($order->uid) ? $GLOBALS['base_url'] . '/order/' . $order->order_id . '/edit' : '';
          break;
        case 'amount-formatted':
          $replacements[$original] = strip_tags(theme('dcart_price', array('amount' => $order->total, 'currency_code' => $order->currency_code)));
          break;
      }
    }
    elseif($type == 'dcart') {
      switch ($name) {
        case 'cart-url':
          $replacements[$original] = url(variable_get('dcart_cart_url', 'cart'), array('absolute' => TRUE));
          break;
        case 'checkout-url':
          $replacements[$original] = url('checkout', array('absolute' => TRUE));
          break;
        case 'wishlist-url':
          $replacements[$original] = url('cart/wishlist', array('absolute' => TRUE));
          break;
      }
    }
  }
  return $replacements;
}
<?php

function dcart_dcart_checkout_pages_info() {

  $path = drupal_get_path('module', 'dcart') . '/includes/checkout/callbacks/pages';
  
  return array(
    'checkout' => array(
      'title' => t('Checkout'),
      'display_title' => t('Enter data'),
      'file' => $path . '/checkout.inc',
      'callback' => 'dcart_checkout_page_checkout',
      'weight' => 0,
      'buttons' => array(
        'prev' => t('Back to cart'),
      ),
    ),
    'complete' => array(
      'title' => t('Checkout complete'),
      'display_title' => t('Finish'),
      'file' => $path . '/complete.inc',
      'callback' => 'dcart_checkout_page_complete',
      'weight' => 30,
    ),
  );
}

function dcart_checkout_pages() {
  $pages = &drupal_static(__FUNCTION__);
  if (!isset($pages)) {
    $pages = module_invoke_all('dcart_checkout_pages_info');
    drupal_alter('dcart_checkout_pages_info', $pages);
    uasort($pages, 'drupal_sort_weight');
  }
  return $pages;
}

function dcart_checkout_page_options() {
  $options = array();
  foreach (dcart_checkout_pages() as $id => $page) {
    $options[$id] = check_plain($page['title']);
  }
  return $options;
}

function dcart_dcart_checkout_panes_info() {

  $path = drupal_get_path('module', 'dcart') . '/includes/checkout/callbacks/panes';

  return array(
    'cart' => array(
      'title' => t('Products in cart'),
      'file' => $path . '/cart.inc',
      'callback' => 'dcart_checkout_pane_cart',
      'weight' => 0,
    ),
    'checkout_shipping_address' => array(
      'title' => t('Shipping address'),
      'file' => $path . '/shipping_address.inc',
      'callback' => 'dcart_checkout_pane_checkout_shipping_address',
      'fieldable' => TRUE,
      'weight' => 10,
    ),
    'shipping_method' => array(
      'title' => t('Shipping method'),
      'file' => $path . '/shipping_method.inc',
      'callback' => 'dcart_checkout_pane_shipping_method',
      'line_item_type' => 'shipping',
      'weight' => 20,
    ),
    'comment' => array(
      'title' => t('Comment'),
      'file' => $path . '/comment.inc',
      'callback' => 'dcart_checkout_pane_comment',
      'fieldable' => TRUE,
      'weight' => 40,
    ),
    
    'payment_method' => array(
      'title' => t('Payment method'),
      'file' => $path . '/payment_method.inc',
      'callback' => 'dcart_checkout_pane_payment_method',
      'weight' => 30,
    ),
    'complete' => array(
      'title' => t('Checkout status'),
      'file' => $path . '/complete.inc',
      'callback' => 'dcart_checkout_pane_complete',
      'page' => 'complete',
      'weight' => 0,
    ),
  );
}

function dcart_checkout_pane_settings($pane_name = NULL) {
  $return = array();
  $query = db_select('dcart_checkout_pane_settings', 's')->fields('s');
  if(isset($pane_name)) {
    $query->condition('name', $pane_name);
  }
  if($results = $query->execute()->fetchAll()) {
    foreach($results as $data) {
      $return[$data->name] = (array) $data;
      $return[$data->name]['settings'] = unserialize($return[$data->name]['settings']);
    }
    if(isset($pane_name)) {
      $return = reset($return);
    }
  }
  return $return;
}

function dcart_checkout_pane_settings_save($pane_name, $settings) {
  $settings += dcart_checkout_pane_settings_default();
  $settings['settings'] = serialize($settings['settings']);
  return db_merge('dcart_checkout_pane_settings')
    ->key(array('name' => $pane_name))
    ->fields($settings)
    ->execute();
}

function dcart_checkout_pane_load($pane_name) {
  $panes = dcart_checkout_panes();
  return isset($panes[$pane_name]) ? $panes[$pane_name] : array(); 
}

function dcart_checkout_pane_settings_default() {
  return array(
    'collapsible' => 1,
    'collapsed' => 0,
    'active' => 1,
    'notitle' => 0,
    'help' =>  '',
    'weight' => 0,
    'settings' => array(),
  );
}

function dcart_checkout_panes() {
  $panes = &drupal_static(__FUNCTION__);
  if (!isset($panes)) {
    $panes = module_invoke_all('dcart_checkout_panes_info');
    $settings = dcart_checkout_pane_settings();
    foreach ($panes as $pane_name => &$pane_data) {
      
      $pane_data += dcart_checkout_pane_settings_default();
      
      if(isset($settings[$pane_name])) {
        $pane_data = $settings[$pane_name] + $pane_data;
      }
      
      if(empty($pane_data['name'])) {
        $pane_data['name'] = $pane_name;
      }
      
      if(empty($pane_data['page'])) {
        $pane_data['page'] = DCART_CHECKOUT_START_PAGE;
      }
    }
    drupal_alter('dcart_checkout_panes_info', $panes);
    uasort($panes, 'drupal_sort_weight');
  }
  return $panes;
}

function dcart_checkout_panes_get($active = FALSE, $fieldable = FALSE, $page = NULL) {
  $panes = dcart_checkout_panes();
  if ($active || $fieldable || isset($page)) {
    foreach ($panes as $pane_id => &$pane_data) {   
      if ($active && empty($panes[$pane_id]['active'])) {
        unset($panes[$pane_id]);
      }
      
      if ($fieldable && empty($panes[$pane_id]['fieldable'])) {
        unset($panes[$pane_id]);
      }
      
      if(isset($page) && $pane_data['page'] != $page) {
        unset($panes[$pane_id]);
      }
    }
  }
  return $panes;
}

function dcart_checkout_panes_options($active = FALSE, $fieldable = FALSE) {
  $options = array();
  foreach(dcart_checkout_panes_get($active, $fieldable) as $pane_id => $pane_data) {
    $options[$pane_id] = check_plain($pane_data['title']);
  }
  return $options;
}

function dcart_checkout_field_types() {
  return array(
    'list_integer',
    'list_float',
    'list_text',
    'list_boolean',
    'number_integer',
    'number_decimal',
    'number_float',
    'text',
    'text_long',
  );
}

function dcart_checkout_attach_buttons(&$form, &$form_state, $page_info) {

  if (!empty($page_info['buttons'])) {
    if (!empty($page_info['buttons']['prev'])) {
      
      $submit_prev = dcart_hook_callback($page_info, 'button_prev_submit');

      $form['prev'] = array(
        '#type' => 'submit',
        '#value' => $page_info['buttons']['prev'],
        '#limit_validation_errors' => array(),
        '#submit' => $submit_prev ? array($submit_prev) : array('dcart_checkout_page_prev_submit'),
        '#attributes' => array('class' => array('checkout-button-prev')),
        '#prefix' => '<div class="checkout-button prev">',
        '#suffix' => '</div>',
      );
      
      if ($validate_prev = dcart_hook_callback($page_info, 'button_prev_validate')) {
        $form['prev']['#validate'] = array($validate_prev);
      }
    }
    
    if (!empty($page_info['buttons']['next'])) {

      $submit_next = dcart_hook_callback($page_info, 'button_next_submit');
      $validate_next = dcart_hook_callback($page_info, 'button_next_validate');
 
      $form['next'] = array(
         '#type' => 'submit',
         '#value' => $page_info['buttons']['next'],
         '#submit' => $submit_next ? array($submit_next) : array('dcart_checkout_page_next_submit'),
         '#validate' => $validate_next ? array($validate_next) : array('dcart_checkout_page_next_validate'),
         '#attributes' => array('class' => array('checkout-button-next')),
         '#prefix' => '<div class="checkout-button next">',
         '#suffix' => '</div>',
      );
    }
  }
}


function dcart_checkout_order_save($cart_products, $uid = NULL) {

  $existing_order = TRUE;
  if(!isset($uid)) {
    $uid = dcart_customer_get();
  }
  $order_id = dcart_order_checkout_incomplete($uid);

  if ($order_id === FALSE) {
    $order = dcart_order_new();
    $order->uid = $uid;
    $order->creator = $GLOBALS['user']->uid;
    $order->status = DCART_CHECKOUT_STATUS_INIT;

    dcart_order_save($order);

    $order_id = $order->order_id;
    $existing_order = FALSE;
  }

  if ($existing_order && ($existing_order_products = dcart_order_product_get($order_id))) {
    dcart_order_product_delete(array_keys($existing_order_products));
  }

  foreach($cart_products as $product) {
    $product['order_id'] = $order_id;
    dcart_order_product_save($product);
  }
  
  dcart_checkout_cookie_set($order_id);
  return $order_id;
}

function dcart_checkout_submit($cart_content) {
  $form_state = array();
  $form_state['build_info']['args'] = array($cart_content);
  foreach($cart_content as $id => $data) {
    $form_state['values']['cart'][$id] = $data;
  }
  module_load_include('inc', 'dcart', 'includes/cart/dcart.cart.pages');
  drupal_form_submit('dcart_cart_form', $form_state);
}

function dcart_checkout_access($order = NULL, $page = NULL) {
  return user_access('dcart create order');
}

function dcart_checkout_attach_panes(&$form, &$form_state, $order) {
  
  $form['#tree'] = TRUE;
  foreach(dcart_checkout_panes_get(TRUE, FALSE, $form_state['dcart_checkout_page']) as $pane_id => $pane) {
    if ($form_builder = dcart_hook_callback($pane, 'form')) { 
      if(!empty($pane['file'])) {
        $form_state['build_info']['files'][] = $pane['file'];
      }

      if ($elements = $form_builder($form, $form_state)) {
        // Attach additional form element settings, such as "#required", "#default_value" properties
        $settings = dcart_checkout_pane_settings($pane_id);  
        if(!empty($settings['settings']['elements'])) {
          foreach($settings['settings']['elements'] as $element => $properties) {
            if (isset($elements[$element])) {
              foreach($properties as $property_name => $value) {
                if(!isset($elements[$element]['#' . $property_name])) {
                  $elements[$element]['#' . $property_name] = $value;
                }
              }
            }
          }
        }
          
        $form['panes'][$pane_id] = array(
          '#title' => $pane['title'],
          '#type' => 'fieldset',
          '#collapsible' => $pane['collapsible'],
          '#collapsed' => $pane['collapsed'],
          '#tree' => TRUE,
        );
          
        if($theme_function = dcart_hook_callback($pane, 'theme')) {
          $form['panes'][$pane_id]['#theme_wrappers'] = array($theme_function);
        }
          
        if(!empty($settings['notitle'])) {
          unset($form['panes'][$pane_id]['#title']);
        }
          
        foreach ($elements as $element_id => $element) {
          $form['panes'][$pane_id][$element_id] = $element;
        }

        $form['panes'][$pane_id]['_help'] = array(
          '#markup' => !empty($pane['help']) ? filset_xss($pane['help']) : '',
          '#weight' => isset($pane['help_weight']) ? $pane['help_weight'] : 99,
          '#prefix' => '<div class="pane-description">',
          '#suffix' => '</div>',
        );
      }    
    }
  }
}

function dcart_checkout_pane_attach_fields($pane_id, &$form_state) {
  $form = array();
  $order = $form_state['dcart_order'];
  foreach ($form_state['instances'] as $field_name => $field_data) {
    if (!empty($field_data['dcart_settings']['checkout_pane']) && $field_data['dcart_settings']['checkout_pane'] == $pane_id) {
      $form['#parents'] = array('panes', $pane_id);
      field_attach_form('dcart_order', $order, $form, $form_state, NULL, array('field_name' => $field_name));
    }
  }
  return $form;
}

function dcart_checkout_pane_form_validate(&$form, &$form_state) {
  foreach(dcart_checkout_panes_get(TRUE, FALSE, $form_state['dcart_checkout_page']) as $pane_id => $pane_info) {
    if($validate_callback = dcart_hook_callback($pane_info, 'form_validate')) {
      $validate_callback($form, $form_state);
    }
  }
}

function dcart_checkout_pane_form_submit($form, &$form_state) {
  foreach(dcart_checkout_panes_get(TRUE, FALSE, $form_state['dcart_checkout_page']) as $pane_id => $pane_info) {
    if($submit_callback = dcart_hook_callback($pane_info, 'form_submit')) {
      $submit_callback($form, $form_state);
    }
  }
  dcart_checkout_save_data($form, $form_state);
}

function dcart_checkout_page_prev_submit(&$form, &$form_state) {
  dcart_checkout_page_redirect($form_state, 'prev');
  foreach (module_implements('dcart_checkout_page_prev') as $module) {
    $function = $module . '_dcart_checkout_page_prev';
    $function($form_state['dcart_checkout_page'], $form_state);
  }
}

function dcart_checkout_page_next_validate(&$form, &$form_state) {
  dcart_checkout_pane_form_validate($form, $form_state);
}

function dcart_checkout_page_next_submit($form, &$form_state) {
  foreach (module_implements('dcart_checkout_page_next') as $module) {
    $function = $module . '_dcart_checkout_page_next';
    $function($form, $form_state);
  }

  dcart_checkout_pane_form_submit($form, $form_state);
  dcart_checkout_page_redirect($form_state, 'next');
}

function dcart_checkout_cookie_clear() {
  setcookie('dcart_checkout_anonymous', "", REQUEST_TIME - 3600, '/', NULL);
  setcookie('dcart_cart', "", REQUEST_TIME - 3600, '/', NULL);
  setcookie('dcart_order', "", REQUEST_TIME - 3600, '/', NULL);
}

function dcart_checkout_page_redirect(&$form_state, $direction) {
  
  $order_id = $form_state['dcart_order']->order_id;

  $current_page = $form_state['dcart_checkout_page'];
  $adjacent = dcart_checkout_page_lookup($current_page);

  $form_state['redirect'] =  'checkout/' . $order_id . '/' . $adjacent[$direction];
  if (is_null($adjacent[$direction]) && $current_page == DCART_CHECKOUT_START_PAGE) {
    $form_state['redirect'] = variable_get('dcart_cart_url', 'cart');
  }
}

function dcart_checkout_redirect_complete_page(&$form_state) {
  $order = $form_state['dcart_order'];
  $pages = dcart_checkout_pages();
  if(isset($pages[DCART_CHECKOUT_COMPLETE_PAGE])) {
    $form_state['redirect'] = 'checkout/' . $order->order_id . '/' . DCART_CHECKOUT_COMPLETE_PAGE;
  }
}

function dcart_checkout_page_form($form, $form_state, $order, $page_name, $page_info) {
  
  $form['#validate'] = array();
  $form['#submit'] = array(); 
  $form['#attributes'] = array('id' => 'checkout-form');

  dcart_checkout_attach_panes($form, $form_state, $order);
  dcart_checkout_attach_buttons($form, $form_state, $page_info);

  $form['#prefix'] = '<div id="checkout-form-wrapper">';
  $form['#suffix'] = '<div class="loading-overlay"></div></div>';
  return $form;
}

function dcart_checkout_calculate_total($form, $form_state) {
  $results = dcart_checkout_calculate($form, $form_state);
  return (int) ($results['products_total'] + $results['line_items_total']);
}

function dcart_checkout_save_data($form, $form_state) {
  $order = $form_state['dcart_order'];
  $page = $form_state['dcart_checkout_page'];

  $order_data = isset($order->data) ? $order->data : array();
  $results = dcart_checkout_calculate($form, $form_state);
  $order_data += array('products' => $results['products'], 'line_items' => $results['line_items']);
  $order_data['submitted_values'] = $form_state['values']['panes'];

  $order->data = $order_data;
  $order->status = 'checkout_' . $page;
  
  $form['#entity_builders'][] = 'dcart_checkout_submit_build_order';
  entity_form_submit_build_entity('dcart_order', $order, $form, $form_state);
  dcart_order_save($order);
}


function dcart_checkout_submit_build_order($entity_type, $entity, $form, $form_state) {

  $instances = $form_state['instances'];
  $panes = $form_state['values']['panes'];

  foreach(dcart_checkout_panes_get(TRUE, TRUE, $form_state['dcart_checkout_page']) as $pane_id => $pane_info) {
    if(isset($panes[$pane_id])) {
      foreach($panes[$pane_id] as $field_name => $field_data) {
        if(isset($instances[$field_name]) && !empty($instances[$field_name]['dcart_settings']['checkout_pane'])) {
          $entity->{$field_name}[LANGUAGE_NONE][0]['value'] = $panes[$pane_id][$field_name][LANGUAGE_NONE][0]['value'];
        }
      }
    }
  }
}

function dcart_checkout_page_lookup($current){
  
  $prev = $next = NULL;
  $pages = dcart_checkout_pages();

  $page_names = array_keys($pages);
  $page_indexes = array_flip($page_names);
  $i = $page_indexes[$current];
  
  if ($i > 0) {
    $prev = $page_names[$i - 1];
  }
    
  if ($i < count($page_names) - 1) {
    $next = $page_names[$i + 1];
  }
    
  return array('prev' => $prev, 'next' => $next);
}

function dcart_checkout_pane_fields_view($pane_id, $order, $no_label = FALSE) {

  $fields = array();
  foreach(dcart_checkout_pane_fields($pane_id) as $field_name => $field_data) {
    $field_value = render(field_view_field('dcart_order', $order, $field_name, array('label'=>'hidden')));
    $label = $no_label ? '' : check_plain($field_data['label']);
    $fields[$field_name] = array($label, $field_value);
  }
  return $fields;
}


function dcart_checkout_pane_fields($pane_id) {
  $fields = array();
  foreach(dcart_order_checkout_fields() as $field_name => $field_data){
    if($pane_id == $field_data['dcart_settings']['checkout_pane']) {
      $fields[$field_name] = $field_data;
    }
  }
  return $fields;
}

function dcart_checkout_pane_refresh($form, &$form_state) {

  $commands = array();
  $page = $form_state['dcart_checkout_page'];
  foreach (dcart_checkout_panes_get(TRUE, FALSE, $page) as $pane) {
    if($refresh_callback = dcart_hook_callback($pane, 'refresh')) {
      $refresh_callback($form, $form_state, $commands);
    }
  }

  $results = dcart_checkout_calculate($form, $form_state);
  $table = theme('dcart_order_total', array('components' => $results));
  $commands[] = ajax_command_replace('#checkout-grand-total-table', '<div id="checkout-grand-total-table">' . $table . '</div>');

  drupal_alter('dcart_checkout_pane_refresh', $form, $form_state, $commands);
  return array('#type' => 'ajax', '#commands' => $commands);
}

function dcart_checkout_calculate($form, $form_state) {

  $order = $form_state['dcart_order'];
  $context = array('form_state' => $form_state);
  $order_products = !empty($order->data['products']) ? $order->data['products'] : array();
  $calculated_products = dcart_order_calculate_products($order_products, $context);
  $components = array('products' => $calculated_products);

  $pane_line_items = dcart_checkout_panes_line_items($form, $form_state);
  $calculated_line_items = dcart_order_calculate_line_items($pane_line_items, $context);
  $components['line_items'] = $calculated_line_items;

  $line_items_total = dcart_order_calculate_line_items_total($calculated_line_items);
  $components['line_items_total'] = $line_items_total;
  
  $products_total = dcart_order_calculate_product_total($calculated_products);
  $components['products_total'] = $products_total;

  foreach (module_implements('dcart_checkout_calculate') as $module) {
    $function = $module . '_dcart_checkout_calculate';
    $function($components, $context);
  }
  return $components;
}

function dcart_checkout_panes_line_items($form, $form_state) {
  $line_items = array();
  $line_item_types = dcart_line_item_types();
  foreach(dcart_checkout_panes_get(TRUE, FALSE, $form_state['dcart_checkout_page']) as $pane) {
    if ($callback = dcart_hook_callback($pane, 'form_get')) {
      $result = $callback($form, $form_state);
      if(!empty($result['line_items'])) {
        reset($result['line_items']);
        $line_item_type = key($result['line_items']);
        if(isset($line_item_types[$line_item_type])) {
          $line_items[$line_item_type]['value'] = $result['line_items'][$line_item_type];
        }
      }
    }
  }
  return $line_items;
}

function dcart_checkout_panes_content_build($order) {

  $panes = array();
  $panes_info = dcart_checkout_panes_get(TRUE);

  foreach($panes_info as $pane_id => $pane) {
    if (isset($panes_info[$pane_id]) && ($view_callback = dcart_hook_callback($panes_info[$pane_id], 'view'))) {
      if ($view = $view_callback($pane_id, $order)) {
        foreach($view as $key => &$item) {
          if(!empty($item[0])) {
            $view[$key][0] = t($item[0]);
          }
        }

        $panes[$pane_id]['pane'] = $pane;
        $panes[$pane_id]['items'] = $view;
        $panes[$pane_id]['weight'] = $panes_info[$pane_id]['weight'];
        $panes[$pane_id]['title'] = $panes_info[$pane_id]['title'];
      }
    }
  }

  uasort($panes, 'drupal_sort_weight');
  drupal_alter('dcart_panes_content_build', $panes);
  return $panes;
}

function _dcart_checkout_extract_address_fields($form_state, $address_fields) {
  $fields = array();
  foreach($form_state['input'] as $field_name => $field_value) {
    if(isset($address_fields[$field_name]) && isset($field_value[LANGUAGE_NONE][0]['value'])) {
      $fields[$field_name] = trim($field_value[LANGUAGE_NONE][0]['value']);
    }
  }
  return array_filter($fields);
}

function dcart_checkout_cookie_set($order_id) {
  setcookie('dcart_complete', drupal_get_hash_salt(), 0, '/');
  $cookie = dcart_cookie_get('dcart_cart');
  $order_cookie = base64_encode($cookie . '::' . $order_id);
  
  $lifespan = variable_get('dcart_order_lifespan', 31557600);
  dcart_cookie_set('dcart_order', $lifespan, $order_cookie);
}

function dcart_checkout_complete($order, $status = 'pending') {
  if($status) {
    dcart_order_status_set($order, $status);
    dcart_order_save($order);
    $skus = array();
    foreach(dcart_order_product_get($order->order_id) as $product) {
      $skus[] = $product['sku'];
    }
    dcart_cart_delete(NULL, $order->uid, DCART_CHECKOUT_READY, $skus);
  }
  
  module_invoke_all('dcart_checkout_complete', $order);
}

function dcart_dcart_checkout_complete($order) {
  dcart_checkout_complete_mail_customer($order);
  dcart_checkout_complete_mail_admin($order);
}

function dcart_checkout_complete_mail_admin($order) {
  $action = 'dcart_action_checkout_completed_email_admin';
  dcart_action_perform($action, $order);
}

function dcart_checkout_valid_cookie($type) {
  $salt = drupal_get_hash_salt();
  $anonymous_cookie = dcart_cookie_get($type);
  return ($anonymous_cookie == $salt);
}

function dcart_checkout_complete_mail_customer($order) {
  $action = 'dcart_action_checkout_completed_email_customer_anonymous';
  if(is_numeric($order->uid)) {
    $action = 'dcart_action_checkout_completed_email_customer';
  }
  dcart_action_perform($action, $order);
}

function dcart_checkout_complete_message($order) {
  $action = is_numeric($order->uid) ? 'dcart_action_checkout_completed_message' : 'dcart_action_checkout_completed_message_anonymous';
  $result = dcart_action_perform($action, $order);
  
  $settings = dcart_action_get_settings($action);
  $filter = isset($settings['filter']) ? $settings['filter'] : NULL;
  
  $message = '';
  if(!empty($result)) {
    $result = reset($result);
    $message = check_markup($result, $filter);
  }
  return $message;
}

function dcart_preprocess_dcart_checkout_login(&$variables) {
  $variables['login'] = drupal_get_form('user_login_block');
  $variables['continue'] = drupal_get_form('dcart_checkout_continue_anonymous_form');
  $variables['text'] = t('You don\'t need to sign in. Just continue and create an account later if you\'d like.');
}

<?php

function dcart_checkout_pane_comment_view($pane_id, $order) {
  return dcart_checkout_pane_fields_view($pane_id, $order, TRUE);
}

function dcart_checkout_pane_comment_form($elements, &$form_state) {
  $fields = dcart_checkout_pane_attach_fields('comment', $form_state);
  return $fields;
}


<?php

function dcart_checkout_pane_shipping_method_form($elements, $form_state) {

  $form = array();
  $options = dcart_shipping_service_rate_options();

  $order = $form_state['dcart_order'];
  $default_shipping = dcart_order_line_items_get($order, 'shipping');

  $default_option = key($options);
  if(!empty($options[$default_shipping['value']])) {
    $default_option = $default_shipping['value'];
  }

  $form['shipping_service'] = array(
    '#type' => 'radios',
    '#title' => t('Shipping method'),
    '#title_display' => 'invisible',
    '#options' => $options,
    '#default_value' => $default_option,
    '#ajax' => array(
      'callback' => 'dcart_checkout_pane_refresh',
      'progress' => array('message' => ''),
    ),
  );
  return $options ? $form : array();
}


function dcart_checkout_pane_shipping_method_form_get($elements, $form_state) {

  if(isset($form_state['input']['panes']['shipping_method']['shipping_service'])) {
    $shipping_service = $form_state['input']['panes']['shipping_method']['shipping_service'];
  }
  elseif(isset($elements['panes']['shipping_method'])) {
    $pane = $elements['panes']['shipping_method'];
    if (isset($pane['shipping_service']['#default_value'])) {
      $shipping_service = $pane['shipping_service']['#default_value'];
    }
  }
  if (isset($shipping_service)) {
    return array('line_items' => array('shipping' => $shipping_service));
  }
}

function dcart_checkout_pane_shipping_method_view($pane_id, $order) {
  if (!empty($order->data['line_items']['shipping']['value'])) {
    $shipping = $order->data['line_items']['shipping']['value'];
    $shipping = dcart_shipping_services_get($shipping);
    $title = !empty($shipping['display_title']) ? $shipping['display_title'] : $shipping['title'];
    return array(array(NULL, $title));
  }
}

function dcart_checkout_pane_shipping_method_settings_form($settings) {
  $form['elements']['shipping_service']['default_value'] = array(
    '#title' => t('@element: default value', array('@element' => t('Shipping service'))),
    '#type' => 'radios',
    '#options' => dcart_shipping_service_rate_options(FALSE),
    '#default_value' => isset($settings['settings']['elements']['shipping_service']['default_value']) ? $settings['settings']['elements']['shipping_service']['default_value'] : NULL,
  );

  $form['elements']['shipping_service']['required'] = array(
    '#title' => t('@element: required', array('@element' => t('Shipping service'))),
    '#type' => 'checkbox',
    '#default_value' => isset($settings['settings']['elements']['shipping_service']['required']) ? $settings['settings']['elements']['shipping_service']['required'] : 0,
  );
  return $form;
}


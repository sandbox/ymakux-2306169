<?php

function dcart_checkout_pane_payment_method_form($elements, $form_state) {

  $form = array();
  $options = dcart_payment_method_options(TRUE, TRUE, TRUE);
  $order = $form_state['dcart_order'];
  
  $form['payment_method'] = array(
    '#title' => t('Payment method'),
    '#title_display' => 'invisible',
    '#type' => 'radios', 
    '#options' => $options,
    '#default_value' => isset($order->data['line_items']['payment']) ? $order->data['line_items']['payment'] : key($options),
    '#ajax' => array(
      'callback' => 'dcart_checkout_pane_refresh',
      'progress' => array('message' => ''),
    ),
  );

  if (!$options) {
   $form['payment_method'] = array('#markup' => t('No payment methods found for your order. Please continue the checkout process.'));
  }
  return $form;
}


function dcart_checkout_pane_payment_method_form_get($elements, $form_state) {

  if(isset($form_state['input']['panes']['payment_method']['payment_method'])) {
    $payment_method = $form_state['input']['panes']['payment_method']['payment_method'];
  }
  elseif (isset($elements['panes']['payment_method'])) {
    $pane = $elements['panes']['payment_method'];
    if (isset($pane['payment_method']['#default_value'])) {
      $payment_method = $pane['payment_method']['#default_value'];
    }
  }
  
  if(isset($payment_method)) {
    return array('line_items' => array('payment' => $payment_method));
  }
}

function dcart_checkout_pane_payment_method_view($pane_id, $order) {
  if (!empty($order->data['line_items']['payment'])) {
    $payment_method = $order->data['line_items']['payment'];
    $payment_methods = dcart_payment_methods();
    $payment_title = !empty($payment_methods[$payment_method]['display_title']) ? $payment_methods[$payment_method]['display_title'] : $payment_methods[$payment_method]['title'];
    return array(array(NULL, $payment_title));
  }
}

function dcart_checkout_pane_checkout_payment_method_settings_form($settings) {
  $form['elements']['payment_method']['default_value'] = array(
    '#title' => t('@element: default value', array('@element' => t('Payment method'))),
    '#type' => 'radios',
    '#options' => dcart_payment_method_options(TRUE, FALSE),
    '#default_value' => isset($settings['settings']['elements']['payment_method']['default_value']) ? $settings['settings']['elements']['payment_method']['default_value'] : NULL,
  );
  $form['elements']['payment_method']['required'] = array(
    '#title' => t('@element: required', array('@element' => t('Payment method'))),
    '#type' => 'checkbox',
    '#default_value' => isset($settings['settings']['elements']['payment_method']['required']) ? $settings['settings']['elements']['payment_method']['required'] : 0,
  );
  return $form;
}
<?php

function dcart_checkout_pane_checkout_shipping_address_view($pane_id, $order) {
  return dcart_checkout_pane_fields_view($pane_id, $order);
}

function dcart_checkout_pane_checkout_shipping_address_form($elements, &$form_state) {
  
  $form = array();
  $order = $form_state['dcart_order'];
  $attached_fields = dcart_checkout_pane_attach_fields('checkout_shipping_address', $form_state);
  
  if(user_is_anonymous()) {
    foreach(element_children($attached_fields) as $element) {
      $form['address'][$element] = $attached_fields[$element];
    }
    return $form;
  }

  $addresses = dcart_customer_addressbook_options($form_state['dcart_order']->uid);

  $address_form_shown = (isset($form_state['triggering_element']['#name'])
  && $form_state['triggering_element']['#name'] == 'shipping_address_add');
  
  $form['address'] = array(
    '#type' => 'container',
    '#attributes' => array('id' => 'addressbook-shipping-address', 'class' => array('addressbook-address')),
    '#tree' => FALSE,
  ); 
  
  if($addresses && !$address_form_shown) {
    end($addresses);
    $default_address = key($addresses);

    if(!empty($order->data['addressbook_record'])) {
      $saved_record = $order->data['addressbook_record'];
      if(isset($addresses[$saved_record])) {
        $default_address = $saved_record;
      }
    }

    $form['address']['record'] = array(
      '#title' => t('Your existing addresses'),
      '#title_display' => 'invisible',
      '#type' => 'radios',
      '#options' => $addresses,
      '#default_value' => $default_address,
      '#ajax' => array(
        'callback' => 'dcart_checkout_pane_refresh',
        'progress' => array('message' => ''),
      ),
    );
  }

  $form['address']['add'] = array(
    '#type' => 'submit',
    '#value' => t('Add another address'),
    '#limit_validation_errors' => array(),
    '#submit' => array('dcart_checkout_pane_checkout_shipping_address_add_submit'),
    '#ajax' => array(
      'callback' => 'dcart_checkout_pane_checkout_shipping_address_add_ajax',
      'wrapper' => 'addressbook-shipping-address',
      'progress' => array('message' => ''),
    ),
    '#name' => 'shipping_address_add',
    '#access' => ($addresses && !$address_form_shown),
    '#prefix' => '<div class="add-address">',
    '#suffix' => '</div>',
    '#weight' => 100,
  );
  
  $form['address']['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#submit' => array('dcart_checkout_pane_checkout_shipping_address_save_submit'),
    '#validate' => array('dcart_checkout_pane_checkout_shipping_address_save_validate'),
    '#ajax' => array(
      'callback' => 'dcart_checkout_pane_checkout_shipping_address_save_ajax',
      'wrapper' => 'addressbook-shipping-address',
      'progress' => array('message' => ''),
    ),
    '#name' => 'shipping_address_save',
    '#access' => $address_form_shown,
    '#weight' => 100,
  );
  
  // Limit validation to only partial attached fields
  foreach(element_children($attached_fields) as $field_name) {
    $form['address']['save']['#limit_validation_errors'][] = array($field_name);
  }
  
  $form['address']['add_cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
    '#limit_validation_errors' => array(),
    '#submit' => array('dcart_checkout_pane_checkout_shipping_address_add_cancel_submit'),
    '#access' => $address_form_shown,
    '#ajax' => array(
      'callback' => 'dcart_checkout_pane_checkout_shipping_address_add_cancel_ajax',
      'wrapper' => 'addressbook-shipping-address',
      'progress' => array('message' => ''),
    ),
    '#name' => 'shipping_address_add_cancel',
    '#weight' => 110,
  );
  
  if($address_form_shown || !$addresses) {
    $form['address'] += dcart_checkout_pane_attach_fields('checkout_shipping_address', $form_state);
  }
  return $form;
}

function dcart_checkout_pane_checkout_shipping_address_add_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
}

function dcart_checkout_pane_checkout_shipping_address_add_ajax(&$form, &$form_state) {
  return $form['panes']['checkout_shipping_address']['address'];
}

function dcart_checkout_pane_checkout_shipping_address_save_ajax($form, &$form_state) {
  return $form['panes']['checkout_shipping_address']['address'];
}

function dcart_checkout_pane_checkout_shipping_address_save_validate($form, &$form_state) {
  dcart_checkout_pane_checkout_shipping_address_form_validate($form, $form_state);
}

function dcart_checkout_pane_checkout_shipping_address_save_submit($form, &$form_state) {
  dcart_checkout_pane_checkout_shipping_address_form_submit($form, $form_state);
  $form_state['rebuild'] = TRUE;
}

function dcart_checkout_pane_checkout_shipping_address_add_cancel_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
}

function dcart_checkout_pane_checkout_shipping_address_add_cancel_ajax(&$form, &$form_state) {
  return $form['panes']['checkout_shipping_address']['address'];
}

function dcart_checkout_pane_checkout_shipping_address_form_validate(&$form, &$form_state) {

  $pane_id = 'checkout_shipping_address';
  $form_state['checkout_address_fields'] = $checkout_address_fields = dcart_order_checkout_fields(TRUE);
  $extra_validators = dcart_field_extra_validators();
  
  if(!empty($form_state['values']['panes'][$pane_id]['address'])) {
    $submitted_address = $form_state['values']['panes'][$pane_id]['address'];
    foreach($submitted_address as $field_name => $field_value) {
      if(empty($checkout_address_fields[$field_name])) {
        unset($form_state['values']['panes'][$pane_id][$field_name], $submitted_address[$field_name]);
      }
    }
  }
  else {
    $submitted_address = _dcart_checkout_extract_address_fields($form_state, $checkout_address_fields);
  }
  
  if(!empty($submitted_address)) {
    foreach($submitted_address as $field_name => $field_value) {
      $order = new stdClass();
      $elements = $form['panes'][$pane_id]['address'][$field_name];
      field_attach_form_validate('dcart_order', $order, $elements, $form_state, array('field_name' => $field_name));

      if (!empty($checkout_address_fields[$field_name]['dcart_settings']['field_validator'])) {
        $extra_validator = $checkout_address_fields[$field_name]['dcart_settings']['field_validator'];
        if (isset($extra_validators[$extra_validator]) && ($callback = dcart_hook_callback($extra_validators[$extra_validator]))) {
          $callback($elements, $form_state);
        }
      }
    }
  }
}

function dcart_checkout_pane_checkout_shipping_address_form_submit($form, $form_state) {

  $order = $form_state['dcart_order'];
  $pane_id = 'checkout_shipping_address';
  $checkout_address_fields = $form_state['checkout_address_fields'];
  
  if (user_is_anonymous()) {
    foreach($checkout_address_fields as $field_name => $field_value) {
      $order->{$field_name} = NULL;
      if(isset($form_state['input']['panes'][$pane_id]['address'][$field_name])) {
        $value = trim($form_state['input']['panes'][$pane_id]['address'][$field_name][LANGUAGE_NONE][0]['value']);
        if($value) {
          $order->{$field_name}[LANGUAGE_NONE][0]['value'] = $value;
        }
      }
    }
    return dcart_order_save($order);
  }

  if(!empty($form_state['input']['record'])) {
    $record_id = (int) $form_state['input']['record'];
    $fill_address_fields = TRUE;
  }
  else {
    $add_addressbook_record = TRUE;
    $submitted_address = _dcart_checkout_extract_address_fields($form_state, $checkout_address_fields);
    
    $address_status = DCART_ADDRESSBOOK_DEFAULT;
    if ($existing_addresses = dcart_customer_addressbook_record_get($order->uid)) {
      $address_status = DCART_ADDRESSBOOK_ACTIVE;
      foreach($existing_addresses as $address) {
        $existing_address = isset($address['data']['address']) ? $address['data']['address'] : array();
        if(!array_diff($submitted_address, $existing_address)) {
          $add_addressbook_record = FALSE;
          break;
        }
      }
    }

    if($add_addressbook_record) {
      dcart_customer_addressbook_record_save(array(
        'data' => array(
          'address' => $submitted_address),
          'status' => $address_status,
      ));
    
      $addresses = dcart_customer_addressbook_record_get($form_state['dcart_order']->uid);
      end($addresses);
      $record_id = key($addresses);
    }
  }
  
  if(isset($record_id)) {
      $order->data['addressbook_record'] = $record_id;
      if(isset($fill_address_fields)) {
        $address = dcart_customer_addressbook_record_get(NULL, NULL, $record_id);
        foreach($checkout_address_fields as $field_name => $field_value) {
          $order->{$field_name} = NULL;
          if(isset($address['data']['address'][$field_name])) {
            $order->{$field_name}[LANGUAGE_NONE][0]['value'] = $address['data']['address'][$field_name];
          }
        }
      }
    dcart_order_save($order);
  }
}

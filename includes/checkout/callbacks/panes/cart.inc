<?php

function dcart_checkout_pane_cart_form($elements, $form_state) {
  $order = $form_state['dcart_order'];
  $cart_content = dcart_cart_content_build($order->uid, NULL, $order->data['products']);
  $form['cart'] = array(
    '#markup' => theme('dcart_cart', array('cart_content' => $cart_content)),
  );
  return $form;
}
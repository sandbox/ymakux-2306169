<?php

function dcart_checkout_page_complete_form($form, $form_state, $order, $page_name, $page_info) {
  dcart_checkout_attach_panes($form, $form_state, $order);
  return $form;
}
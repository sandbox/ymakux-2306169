<?php

function dcart_checkout_redirect() {
  if($cart_content = dcart_cart_content_build()) {
    return dcart_checkout_submit($cart_content);
  }
  drupal_goto(variable_get('dcart_cart_url', 'cart'));
}

function dcart_checkout($order, $page_name, $page_info) {

  $cart = variable_get('dcart_cart_url', 'cart');
  if (empty($order->data['products'])) {
    return drupal_goto($cart);
  }
  if($page_name != DCART_CHECKOUT_COMPLETE_PAGE) {
    if($order->status == DCART_CHECKOUT_STATUS_CANCELED) {
      return drupal_goto($cart);
    }
  }
  
  if($page_name == DCART_CHECKOUT_COMPLETE_PAGE) {
    if(!dcart_checkout_valid_cookie('dcart_complete')) {
      return drupal_goto($cart);
    }
    dcart_checkout_cookie_clear();
  }
  else {
    if($order->order_id != dcart_cookie_get_order_id()) {
      return MENU_ACCESS_DENIED;
    }

    $checkout_statuses = dcart_order_statuses_get(TRUE);
    if(!isset($checkout_statuses[$order->status])) {
      return drupal_goto($cart);
    }
  }
  
  if($page_callback = dcart_hook_callback($page_info)) {
    return $page_callback($order, $page_name, $page_info);
  }
  
  $build = array();
  if (user_is_anonymous()
  && !dcart_checkout_valid_cookie('dcart_checkout_anonymous')
  && $page_name != DCART_CHECKOUT_COMPLETE_PAGE) {
    if (variable_get('dcart_checkout_login_form', 1)) {
      $build['login'] = array('#theme' => 'dcart_checkout_login');
    }
    return $build;
  }
      
  if(isset($page_info['title'])) {
    drupal_set_title(check_plain($page_info['title']));
  }

  $form_state = array(
    'dcart_order' => $order,
    'dcart_checkout_page' => $page_name,
    'instances' => field_info_instances('dcart_order', 'dcart_order'),
    'dcart_order_statuses' => dcart_order_statuses(),
    'dcart_checkout_panes' => dcart_checkout_panes(),
    'build_info' => array('args' => array($order, $page_name, $page_info)),
  );
      
  $build['progress']['#markup'] = theme('dcart_checkout_progress');
  $build['checkout'] = drupal_build_form('dcart_checkout_page_form', $form_state);
  return $build;
}

function dcart_checkout_continue_anonymous_form($form, &$form_state) {
  
  $form['#access'] = user_access('dcart create order');
  $form['actions'] = array('#type' => 'actions');

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Continue'),
    '#submit' => array('dcart_checkout_continue_anonymous_form_submit'),
  );
  return $form;
}

function dcart_checkout_continue_anonymous_form_submit($form, &$form_state) {
  $hash = drupal_get_hash_salt();
  $lifespan = variable_get('dcart_checkout_cookie_lifespan', 31557600);
  dcart_cookie_set('dcart_checkout_anonymous', $lifespan, $hash);

  foreach (module_implements('dcart_checkout_continue_anonymous') as $module) {
    $function = $module . '_dcart_checkout_continue_anonymous';
    $function($form_state);
  }
}

function theme_dcart_checkout_progress($variables) {
  $output = '';
  if(arg(0) == 'checkout' && is_numeric(arg(1))) {
    $current_page = (arg(2)) ? arg(2) : 'checkout';
    $progress = array();
    foreach(dcart_checkout_pages() as $page_id => $page_data) {
      
      if(isset($page_data['progress']) && !$page_data['progress']) {
        continue;
      }
      
      $class = array();
      if ($page_id === $current_page) {
        $class[] = 'active';
      }
      $progress[] = array(
        'data' => !empty($page_data['display_title']) ? check_plain($page_data['display_title']) : check_plain($page_data['title']),
        'class' => $class,
      );
    }
  }
  
  $output = '<div id="dcart-checkout-progress">' . theme('item_list', array(
    'items' => $progress,
    'type' => 'ul',
    'attributes' => array('class' => array('clearfix', 'inline')),
  )) . '</div>';
  
  return $output;
}

function dcart_checkout_settings_form($form, $form_state) {
  
  $form['dcart_checkout_login_form'] = array(
    '#title' => t('Suggest login form for anonymous'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('dcart_checkout_login_form', 1),
  );
  $form['dcart_checkout_forbid_stock_0'] = array(
    '#title' => t('Forbid checkout if stock level is 0'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('dcart_checkout_forbid_stock_0', 0),
  );
  $form['dcart_checkout_forbid_stock_null'] = array(
    '#title' => t('Forbid checkout if stock level isn\'t set'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('dcart_checkout_forbid_stock_null', 0),
  );
  return system_settings_form($form);
}

function dcart_checkout_panes_form($form, $form_state) {
  
  $pages = dcart_checkout_pages();
  $panes = dcart_checkout_panes();

  $form['#tree'] = TRUE;
  $form['pages_info'] = array('#type' => 'value', '#value' => $pages);
  
  foreach ($pages as $page_id => $page_info) {
    foreach ($panes as $pane_id => $pane_info) {
      if ($pane_info['page'] == $page_id) {
        $form['pages'][$page_id][$pane_id]['name'] = array(
          '#type' => 'value',
          '#value' => $pane_info['name'],
        );
        $form['pages'][$page_id][$pane_id]['collapsible'] = array(
          '#type' => 'value',
          '#value' => $pane_info['collapsible'],
        );
        $form['pages'][$page_id][$pane_id]['collapsed'] = array(
          '#type' => 'value',
          '#value' => $pane_info['collapsed'],
        );
        $form['pages'][$page_id][$pane_id]['notitle'] = array(
          '#type' => 'value',
          '#value' => $pane_info['notitle'],
        );
        $form['pages'][$page_id][$pane_id]['help'] = array(
          '#type' => 'value',
          '#value' => $pane_info['help'],
        );
        $form['pages'][$page_id][$pane_id]['settings'] = array(
          '#type' => 'value',
          '#value' => $pane_info['settings'],
        );
        $form['pages'][$page_id][$pane_id]['title'] = array(
          '#markup' => $pane_info['title'],
        );
        $form['pages'][$page_id][$pane_id]['active'] = array(
          '#type' => 'checkbox',
          '#default_value' => $pane_info['active'],
        );
        $form['pages'][$page_id][$pane_id]['operations']['settings'] = array(
          '#type' => 'link',
          '#title' => t('Settings'),
          '#href' => 'admin/dcart/settings/checkout/panes/' . $pane_id,
          '#options' => array('query' => array('destination' => current_path())),
        );
        $form['pages'][$page_id][$pane_id]['weight'] = array(
          '#type' => 'weight',
          '#delta' => 10,
          '#attributes' => array('class' => array('checkout-pane-weight', 'checkout-pane-weight-' . $page_id)),
          '#default_value' => $pane_info['weight'],
        );
      }
    }
  }
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  return $form;
}

function theme_dcart_checkout_panes_form($variables) {
  
  $output = '';
  $form = $variables['form'];
  $header = array(t('Pane'), t('Active'), t('Weight'), t('Operations'));

  $step = 1;
  $table_id = 'checkout-page-table';
  
  $rows = array();
  foreach (element_children($form['pages']) as $page_id) {
    $title =  $form['pages_info']['#value'][$page_id]['title'];
    $rows[] = array(array('data' => t('Step @num - @title', array('@num' => $step, '@title' => $title)), 'colspan' => 5));

    drupal_add_tabledrag($table_id, 'order', 'sibling', 'checkout-pane-weight', 'checkout-pane-weight-' . $page_id);
 
    foreach (element_children($form['pages'][$page_id]) as $pane_id) {
      $rows[] = array(
        'data' => array(
          render($form['pages'][$page_id][$pane_id]['title']),
          render($form['pages'][$page_id][$pane_id]['active']),
          render($form['pages'][$page_id][$pane_id]['weight']),
          render($form['pages'][$page_id][$pane_id]['operations']),
        ),
        'class' => array('draggable'),
      );
    }
    $step++;
  }
  
  $output .= theme('table', array(
    'rows' => $rows,
    'header' => $header,
    'empty' => t('No checkout panes.'),
    'attributes' => array('id' => $table_id),
  ));
  return $output . drupal_render_children($form);
}

function dcart_checkout_panes_form_submit($form, $form_state) {
  foreach ($form_state['values']['pages'] as $page_id => $page_panes) {
    foreach ($page_panes as $pane_name => $pane_settings) {
      dcart_checkout_pane_settings_save($pane_name, $pane_settings);
    }
  }
  drupal_set_message(t('The configuration options have been saved.'));
}

function dcart_checkout_pane_settings_form($form, $form_state, $pane) {
  
  $form_state['checkout_pane'] = $pane;

  $form['settings'] = array(
    '#type' => 'container',
    '#weight' => 99,
    '#tree' => TRUE,
  );
  
  if ($form_function = dcart_hook_callback($pane, 'settings_form')) {
    if($elements = $form_function($pane)) {
      $form['settings'] += $elements;
    }
  }

  $form['weight'] = array(
    '#type' => 'value',
    '#value' => $pane['weight'],
  );
  $form['collapsible'] = array(
    '#title' => t('Collapsible'),
    '#type' => 'checkbox',
    '#default_value' => $pane['collapsible'],
    '#description' => t('If selected, customer can collapse/expand pane fieldset.'),
  );
  $form['collapsed'] = array(
    '#title' => t('Collapsed'),
    '#type' => 'checkbox',
    '#default_value' => $pane['collapsed'],
    '#description' => t('Select to make pane fieldset collapsed by default.'),
  );
  $form['active'] = array(
    '#title' => t('Active'),
    '#type' => 'checkbox',
    '#default_value' => $pane['active'],
    '#description' => t('Only active panes will be shown on checkout page.'),
  );
  $form['notitle'] = array(
    '#title' => t('No title'),
    '#type' => 'checkbox',
    '#default_value' => $pane['notitle'],
    '#description' => t('Hide fieldset title'),
  );
  $form['help'] = array(
    '#title' => t('Help text'),
    '#type' => 'textarea',
    '#rows' => 2,
    '#default_value' => filter_xss($pane['help']),
    '#description' => t('This text will be displayed inside pane fieldset.'),
  );
  $form['actions'] = array(
    '#type' => 'actions',
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  return $form;
}

function dcart_checkout_pane_settings_form_validate($form, &$form_state) {
  form_state_values_clean($form_state);
  if($validate_function = dcart_hook_callback($form_state['checkout_pane'], 'settings_form_validate')) {
    $validate_function($form, $form_state);
  }
}

function dcart_checkout_pane_settings_form_submit($form, $form_state) {

  $pane = $form_state['checkout_pane'];
  if($submit_function = dcart_hook_callback($pane, 'settings_form_submit')) {
    $submit_function($form, $form_state);
  }
  dcart_checkout_pane_settings_save($pane['name'], $form_state['values']);
  drupal_set_message(t('The configuration options have been saved.'));
}

<?php

function dcart_currency_form($form, &$form_state) {
  
  drupal_set_title(t('Currency settings'));
  
  $form['#tree'] = TRUE;
  $settings = dcart_currency_settings();
  $default_currency = dcart_default_currency();
  
  $options = $active = array();
  foreach(dcart_currencies() as $currency_code => $currency) {
    $title = check_plain($currency['name']);
    $form['currencies']['name'][$currency_code] = array('#markup' => $title);
    $form['currencies']['code'][$currency_code] = array('#markup' => $currency_code);
    $options[$currency_code] = $title;
    
    if(!empty($currency['active'])) {
      $active[] = $currency_code;
    }

    $form['currencies'][$currency_code]['conversion_rate'] = array(
      '#type' => 'textfield',
      '#title' => t('Conversion rate @currency for @default', array('@currency' => $currency_code, '@default' => $default_currency)),
      '#default_value' => $currency['conversion_rate'],
      '#element_validate' => array('element_validate_number'),
      '#size' => 3,
      '#disabled' => !dcart_currency_access('rate'),
    );
  }

  $active[] = $default_currency;

  $form['currencies']['default'] = array(
    '#type' => 'radios',
    '#options' => $options,
    '#default_value' => $default_currency,
    '#disabled' => !dcart_currency_access('default'),
  );
    
  $form['currencies']['active'] = array(
    '#type' => 'checkboxes',
    '#options' => $options,
    '#default_value' => $active,
    '#disabled' => !dcart_currency_access('active'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}

function theme_dcart_currency_form($variables) {
  $rows = array();
  $form = $variables['form'];
  $default_currency = dcart_default_currency();
  foreach ($form['currencies']['name'] as $key => $element) {
    if (is_array($element) && element_child($key)) {
      $title = drupal_render($form['currencies']['name'][$key]);
      $form['currencies']['active'][$key]['#title'] = t('Enable !title', array('!title' => $title));
      $form['currencies']['active'][$key]['#title_display'] = 'invisible';
      $form['currencies']['default'][$key]['#title'] = t('Set !title as default', array('!title' => $title));
      $form['currencies']['default'][$key]['#title_display'] = 'invisible';
      $form['currencies'][$key]['conversion_rate']['#title_display'] = 'invisible';

      if($key == $default_currency) {
        $form['currencies'][$key]['conversion_rate']['#access'] = FALSE;
      }
      
      $rows[$key] = array(
        $title,
        drupal_render($form['currencies']['code'][$key]),
        drupal_render($form['currencies']['active'][$key]),
        drupal_render($form['currencies']['default'][$key]),
        drupal_render($form['currencies'][$key]['conversion_rate']),
      );
    }
  }
  $header = array(t('Currency'), t('Code'), t('Active'), t('Default'), t('Conversion rate for %default', array('%default' => $default_currency)));
  return theme('table', array('rows' => $rows, 'header' => $header)) . drupal_render_children($form);
}

function dcart_currency_form_validate($form, &$form_state) {

  $form_state['default_currency'] = $form_state['values']['currencies']['default'];
  unset($form_state['values']['currencies']['default']);

  $active = $form_state['values']['currencies']['active'];
  unset($form_state['values']['currencies']['active']);

  foreach($form_state['values']['currencies'] as $currency_code => $currency) {
    $rate = trim($form_state['values']['currencies'][$currency_code]['conversion_rate']);
    $form_state['values']['currencies'][$currency_code]['active'] = !empty($active[$currency_code]);
    $form_state['values']['currencies'][$currency_code]['conversion_rate'] = ($rate == '') ? 1 : $rate;
  }

  $form_state['values']['currencies'][$form_state['default_currency']]['active'] = TRUE;
}

function dcart_currency_form_submit($form, &$form_state) {
  dcart_currency_settings_save($form_state['values']['currencies'], $form_state['default_currency']);
  drupal_set_message(t('The configuration options have been saved.'));
}

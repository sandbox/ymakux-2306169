<?php

function dcart_dcart_currency_info() {
  return array(
    'USD' => array(
      'symbol' => '$',
      'name' => t('United States Dollar'),
      'numeric_code' => '840',
      'symbol_placement' => 'before',
      'minor_unit' => t('Cent'),
      'major_unit' => t('Dollar'),
    ),
    'EUR' => array(
      'symbol' => '€',
      'name' => t('Euro'),
      'symbol_placement' => 'after',
      'numeric_code' => '978',
      'minor_unit' => t('Cent'),
      'major_unit' => t('Euro'),
    ),
    'UAH' => array(
      'symbol' => 'грн',
      'name' => t('Ukrainian Hryvnia'),
      'numeric_code' => '980',
      'symbol_placement' => 'after',
      'minor_unit' => t('Kopiyka'),
      'major_unit' => t('Hryvnia'),
    ),
  );
}

function dcart_currencies() {
  $currencies = &drupal_static(__FUNCTION__);
  if (!isset($currencies)) {
    $currencies = module_invoke_all('dcart_currency_info');
    $settings = dcart_currency_settings();
    
    foreach($currencies as $code => &$currency) {
      if(!isset($currency['code'])) {
        $currency['code'] = $code;
      }
      
      $currency += dcart_currency_default();
      
      if(isset($settings[$code])) {
        $currency = $settings[$code] + $currency;
      }
    }
    drupal_alter('dcart_currency_info', $currencies);
    ksort($currencies);
  }
  return $currencies;
}

function dcart_currencies_get($enabled = FALSE, $currency = NULL) {
  $currencies = dcart_currencies();
  if (isset($currency)) {
    return isset($currencies[$currency]) ? $currencies[$currency] : array();
  }
  if ($enabled) {
    foreach ($currencies as $code => $info) {
      if (empty($info['active'])) {
        unset($currencies[$code]);
      }
    }
  }
  return $currencies;
}

function dcart_currency_options($enabled = FALSE) {
  $options = array();
  foreach (dcart_currencies($enabled, NULL) as $code => $info) {
    $options[$code] = $info['name'];
  }
  return $options;
}

function dcart_currency_load($currency_code = NULL) {
  $currencies = dcart_currencies();
  if (!isset($currency_code)) {
    $currency_code = dcart_default_currency();
  }
  return isset($currencies[$currency_code]) ? $currencies[$currency_code] : array();
}

function dcart_currency_default() {
  return array(
    'symbol' => '',
    'decimals' => 2,
    'thousands_separator' => ',', 
    'decimal_separator' => '.', 
    'symbol_placement' => '', 
    'symbol_spacer' => ' ', 
    'code_placement' => 'after', 
    'code_spacer' => ' ', 
    'format_callback' => '', 
    'conversion_callback' => '',
    'conversion_rate' => 1,
    'rounding_step' => 0,
    'active' => TRUE,
  );
}

function dcart_currency_convert($amount, $currency_code, $target_currency_code = NULL) {
  $currency = dcart_currency_load($currency_code);
  
  $target_currency_code = isset($target_currency_code) ? $target_currency_code : dcart_default_currency();

  // Invoke the custom conversion callback if specified.
  if (!empty($currency['conversion_callback'])) {
    return $currency['conversion_callback']($amount, $currency_code, $target_currency_code);
  }

  $target_currency = dcart_currency_load($target_currency_code);
  $exponent = $target_currency['decimals'] - $currency['decimals'];
  $amount *= pow(10, $exponent);

  return $amount * ($currency['conversion_rate'] / $target_currency['conversion_rate']);
}

function dcart_default_currency() {
  return variable_get('dcart_default_currency', 'USD');
}

function dcart_currency_settings($currency_code = NULL) {
  $settings = variable_get('dcart_currency_settings', array());
  if(isset($currency_code)) {
    return isset($settings[$currency_code]) ? $settings[$currency_code] : array();
  }
  return $settings;
}

function dcart_currency_settings_save($settings = array(), $default_currency = NULL) {
  foreach (module_implements('dcart_currency_settings_save') as $module) {
    $function = $module . '_dcart_currency_settings_save';
    $function($settings, $default_currency);
  }
  
  if(!empty($settings)) {
    variable_set('dcart_currency_settings', $settings);
  }
  
  if(isset($default_currency)) {
    variable_set('dcart_default_currency', $default_currency);
  }
}

function dcart_currency_access($op) {
  switch($op) {
    case 'edit':
      return user_access('dcart currency set default') || user_access('dcart currency exchange rate');
      break;
    case 'default':
      return user_access('dcart currency set default');
      break;
    case 'rate':
      return user_access('dcart currency exchange rate');
      break;
    case 'active':
      return user_access('dcart currency set active');
      break;
  }
}
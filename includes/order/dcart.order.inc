<?php

function dcart_dcart_order_status_info() {
  $order_statuses['checkout_checkout'] = array(
    'title' => t('Checkout: Checkout'),
    'display_title' => t('In checkout'),
    'description' => t('Order still in checkout process'),
  );
  $order_statuses['awaiting_payment'] = array(
    'title' => t('Awaiting payment'),
    'description' => t('Checkout complete, order is awaiting payment.'),
  );
  $order_statuses['checkout_canceled'] = array(
    'title' => t('Checkout canceled'),
    'description' => t('Checkout canceled for some reason during checkout'),
  );
  $order_statuses['canceled'] = array(
    'title' => t('Canceled by administrator'),
    'display_title' => t('Order has been canceled by administrator'),
    'description' => t('For some reason order has been canceled by store\'s administration.'),
  );
  $order_statuses['pending'] = array(
    'title' => t('Pending'),
    'description' => t('Order will be considered by store\'s administration.'),
  );
  $order_statuses['processing'] = array(
    'title' => t('Processing'),
    'description' => t('Currently order is processing by store\'s administration.'),
  );
  $order_statuses['completed'] = array(
    'title' => t('Completed'),
    'description' => t('Order has been fulfilled and sent to customer.'),
  );
  return $order_statuses;
}

function dcart_dcart_address_field_type_info() {
  return array(
    'first_name' => array('title' => t('First name')),
    'middle_name' => array('title' => t('Middle name')),
    'last_name' => array('title' => t('Last name')),
    'city' => array('title' => t('City')),
    'thoroughfare' => array('title' => t('Street address')),
    'premise' => array('title' => t('Apartment, suite, box...')),
    'postal_code' => array('title' => t('Postal code / ZIP Code')),
    'phone' => array('title' => t('Phone')),
    'email' => array('title' => t('E-mail')),
  );
}

function dcart_order_address_field_types() {
  $types = &drupal_static(__FUNCTION__);
  if (!isset($types)) {
    $types = module_invoke_all('dcart_address_field_type_info');
    drupal_alter('dcart_address_field_type_info', $types);
  }
  return $types;
}

function dcart_order_address_field_type_options() {
  $options = array();
  foreach(dcart_order_address_field_types() as $type => $data) {
    $options[$type] = $data['title'];
  }
  return $options;
}

function dcart_order_statuses() {
  $statuses = &drupal_static(__FUNCTION__);
  if (!isset($statuses)) {
    $statuses = module_invoke_all('dcart_order_status_info');
    drupal_alter('dcart_order_status_info', $statuses);
  }
  return $statuses;
}

function dcart_order_statuses_get($checkout = NULL) {
  $statuses = dcart_order_statuses();
  foreach($statuses as $status_id => $status) {
    if($checkout === TRUE && substr($status_id, 0, 9) != 'checkout_') {
      unset($statuses[$status_id]);
    }
    elseif($checkout === FALSE && substr($status_id, 0, 9) == 'checkout_') {
      unset($statuses[$status_id]);
    }
  }
  return $statuses;
}

function dcart_order_status_options($exclude_checkout = FALSE) {
  $options = array();
  foreach (dcart_order_statuses() as $status_name => $status_data) {
    if ($exclude_checkout && substr($status_name, 0, 9) == 'checkout_') {
      continue;
    }
    $options[$status_name] = check_plain($status_data['title']);
  }
  return $options;
}

/**
 * Implements hook_entity_info().
 */
function dcart_entity_info() {
  $return = array(
    'dcart_order' => array(
      'label' => t('Drupal Cart order'),
      'base table' => 'dcart_order',
      'load hook' => 'dcart_order_load',
      'uri callback' => 'dcart_order_uri',
      'fieldable' => TRUE,
      'entity keys' => array(
        'id' => 'order_id',
      ),
      'view modes' => array(
        'administrator' => array(
          'label' => t('Administrator'),
          'custom settings' => TRUE,
        ),
        'customer' => array(
          'label' => t('Customer'),
          'custom settings' => TRUE,
        ),
      ),
      'bundles' => array(
        'dcart_order' => array(
          'label' => t('Dcart order'),
          'admin' => array(
            'path' => 'admin/dcart/settings/order/bundle',
            'access arguments' => array('dcart administer order bundle'),
          ),
        ),
      ),
    ),
  );

  return $return;
}

/**
 * Entity uri callback: gives modules a chance to specify a path for an order.
 */
function dcart_order_uri($order) {
  return array(
    'path' => 'order/' . $order->order_id,
  );
}

function dcart_order_save($order) {
  
  $transaction = db_transaction();
  
  try {
    // Load the stored entity, if any.
    if (!empty($order->order_id) && !isset($order->original)) {
      $order->original = entity_load_unchanged('dcart_order', $order->order_id);
    }

    field_attach_presave('dcart_order', $order);
    module_invoke_all('dcart_order_presave', $order);
    module_invoke_all('entity_presave', $order, 'dcart_order');

    if (empty($order->created)) {
      $order->created = REQUEST_TIME;
    }
    $order->changed = REQUEST_TIME;

    if (empty($order->order_id)) {
      $op = 'insert';
      $status = drupal_write_record('dcart_order', $order);
      field_attach_insert('dcart_order', $order);
    }
    else {
      $op = 'update';
      $order->total = dcart_order_calculate_total($order);
      $status = drupal_write_record('dcart_order', $order, 'order_id');
      field_attach_update('dcart_order', $order);
    }

    // Reset the order static variables.
    drupal_static_reset();

    // Invoke the taxonomy hooks.
    module_invoke_all("dcart_order_$op", $order);
    module_invoke_all("entity_$op", $order, 'dcart_order');
    unset($order->original);
    return $status;
  }
  catch(Exception $e) {
    $transaction->rollback();
    watchdog_exception('dcart_order', $e);
    throw $e;
  }
}

function dcart_order_new() {
  global $user;
  $order = new stdClass();
  $order->order_id = NULL;
  $order->uid = '';
  $order->mail = '';
  $order->creator = $user->uid;
  $order->status = 'pending';
  $order->domain = $_SERVER['SERVER_NAME'];
  $order->created = REQUEST_TIME;
  $order->changed = REQUEST_TIME;
  $order->data = array();
  $order->currency_code = dcart_default_currency();
  return $order;
}

function dcart_order_load_multiple($order_ids = array(), $conditions = array(), $reset = FALSE) {
  return entity_load('dcart_order', $order_ids, $conditions, $reset);
}

function dcart_order_load($order_id = NULL, $conditions = array(), $reset = FALSE) {
  $order_ids = isset($order_id) ? array($order_id) : array();
  $order = dcart_order_load_multiple($order_ids, $conditions, $reset);
  return $order ? reset($order) : FALSE;
}

function dcart_order_delete($order_id) {
  dcart_order_delete_multiple(array($order_id));
}

function dcart_order_delete_multiple($order_ids) {
  $transaction = db_transaction();
  if (!empty($order_ids)) {
    $orders = dcart_order_load_multiple($order_ids, array());
    try {
      foreach ($orders as $order_id => $order) {
        module_invoke_all('dcart_order_delete', $order);
        module_invoke_all('entity_delete', $order, 'dcart_order');
        field_attach_delete('dcart_order', $order);
      }
      db_delete('dcart_order')->condition('order_id', $order_ids, 'IN')->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog_exception('dcart_order', $e);
      throw $e;
    }
    entity_get_controller('dcart_order')->resetCache();
  }
}

function dcart_order_access($operation, $order) {

  $order_is_my = ($order->uid == dcart_customer_get());

  switch ($operation) {
    case 'delete':
      if(user_access('dcart delete any order') || (user_access('dcart delete own order') && $order_is_my)) {
        return TRUE;
      } 
      break;
    case 'edit':
      if(user_access('dcart edit any order') || (user_access('dcart edit own order') && $order_is_my)) {
        return TRUE;
      } 
      break;
    case 'create':
      return user_access('dcart create order');
      break;
    case 'view' :
      if(user_access('dcart view any order') || (user_access('dcart view own order') && $order_is_my)) {
        return TRUE;
      }
      break;
  }
  return FALSE;
}

function dcart_order_view($order, $view_mode = 'customer', $langcode = NULL) {
  
  drupal_set_title(t('Order #@order_id, @created', array(
    '@order_id' => $order->order_id,
    '@created' => format_date($order->created, 'short'),
  )));
  
  dcart_order_tag_new($order);
  
  if (!isset($langcode)) {
    $langcode = $GLOBALS['language_content']->language;
  }
  
  // Remove previously built content, if exists.
  $order->content = array();

  // Allow modules to change the view mode.
  $context = array(
    'entity_type' => 'dcart_order',
    'entity' => $order,
    'langcode' => $langcode,
  );
  
  drupal_alter('entity_view_mode', $view_mode, $context);

  // Build fields content.
  field_attach_prepare_view('dcart_order', array($order->order_id => $order), $view_mode, $langcode);
  entity_prepare_view('dcart_order', array($order->order_id => $order), $langcode);
  $order->content += field_attach_view('dcart_order', $order, $view_mode, $langcode);

  // Allow modules to make their own additions to the order.
  module_invoke_all('dcart_order_view', $order, $view_mode, $langcode);
  module_invoke_all('entity_view', $order, 'dcart_order', $view_mode, $langcode);

  // Make sure the current view mode is stored if no module has already
  // populated the related key.
  if (empty($node->content['#view_mode'])) {
    $node->content['#view_mode'] = $view_mode;
  }

  $build = $order->content;
  // We don't need duplicate rendering info in order->content.
  unset($order->content);

  $build += array(
    '#theme' => 'dcart_order',
    '#order' => $order,
    '#view_mode' => $view_mode,
    '#language' => $langcode,
  );

  // Allow modules to modify the structured order.
  $type = 'dcart_order';
  drupal_alter(array('dcart_order_view', 'entity_view'), $build, $type);
  return $build;
}

function dcart_preprocess_dcart_order(&$variables) {
  global $theme_key;
  $variables['site_name'] = check_plain(variable_get('site_name', ''));
  $variables['site_slogan'] = check_plain(variable_get('site_slogan', ''));
  $variables['view_mode'] = $variables['elements']['#view_mode'];
  $variables['order'] = $order = $variables['elements']['#order'];
  $variables['created'] = format_date($order->created);
  $variables['changed'] = format_date($order->changed);
  $variables['order_extra']['comments'] = dcart_checkout_pane_fields_view('comment', $order, TRUE);
  $uri = entity_uri('dcart_order', $order);
  $variables['order_url']  = url($uri['path'], $uri['options']);

  $variables['site_logo'] = '';
  if ($logo = theme_get_setting('logo', $theme_key)) {
    $variables['site_logo'] = theme('image', array('path' => $logo));
  }

  // Flatten the order object's member fields.
  $variables = array_merge((array) $order, $variables);
  
  $statuses = dcart_order_statuses();
  $status = t('Unknown');

  if(isset($statuses[$variables['status']]['display_title'])) {
    $status = check_plain($statuses[$variables['status']]['display_title']);
  }
  elseif(isset($statuses[$variables['status']]['title'])) {
    $status = check_plain($statuses[$variables['status']]['title']);
  }
    
  $variables['order_status'] = $status;
  $variables['products'] = $variables['line_items'] = $variables['shipping_address'] = array();
  $instances = field_info_instances('dcart_order', 'dcart_order');

  $properties = dcart_attribute_properties();
  if(!empty($order->data['products'])) {
    
    $product_amounts = array();
    foreach ($order->data['products'] as $id => $product) {

      if ($node = dcart_get_node_by_sku($product['sku'], TRUE)) {
        $attribute = dcart_product_attributes_get($node, $product['sku']);
        $product_title = check_plain($node->title);
        
        if(empty($node->status)) {
          $product_title += ' ' . t('unavailable');
        }
        
        $product_price = $product['amount'];
        $product_count = $product['count'];
        $product_amount = $product_price * $product_count;
        $product_amounts[] = $product_amount;
        
        $product_attributes = theme('dcart_product_attributes', array(
          'node' => $node,
          'attribute' => $attribute,
        ));
        
        $variables['products'][$id] = array(
          'sku' => $product['sku'],
          'title' => $product_title,
          'attributes' => $product_attributes,
          'count' => $product_count,
          'price' => dcart_price_format_amount($product_price, $order->currency_code, TRUE),
          'total' => dcart_price_format_amount($product_amount, $order->currency_code, TRUE),
        );
      }
    }
  }
  
  $variables['order_items'] = $variables['products'];
  $variables['product_total'] = dcart_order_calculate_product_total($product_amounts);
  $variables['order_items'] = array_merge($variables['order_items'], array(
    'subtotal' => array(
      'title' => t('Subtotal'),
      'amount' => dcart_price_format_amount($variables['product_total'], $order->currency_code, TRUE))));
    
  $variables['order_total'] = theme('dcart_price', array('amount' => $order->total, 'currency_code' => $order->currency_code));

  $line_item_types = dcart_line_item_types();
  foreach(dcart_order_line_items_get($order) as $type => $amount) {
    $title = isset($line_item_types[$type]['display_title']) ? check_plain($line_item_types[$type]['display_title']) : check_plain($line_item_types[$type]['title']);
    $variables['line_items'][$type] = array(
     'title' => $title,
     'amount' => dcart_price_format_amount((int) $amount, $order->currency_code, TRUE));
    $variables['order_items'][$type] = $variables['line_items'][$type];
  }

  $variables['order_items'] = array_merge($variables['order_items'], array(
  'total' => array('title' => t('Total'), 'amount' => $variables['order_total'])));

  $shipping_fields = dcart_order_checkout_fields_get($order, 'shipping');
  foreach($shipping_fields as $field_name => $field_value) {
    $variables['shipping_address'][$field_name] = array(
    'title' => dcart_translatable_string($instances[$field_name]['label']) ? t($instances[$field_name]['label']) : check_plain($instances[$field_name]['label']),
    'value' => $field_value);
  }

  drupal_add_library('dcart', 'print_element');

  $variables['address'] = theme('dcart_address');
  $variables['content'] = array();
  field_attach_preprocess('dcart_order', $order, $variables['content'], $variables);
  $variables['theme_hook_suggestions'] = array('order__' . $variables['view_mode']);
}

function dcart_order_product_delete($product_id = NULL, $order_id = NULL) {
  $query = db_delete('dcart_order_product');
  if (isset($order_id)) {
    $query->condition('order_id', $order_id);
  }
  elseif(isset($product_id)) {
    $query->condition('id', (array) $product_id, 'IN');
  }
  return $query->execute();
}

function dcart_order_product_default() {
   return array(
    'count' => 1,
    'currency_code' => dcart_default_currency(),
    'uid' => dcart_customer_get(),
    'data' => array(),
    'id' => NULL,
  );
}

function dcart_order_product_save($product) {
  $product += dcart_order_product_default();
  foreach (module_implements('dcart_order_product_presave') as $module) {
    $function = $module . '_dcart_order_product_presave';
    $function($product);
  }
  if (is_array($product['data'])) {
    $product['data'] = serialize($product['data']);
  }
  return db_merge('dcart_order_product')
    ->key(array('id' => $product['id']))
    ->fields($product)
    ->execute();
}

function dcart_order_product_get($order_id = NULL, $sku = NULL, $uid = NULL, $product_id = NULL) {
  $results = array();
  $query = db_select('dcart_order_product', 'p')->fields('p');

  if (isset($order_id)) {
    $query->condition('p.order_id', $order_id);
  }
  
  if (isset($sku)) {
    $query->condition('p.sku', $sku);
  }
  
  if (isset($uid)) {
    $query->condition('p.uid', $uid);
  }
  
  if(isset($product_id)) {
    $query->condition('p.id', $product_id);
  }

  if ($result = $query->execute()->fetchAll()) {
    foreach ($result as $key => $data) {
      $results[$data->id] =  (array) $data;
      $results[$data->id]['data'] = unserialize($data->data);
    }
  }
  
  if(isset($product_id)) {
    return isset($results[$product_id]) ? $results[$product_id] : array();
  }
  
  return $results;
}

function dcart_order_line_items_get($order, $line_item = NULL) {

  if(is_numeric($order)) {
    $order = dcart_order_load($order);
  }

  $line_items = array();
  if (!empty($order->data['line_items'])) {
    $line_item_types = dcart_line_item_types();
    foreach($order->data['line_items'] as $line_item_type => $line_item_value) {
      if (isset($line_item_types[$line_item_type])) {
        $line_items[$line_item_type] = $line_item_value;
      }
    }
  }
  
  if(isset($line_item)) {
    return isset($line_items[$line_item]) ? $line_items[$line_item] : NULL;    
  }
  
  return $line_items;
}

function dcart_order_line_items_set($order, $line_item) {

  $load = is_numeric($order);
  
  if($load) {
    $order = dcart_order_load((int) $order);
  }
  
  $line_item_types = dcart_line_item_types();
  foreach((array) $line_item as $type => $value) {
    if(isset($line_item_types[$type])) {
      $order->data['line_items'][$type] = $value;
    }
  }
  if($load) {
    dcart_order_save($order);
  }
}


function dcart_order_status_set($order, $status) {
  $statuses = dcart_order_statuses();
  if (isset($statuses[$status])) {
    if(is_numeric($order)) {
      $order = dcart_order_load($order);
      $order->status = $status;
      dcart_order_save($order);
    }
    else {
      $order->status = $status;
    }
  }
}

function dcart_order_in_checkout($order) {
  if(is_numeric($order)) {
    $order = dcart_order_load($order);
  }
  return (substr($order->status, 0, 9) == 'checkout_');
} 

function dcart_order_checkout_fields_get($order, $address = FALSE) {
  $checkout_fields = array();
  foreach (dcart_order_checkout_fields($address) as $field_name => $field_data) {
    if ($items = field_get_items('dcart_order', $order, $field_name)) {
      $item = reset($items);
      $checkout_fields[$field_name] = check_plain($item['value']);
    }
  }
  return $checkout_fields;
}

function dcart_order_checkout_fields($address_fields = FALSE, $address_types = array()) {
  $fields = array();
  foreach (field_info_instances('dcart_order', 'dcart_order') as $field_name => $field_data) {
    if (!empty($field_data['dcart_settings']['checkout_pane'])) {
      if($address_fields) {
        if(empty($field_data['dcart_settings']['checkout_address'])) {
          continue;
        }
        if($address_types) {
          if(empty($field_data['dcart_settings']['address_type'])) {
            continue;
          }
          if(!in_array($field_data['dcart_settings']['address_type'], (array) $address_types)) {
            continue;
          }
        }
      }
      $fields[$field_name] = $field_data;
    }
  }
  return $fields;
}

function dcart_order_address_field_names($address_types) {
  $names = array();
  foreach(dcart_order_checkout_fields(TRUE, (array) $address_types) as $field_name => $field_data) {
    $names[$field_data['dcart_settings']['address_type']] = $field_name;
  }
  return $names;
}

function dcart_order_checkout_fields_options($address_fields = FALSE) {
  $options = array();
  foreach(dcart_order_checkout_fields() as $field_name => $field_data) {
    $options[$field_name] = check_plain($field_data['label']) . ' (' . $field_name . ')';
  }
  return $options;
}

function dcart_order_title() {
  $args = func_get_args();
  $order = $args[0];
  if (!isset($args[1])) {
    if (isset($order->order_id)) {
      return t('Order @order_id', array('@order_id' => $order->order_id));
    }
  }
}

function dcart_preprocess_dcart_order_edit_form(&$variables) {

  $form = $variables['form'];
  $variables['creator'] = $form['creator'];
  $variables['owner'] = $form['uid'];
  $variables['products'] = !empty($form['components']['products']) ? $form['components']['products'] : array();
  $variables['line_items'] = !empty($form['components']['line_items']) ? $form['components']['line_items'] : array();
  $variables['update'] = $form['update'];
  $variables['status'] = $form['status'];
  $variables['mail'] = $form['mail'];
  $variables['buttons'] = $form['actions'];
  $variables['add'] = $form['add'];
  $variables['total'] = t('Order total: !total', array('!total' => theme('dcart_price', array('amount' => $form['total']['#value']))));
  $variables['created'] = $form['created'];
  $variables['send_mail'] = $form['send_mail'];
  
  $all_fields = $address_fields = array();
  foreach(field_info_instances('dcart_order', 'dcart_order') as $field_name => $field_data) {
    if(empty($form[$field_name])) {
      continue; 
    }
    
    $required = !empty($field_data['required']) ? ' <span class="required">' . t('(required)') . '</span>' : '';
    $all_fields[$field_name] = array(check_plain($field_data['label']) . $required, render($form[$field_name]));
    if(!empty($field_data['dcart_settings']['checkout_field'])) {
      if(!empty($field_data['dcart_settings']['checkout_address'])) {
        $address_fields[$field_name] = array(check_plain($field_data['label']) . $required, render($form[$field_name]));
      }
    }
  }
  
  $variables['fields']['#markup'] = theme('table', array('rows' => $all_fields, 'caption' => t('Order fields'), 'attributes' => array('class' => array('order-fields'))));
  $variables['address_fields']['#markup'] = theme('table', array('rows' => $address_fields, 'caption' => t('Address'), 'attributes' => array('class' => array('order-fields'))));
  
  foreach(element_children($variables['form']) as $element) {
    if (in_array($element, array('form_build_id', 'form_token', 'form_id'))) {
      continue;
    }
    hide($variables['form'][$element]);
  }
}

function dcart_order_calculate_line_items_total($line_items) {
  $total = 0;
  foreach($line_items as $name => $amount) {
    $total += (int) $amount;
  }
  return $total;
}

function dcart_order_calculate_product_total($products) {

  $total = 0;
  foreach($products as $sku => $amount) {
    $total += (int) $amount;
  }
  return $total;
}

function dcart_order_calculate_products($products, $context = array(), $currency_code = NULL) {
  $calculate_products = array();
  $currency_code = isset($currency_code) ? $currency_code : dcart_default_currency();
  if ($currency = dcart_currency_load($currency_code)) {
    foreach($products as $product) {
      $amount = $product['amount'];
      if ($product['currency_code'] != $currency['code']) {
        $amount = dcart_currency_convert($amount, $product['currency_code'], $currency_code);
      }
      $calculate_products[] = ($amount * $product['count']);
    }
  }
  return $calculate_products;
}

function dcart_order_calculate_line_items($line_items, $context = array(), $currency_code = NULL) {

  $calculation = array();
  $line_item_types = dcart_line_item_types();

  uasort($line_items, 'drupal_sort_weight');
  foreach($line_items as $name => $data) {
    if (isset($data['value']) && isset($line_item_types[$name]) && ($callback = dcart_hook_callback($line_item_types[$name], 'calculate'))) {
      $calculation[$name]['amount'] = $callback($data['value'], $context);
      $calculation[$name]['value'] = $data['value'];
    }
  }
  return $calculation;
}

function dcart_order_calculate_total($order) {
  $products_total = dcart_order_product_total($order);
  $line_items_total = dcart_order_line_items_total($order);
  return $products_total + $line_items_total;
}

function dcart_order_product_total($order) {
  if(is_numeric($order)) {
    $order = dcart_order_load($order);
  }
  
  $total = 0;
  if(!empty($order->data['products'])) {
    $products = dcart_order_calculate_products($order->data['products']);
    $total = dcart_order_calculate_product_total($products);
  }
  return (int) $total;
}

function dcart_order_line_items_total($order) {
  if(is_numeric($order)) {
    $order = dcart_order_load($order);
  }
  
  $total = 0;
  if(!empty($order->data['line_items'])) {
    $line_items = dcart_order_calculate_line_items($order->data['line_items']);
    $total = dcart_order_calculate_line_items_total($line_items);
  }
  return (int) $total;
}


function dcart_order_checkout_incomplete($uid) {
  $order_id = FALSE;
  $checkout_statuses = dcart_order_statuses_get(TRUE);
  unset($checkout_statuses[DCART_CHECKOUT_STATUS_CANCELED]);
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'dcart_order');
  $query->propertyCondition('uid', $uid);
  $query->propertyCondition('status', array_keys($checkout_statuses), 'IN');
  $query->propertyOrderBy('order_id', 'DESC');
  $result = $query->execute();
  
  if (!empty($result['dcart_order'])) {
    reset($result['dcart_order']);
    $order_id = key($result['dcart_order']);
  }
  return $order_id;
}

function _dcart_order_fields_extra_validation($fields, $form, &$form_state) {
  $extra_validators = dcart_field_extra_validators();
  foreach($fields as $field_name => $field_data) {
    if(!empty($field_data['dcart_settings']['field_validator'])) {
      $validator = $field_data['dcart_settings']['field_validator'];
      if(isset($extra_validators[$validator]) && $extra_validator_callback = dcart_hook_callback($extra_validators[$validator])) {
        $extra_validator_callback($form[$field_name], $form_state);
      }
    }
  }
}

function dcart_order_last_viewed($order_id) {
  global $user;
  $history = &drupal_static(__FUNCTION__, array());
  if (!isset($history[$order_id])) {
    $history[$order_id] = db_query("SELECT timestamp FROM {dcart_order_history} WHERE uid = :uid AND order_id = :order_id", array(':uid' => $user->uid, ':order_id' => $order_id))->fetchObject();
  }
  return (isset($history[$order_id]->timestamp) ? $history[$order_id]->timestamp : 0);
}

function dcart_order_tag_new($order) {
  global $user;
  if ($user->uid) {
    db_merge('dcart_order_history')->key(array(
      'uid' => $user->uid,
      'order_id' => $order->order_id,
    ))->fields(array('timestamp' => REQUEST_TIME))->execute();
  }
}

function dcart_order_mark($order_id, $timestamp) {
  global $user;
  $cache = &drupal_static(__FUNCTION__, array());

  if (!$user->uid) {
    return 0;
  }
  if (!isset($cache[$order_id])) {
    $cache[$order_id] = dcart_order_last_viewed($order_id);
  }
  if ($cache[$order_id] == 0 && $timestamp > DCART_ORDER_NEW_LIMIT) {
    return 1;
  }
  elseif ($timestamp > $cache[$order_id] && $timestamp > DCART_ORDER_NEW_LIMIT) {
    return 2;
  }
  return 0;
}

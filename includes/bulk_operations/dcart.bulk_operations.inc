<?php

function dcart_dcart_bulk_operations_info() {
  $file_path = drupal_get_path('module', 'dcart') . '/includes/bulk_operations/callbacks';
  return array(
    'order_status' => array(
      'type' => 'order',
      'title' => t('Change status'),
      'batch_limit' => 10,
      'description' => t('Change order status for all selected orders'),
      'callback' => 'dcart_bulk_operations_order_status',
      'file' => $file_path . '/order_status.inc',
    ),
    'order_delete' => array(
      'type' => 'order',
      'title' => t('Delete'),
      'batch_limit' => 10,
      'description' => t('Completely delete all selected orders from database.'),
      'callback' => 'dcart_bulk_operations_order_delete',
      'file' => $file_path . '/order_delete.inc',
    ),
    'product_delete' => array(
      'type' => 'product',
      'title' => t('Delete selected'),
      'batch_limit' => 10,
      'description' => t('Completely delete all selected products from database.'),
      'callback' => 'dcart_bulk_operations_product_delete',
      'file' => $file_path . '/product_delete.inc',
    ),
    'product_change_price' => array(
      'type' => 'product',
      'title' => t('Change price'),
      'batch_limit' => 10,
      'description' => t('Change product price'),
      'callback' => 'dcart_bulk_operations_product_change_price',
      'file' => $file_path . '/product_change_price.inc',
    ),
  );
}

function dcart_bulk_operations() {
  $operations = &drupal_static(__FUNCTION__);
  if (!isset($operations)) {
    $operations = module_invoke_all('dcart_bulk_operations_info');
    drupal_alter('dcart_bulk_operations_info', $operations);
  }
  return $operations;
}

function dcart_bulk_operations_get($operation = NULL, $type = NULL) {
  $operations = dcart_bulk_operations();
  if (isset($type) && $operations) {
    foreach ($operations as $name => &$data) {
      if ($data['type'] != $type) {
        unset($operations[$name]);
      }
    }
  }
  if ($operation) {
    return !empty($operations[$operation]) ? $operations[$operation] : array();
  }
  return $operations;
}

function dcart_bulk_operations_options($type = NULL, $operation = NULL) {
  $options = array();
  foreach (dcart_bulk_operations_get($operation, $type) as $operation_name => $operation_info) {
    $options[$operation_name] = check_plain($operation_info['title']);
  }
  return $options;
}

function dcart_bulk_operations_form($form, &$form_state) {

  $form['#tree'] = TRUE;
  extract($_SESSION['dcart_bulk_operations']);

  $operation = dcart_bulk_operations_get($action);

  $question = t('Are you sure you want to perform %action action?', array('%action' => $operation['title']));
  drupal_set_title($question, PASS_THROUGH);
  
  if($form_callback = dcart_hook_callback($operation, 'form')) {
    $form_callback($form, $form_state, $items, $operation);
  }
  
  $form['bulk_operations']['items'] = array(
    '#type' => 'value',
    '#value' => $items,
  );
  
  $form['bulk_operations']['operation'] = array(
    '#type' => 'value',
    '#value' => $operation,
  );
  
  $form['agree'] = array(
    '#title' => t('Confirm'),
    '#type' => 'checkbox',
    '#default_value' => 0,
    '#required' => TRUE,
  );

  $form['actions'] = array('#type' => 'actions');

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
    '#submit' => array('dcart_bulk_operations_form_submit'),
    '#validate' => array('dcart_bulk_operations_form_validate'),
    '#states' => array(
      'enabled' => array(
        ':input[name="agree"]' => array('checked' => TRUE),
      ),
    ),
  );
  
  if (isset($_GET['destination'])) {
    $form['actions']['cancel'] = array(
      '#type' => 'link',
      '#title' => t('Cancel'),
      '#href' => $_GET['destination'],
    );
  }

  return $form;
}

function dcart_bulk_operations_form_validate($form, &$form_state) {
  $operation = $form_state['values']['bulk_operations']['operation'];
  if($validate_callback = dcart_hook_callback($operation, 'form_validate')) {
    $validate_callback($form, $form_state);
  }
}


function dcart_bulk_operations_form_submit($form, &$form_state) {

  $arguments = $form_state['values']['bulk_operations'];
  
  extract($arguments);
  $arguments += array('count' => count($items));
  
  if (!empty($operation['batch_limit'])) {
    $items = array_chunk($items, $operation['batch_limit']);
  }

  $operations = array();
  foreach ($items as $item) {
    $arguments['item'] = $item;
    $operations[] = array($operation['callback'], array($arguments));
  }  

  $batch = array('operations' => $operations, 'finished' => $operation['callback'] . '_finished');
  
  if (!empty($operation['file'])) {
    $batch['file'] = $operation['file'];
  }

  batch_set($batch);
}

function dcart_bulk_operations_validate($form, &$form_state) {
  
  form_state_values_clean($form_state);
  unset($_SESSION['dcart_bulk_operations']);
  
  $form_state['values']['items'] = array_filter($form_state['values']['items']);
  
  if (empty($form_state['values']['items'])) {
    form_set_error('', t('You must select at least one item'));
  }
}

function dcart_bulk_operations_submit($form, &$form_state) {
  $_SESSION['dcart_bulk_operations'] = $form_state['values'];
  $form_state['redirect'] = array('operations/bulk/' . $form_state['values']['type'], array('query' => array('destination' => $_GET['q'])));
}

/* ------------------------------------- Helper functions ------------------------------------- */

function _dcart_batch_finished($success, $results, $operations, $redirect = NULL, $success_message = NULL) {
  
  unset($_SESSION['dcart_bulk_operations'], $_SESSION['dcart_bulk_operation_selected']);

  if ($success) {
    if (isset($success_message)) {
      drupal_set_message($success_message);
    }
    else {
      drupal_set_message(t('Processed @count item(s)', array('@count' => count($results))));
    }
    
    if (isset($redirect)) {
      drupal_goto($redirect);
    }
  }
  else {
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation', array('%error_operation' => $error_operation[0]));
    drupal_set_message($message, 'error');
  }
}

function _dcart_batch_prepare_context(&$context, $max) {
  if (empty($context['sandbox']['progress'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['current'] = 0;
    $context['sandbox']['max'] = $max;
  } 
}

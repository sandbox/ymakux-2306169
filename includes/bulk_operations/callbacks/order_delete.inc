<?php

function dcart_bulk_operations_order_delete_form(&$form, $form_state, $items, $operation) {
  $count = count($items);
  if($count < 10) {
    $description = t('Orders with the following IDs will be completely deleted: @ids', array('@ids' => implode(',', $items)));
  }
  else {
    $description = format_plural($count, '1 selected order will be completely deleted', '@count selected orders will be completely deleted');
  }
  $description .= '<p>' . t('This action cannot be undone.') . '</p>';
  $form['description'] = array('#markup' => $description);
}

function dcart_bulk_operations_order_delete($arguments, &$context) {

  extract($arguments);
  _dcart_batch_prepare_context($context, $count);

  $delete = array();
  if ($orders = dcart_order_load_multiple((array) $item)) {
    foreach ($orders as $order) {
      if (dcart_order_access('delete', $order)) {
        $delete[] = $order->order_id;
      }
    }
  }
  
  if ($delete) {
    dcart_order_delete_multiple($delete);
    foreach ($delete as $delete_id) {
      $context['results'][$delete_id] = $delete_id;
      $context['sandbox']['progress']++;
      $context['sandbox']['current'] = $delete_id;
    }
  }
  
  if ($context['sandbox']['progress'] >= $context['sandbox']['max']) {
    $context['finished'] = TRUE;
  }
}

function dcart_bulk_operations_order_delete_finished($success, $results, $operations) {
  $message = t('Deleted @count order(s)', array('@count' => count($results)));
  _dcart_batch_finished($success, $results, $operations, 'admin/dcart/orders', $message);
}
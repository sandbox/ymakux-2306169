<?php

function dcart_bulk_operations_product_change_price_form(&$form, $form_state, $items, $operation) {

  $description = t('You are about to change price for product(s) with the following ID(s): @ids', array('@ids' => implode(',', $items)));
  $description .= '<p>' . t('This action cannot be undone.') . '</p>';
  $form['description'] = array('#markup' => $description);

  $form['bulk_operations']['value'] = array(
    '#title' => t('Value'),
    '#type' => 'textfield',
    '#description' => t('A value you want to add to existing price. To subtract, enter a negative value.'),
    '#required' => TRUE,
    '#size' => 3,
  );
  $form['bulk_operations']['type'] = array(
    '#title' => t('Type'),
    '#type' => 'select',
    '#description' => t('Type of value, either fixed amount or percents.'),
    '#options' => array('fixed' => t('Fixed'), 'percent' => t('Percent')),
    '#default_value' => 'fixed',
  );
}
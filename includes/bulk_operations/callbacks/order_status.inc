<?php

function dcart_bulk_operations_order_status_form(&$form, $form_state, $items, $operation) {

  $description = t('You are about to change order status for order(s) with the following ID(s): @ids', array('@ids' => implode(',', $items)));
  $description .= '<p>' . t('This action cannot be undone.') . '</p>';
  $form['description'] = array('#markup' => $description);
  
  $form['bulk_operations']['status'] = array(
    '#title' => t('Order status'),
    '#type' => 'select',
    '#options' => dcart_order_status_options(TRUE),
    '#description' => t('Select a new order status'),
    '#required' => TRUE,
  );
}

function dcart_bulk_operations_order_status($arguments, &$context) {

  extract($arguments);
  _dcart_batch_prepare_context($context, $count);

  if ($orders = dcart_order_load_multiple((array) $item)) {
    foreach ($orders as $order) {
      if (dcart_order_access('edit', $order)) {

        $order->status = $status;
        dcart_order_save($order);
        
        $context['results'][$order->order_id] = $order->order_id;
        $context['sandbox']['progress']++;
        $context['sandbox']['current'] = $order->order_id;
      }
    }
  }

  if ($context['sandbox']['progress'] >= $context['sandbox']['max']) {
    $context['finished'] = TRUE;
  }
}

function dcart_bulk_operations_order_status_finished($success, $results, $operations) {
  $message = t('Changed status for @count order(s)', array('@count' => count($results)));
  _dcart_batch_finished($success, $results, $operations, 'admin/dcart/orders', $message);
}
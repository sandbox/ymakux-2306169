<?php

function dcart_bulk_operations_product_delete_form(&$form, $form_state, $items, $operation) {
  $count = count($items);
  if($count < 10) {
    $description = t('Products with the following IDs will be completely deleted: @ids', array('@ids' => implode(',', $items)));
  }
  else {
    $description = format_plural($count, '1 selected product will be completely deleted', '@count selected products will be completely deleted');
  }
  $description .= '<p>' . t('This action cannot be undone.') . '</p>';
  $form['description'] = array('#markup' => $description);
}

function dcart_bulk_operations_product_delete($arguments, &$context) {
  
  extract($arguments);
  _dcart_batch_prepare_context($context, $count);

  $delete = array();
  if ($nodes = node_load_multiple((array) $item)) {
    foreach ($nodes as $node) {
      if (node_access('delete', $node)) {
        $delete[] = $node->nid;
      }
    }
  }
  if ($delete) {
    node_delete_multiple($delete);
    foreach ($delete as $nid) {
      $context['results'][$nid] = $nid;
      $context['sandbox']['progress']++;
      $context['sandbox']['current'] = $nid;
    }
  }
  if ($context['sandbox']['progress'] >= $context['sandbox']['max']) {
    $context['finished'] = TRUE;
  }
}

function dcart_bulk_operations_product_delete_finished($success, $results, $operations) {
  $message = t('Deleted @count product(s)', array('@count' => count($results)));
  _dcart_batch_finished($success, $results, $operations, 'admin/dcart/products', $message);
}
<?php

function dcart_payment_methods() {
  $payment_methods = &drupal_static(__FUNCTION__);
  if (!isset($payment_methods)) {
    $payment_methods = module_invoke_all('dcart_payment_method_info');
    $settings = dcart_payment_method_settings();
    
    $weight = 0;
    foreach($payment_methods as $id => &$payment_method) {

      if(empty($payment_method['name'])) {
        $payment_method['name'] = $id;
      }
      
      if(!isset($payment_method['weight'])) {
        $payment_method['weight'] = $weight;
      }
      
      if(!isset($payment_method['active'])) {
        $payment_method['active'] = TRUE;
      }
      
      if(!empty($settings[$id])) {
        $payment_methods[$id] = $settings[$id] + $payment_methods[$id];
      }
      
      $weight += 10;
    }
    drupal_alter('dcart_payment_method_info', $payment_methods);
    uasort($payment_methods, 'drupal_sort_weight');
  }
  return $payment_methods;
}

function dcart_payment_title($payment_method, $op) {
  switch($op) {
    case 'edit':
      return t('Edit payment method @name', array('@name' => $payment_method['title']));
      break;
  }
}

function dcart_payment_load($id) {
  return dcart_payment_methods_get($id);
}

function dcart_payment_methods_get($payment_method = NULL, $active = FALSE, $checkout = NULL) {
  $payment_methods = dcart_payment_methods();
  
  if(isset($payment_method)) {
    return isset($payment_methods[$payment_method]) ? $payment_methods[$payment_method] : NULL;
  }
  
  foreach($payment_methods as $id => $method) {
    if ($active && empty($method['active'])) {
      unset($payment_methods[$id]);
    } 
    if($checkout === TRUE && !empty($method['no_checkout'])) {
      unset($payment_methods[$id]);
    }
    if($checkout === FALSE && empty($method['no_checkout'])) {
      unset($payment_methods[$id]);
    }
  }
  return $payment_methods;
}

function dcart_payment_method_options($active = TRUE, $themed = TRUE, $checkout = NULL) {
  $options = array();
  foreach(dcart_payment_methods_get(NULL, $active, $checkout) as $method_id => $method_info) {
    $title = !empty($method_info['display_title']) ? $method_info['display_title'] : $method_info['title'];
    $options[$method_id] = $title;
    if ($themed) {
      $options[$method_id] = theme('dcart_radio_option', array(
       'title' => $title,
       'description' => !empty($method_info['description']) ? $method_info['description'] : '',
      ));
    }
  }

  drupal_alter('dcart_payment_method_options', $options);
  return $options;
}


function dcart_payment_method_settings($payment_method = NULL) {
  $return = array();
  $query = db_select('dcart_payment_method_settings', 's')->fields('s');

  if(isset($payment_method)) {
    $query->condition('name', $payment_method);
  }

  if($results = $query->execute()->fetchAll()) {
    foreach($results as $data) {
      $return[$data->name] = (array) $data;
      $return[$data->name]['settings'] = unserialize($return[$data->name]['settings']);
    }

    if(isset($payment_method)) {
      $return = reset($return);
    }
  }
  return $return;
}

function dcart_payment_method_settings_save($payment_method_name, $settings) {
  
  $settings += array(
    'active' => 1,
    'amount' => 0,
    'currency_code' => dcart_default_currency(),
    'settings' => array(),
    'weight' => 0,
  );
  
  $settings['settings'] = serialize($settings['settings']);
  
  return db_merge('dcart_payment_method_settings')
    ->key(array('name' => $payment_method_name))
    ->fields($settings)
    ->execute();
}

function dcart_payment_transaction_new() {
  return array(
    'id' => NULL,
    'transaction_id' => 0,
    'uid' => dcart_customer_get(),
    'order_id' => 0,
    'payment_method' => '',
    'message' => '',
    'message_variables' => array(),
    'amount' => 0,
    'currency_code' => dcart_default_currency(),
    'status' => 'success',
    'created' => REQUEST_TIME,
    'data' => array('ip' => $_SERVER['REMOTE_ADDR']),
  );
}

function dcart_payment_transaction_save($transaction = array()) {
  $transaction += dcart_payment_transaction_new();
  foreach (module_implements('dcart_transaction_save') as $module) {
    $function = $module . '_dcart_transaction_save';
    $function($transaction);
  }
  $transaction['data'] = serialize($transaction['data']);
  $transaction['message_variables'] = serialize($transaction['message_variables']);
  
  return db_merge('dcart_transaction')
    ->key(array('id' => $transaction['id']))
    ->fields($transaction)
    ->execute();
}

function dcart_dcart_transaction_save($transaction) {
  if($order = dcart_order_load($transaction['order_id'])) {
    $order->data['transaction_id'] = $transaction['transaction_id'];
    dcart_order_save($order);
  }
}

function dcart_payment_transaction_load($id) {
  $result = db_select('dcart_transaction', 't')
    ->fields('t')
    ->condition('id', $id)
    ->execute()
    ->fetchObject();
    
  if($result) {
    $result = (array) $result;
    $result['data'] = unserialize($result['data']);
    $result['message_variables'] = unserialize($result['message_variables']);
    return $result;
  }
  return array();
}

<?php

function dcart_payment_form($form, $form_state) {
  
  $form['#tree'] = TRUE;
  foreach(dcart_payment_methods() as $method_id => $method_info) {
    $settings = dcart_payment_method_settings($method_id);
    $form['payment_methods'][$method_id]['weight'] = array(
      '#type' => 'weight',
      '#delta' => 10,
      '#default_value' => isset($settings['weight']) ? $settings['weight'] : 0,
      '#attributes' => array('class' => array('payment-method-weight')),
    );
    $form['payment_methods'][$method_id]['active'] = array(
      '#type' => 'checkbox',
      '#default_value' => isset($settings['active']) ? $settings['active'] : !empty($method_info['active']),
    );
    if (dcart_hook_callback($method_info, 'settings_form')) {
      $form['payment_methods'][$method_id]['settings'] = array(        
         '#type' => 'link',
         '#title' => t('Settings'),
         '#href' => 'admin/dcart/settings/payment/' . $method_id . '/settings',
         '#options' => array('query' => array('destination' => $_GET['q'])),
      );
    }
  }
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#access' => !empty($form['payment_methods']),
  );
  
  return $form;
}

function dcart_payment_form_submit($form, &$form_state) {
  form_state_values_clean($form_state);
  $settings = dcart_payment_method_settings();
  $settings = array_replace_recursive($settings, $form_state['values']['payment_methods']);
  variable_set('dcart_payment_methods_settings', $settings);
  drupal_set_message(t('The configuration options have been saved.'));
}

function dcart_payment_method_settings_form($form, $form_state, $payment_method) {

  $form_state['payment_method'] = $payment_method;
  $form_state['payment_method_name'] = $payment_method['name'];

  $form['active'] = array(
    '#title' => t('Active'),
    '#type' => 'checkbox',
    '#default_value' => (isset($payment_method['active']) && $payment_method['active']) ?  TRUE : FALSE,
  );
  
  $form['amount'] = array(
    '#title' => t('Amount'),
    '#type' => 'textfield',
    '#size' => 4,
    '#default_value' => isset($payment_method['amount']) ? $payment_method['amount'] : '',
    '#element_validate' => array('element_validate_number'),
    '#pre_render' => array('dcart_element_amount_to_decimal'),
  );
  
  $form['currency_code'] = array(
    '#title' => t('Currency'),
    '#type' => 'select',
    '#options' => dcart_currency_options(TRUE),
    '#default_value' => isset($payment_method['currency_code']) ? $payment_method['currency_code'] : dcart_default_currency(),
  ); 

  $form['settings'] = array(
    '#type' => 'container',
    '#weight' => 99,
  );

  if ($form_function = dcart_hook_callback($payment_method, 'settings_form')) {
    $payment_settings = !empty($payment_method['settings']) ? $payment_method['settings'] : array();
    if ($elements = $form_function($payment_settings)) {
      $form['settings'] += $elements;
    }
  }
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
     '#type' => 'submit',
     '#value' => t('Save'),
  );
  return $form;
}

function dcart_payment_method_settings_form_validate($form, &$form_state) {
  form_state_values_clean($form_state);
  if ($validate_function = dcart_hook_callback($form_state['payment_method'], 'settings_form_validate')) {
    $validate_function($form, $form_state);
  }
  $amount = $form_state['values']['amount'];
  $form_state['values']['amount'] = dcart_price_decimal_to_amount($amount);

  foreach($form_state['values'] as $key => $value) {
    if(!in_array($key, array('active', 'amount', 'currency_code'))) {
      $form_state['values']['settings'][$key] = $value;
      unset($form_state['values'][$key]);
    }
  }
}

function dcart_payment_method_settings_form_submit($form, $form_state) {
  if ($submit_function = dcart_hook_callback($form_state['payment_method'], 'settings_form_submit')) {
    $submit_function($form, $form_state);
  }
  dcart_payment_method_settings_save($form_state['payment_method_name'], $form_state['values']);
  drupal_set_message(t('The configuration options have been saved.'));
}

function theme_dcart_payment_form($variables) {
  $rows = array();
  $form = $variables['form'];
  foreach (element_children($form['payment_methods']) as $payment_method_id) {
    $payment_method = dcart_payment_methods_get($payment_method_id);
    $rows[$payment_method_id] = array(
      'data' => array(
        check_plain($payment_method['title']),
        render($form['payment_methods'][$payment_method_id]['active']),
        render($form['payment_methods'][$payment_method_id]['weight']),
        isset($form['payment_methods'][$payment_method_id]['settings']) ? render($form['payment_methods'][$payment_method_id]['settings']) : t('No settings'),
      ),
      'class' => array('draggable'),
    );
  }
  
  $header = array(t('Payment method'), t('Active'), t('Weight'), t('Actions'));
  
  $table_id = 'payment-methods-table';
  $output = theme('table', array(
    'rows' => $rows,
    'header' => $header,
    'empty' => t('No payment methods'),
    'attributes' => array('id' => $table_id),
  ));
  drupal_add_tabledrag($table_id, 'order', 'sibling', 'payment-method-weight');
  $output .= drupal_render_children($form);
  return $output;
}

function dcart_payment_transactions() {
  
  drupal_set_title(t('Transactions'));
  
  $header = array(
    array('data' => t('Order ID'), 'field' => 'order_id'),
    array('data' => t('Customer')),
    array('data' => t('Transaction ID'), 'field' => 'transaction_id'),
    array('data' => t('Payment method'), 'field' => 'payment_method'),
    array('data' => t('Amount'), 'field' => 'amount'),
    array('data' => t('Currency'), 'field' => 'currency_code'),
    array('data' => t('Created'), 'field' => 'created', 'sort' => 'desc'),
    array('data' => t('Status'), 'field' => 'status'),
    array('data' => t('Operations')),
  );
  
  $limit = 10;

  $query = db_select('dcart_transaction', 't')
    ->fields('t')
    ->extend('TableSort')
    ->extend('PagerDefault')
    ->limit($limit);

  $payment_methods = dcart_payment_methods();
  
  $rows = array();
  if($results = $query->orderByHeader($header)->execute()) {
    foreach($results as $result) {    
      $payment_method = isset($payment_methods[$result->payment_method])
      ? check_plain($payment_methods[$result->payment_method]['title']) : t('Unknown');

      $status = t('Unknown');
      switch($result->status) {
        case 'success':
          $status = t('OK');
          break;
        case 'failure':
          $status = t('FAIL');
      }
      
      $customer = variable_get('anonymous', t('Anonymous'));
      if(is_numeric($result->uid) && ($user = user_load($result->uid))) {
        $customer = l($user->name, 'user/' . $user->uid);
      }

      $rows[$result->id] = array(
        'data' => array(
          $result->order_id,
          $customer,
          $result->transaction_id,
          $payment_method,
          $result->amount,
          $result->currency_code,
          format_date($result->created),
          $status,
          l(t('Details'), 'admin/dcart/orders/transactions/' . $result->id . '/details'),
        ),
      );
    }
  }

  $output = theme('table', array('rows' => $rows, 'header' => $header, 'empty' => t('No saved transactions')));
  $output .= theme('pager');
  return $output;
}

function dcart_payment_settings_form($form, $form_state) {
  $form['dcart_payment_transaction_lifespan'] = array(
    '#title' => t('Transactions lifespan'),
    '#type' => 'textfield',
    '#size' => 8,
    '#default_value' => variable_get('dcart_payment_transaction_lifespan', 31557600),
    '#element_validate' => array('element_validate_number'),
    '#description' => t('How long payment transactions will stay in the database, in seconds. 31557600 seconds = 1year. Enter 0 to never delete transactions from database.'),
  );
  
  return system_settings_form($form);
}

function dcart_payment_transaction_details($transaction) {
  drupal_set_title(t('Details of transaction #@num', array('@num' => $transaction['id'])));
  return theme('dcart_payment_transaction_details', array('transaction' => $transaction));
}

function theme_dcart_payment_transaction_details($variables) {

  $transaction = $variables['transaction'];
  $response = !empty($transaction['data']['response']) ? check_plain($transaction['data']['response']) : '';
  $ip = !empty($transaction['data']['ip']) ? check_plain($transaction['data']['ip']) : '';

  $message_text = !empty($transaction['message']) ? (string) $transaction['message'] : '';
  $message_variables = !empty($transaction['message_variables']) ? (array) $transaction['message_variables'] : array();

  $output = t('Nothing to display.');
  
  if($message_text) {
    $message = t($message_text, $message_variables);
    $output = '<div class="transaction-message">' . $message_text . '</div>';
  }
  
  if($ip) {
    $output .= '<div class="transaction-ip">' . $ip . '</div>';
  }
  
  if($response) {
    $output .= '<h3>' . t('Response') . '</h3>';
    $output .= '<div class="transaction-response"><code>' . $response . '</code></div>';
  }
  return $output;
}

function theme_dcart_payment_description($variables) {
  $payment = $variables['payment_method'];
  
  $output = '';
  if(!empty($payment['image'])) {
    $output .= '<div class="icon">' . theme('image', array('path' => $payment['image'])) . '</div>';
  }
  
  $title = !empty($payment['display_title']) ? $payment['display_title'] : $payment['title'];
  $output .= '<div class="text">';
  $output .= '<div class="title">' . check_plain($title) . '</div>';
  if(!empty($payment['description'])) {
    $output .= '<div class="description">' . filter_xss($payment['description']) . '</div>';
  }
  $output .= '</div>';
  return $output;
}


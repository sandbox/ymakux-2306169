<?php

function dcart_action_default_settings($action = NULL) {
  
$checkout_completed_email_admin = '
Order [dcart-order:id], [dcart-order:created], total [dcart-order:amount-formatted]
Status [dcart-order:status]
Order details
[dcart-order:content-plain]

View [dcart-order:admin-view-url]
Edit [dcart-order:admin-edit-url]
';
  
$checkout_completed_email_customer = '
Thank you for shopping at [site:name]!
Order [dcart-order:id] created [dcart-order:created], total [dcart-order:amount-formatted]
Current order status [dcart-order:status]
Order details
[dcart-order:content-plain]
View order online [dcart-order:customer-view-url]
--------------------------------------------
If you have any questions, please contact us [site:mail].
';

$checkout_completed_email_customer_anonymous = '
Thank you for shopping at [site:name]!
Order [dcart-order:id] created [dcart-order:created], total [dcart-order:amount-formatted]
Current order status [dcart-order:status]
Order details
[dcart-order:content-plain]
[dcart-order:status-url]
--------------------------------------------
If you have any questions, please contact us [site:mail].
';

$checkout_completed_message_auth = '
Your order is complete!<br />
Order number <a href="[dcart-order:customer-view-url]">[dcart-order:id]</a>.<br />
Thank you for shopping at [site:name].<br />
<p></p>
<p><a href="[site:url]">Return to the front page</a></p>
';

$checkout_completed_message_anon = '
Thank you for order!<br />
Your order ID is [dcart-order:id]<br />
You still have no account at [site:name], so you can use this unique link to check order status in the future(also sent to e-mail you provided).<br />
<br />
<br />
<a href="[dcart-order:status-url]">[dcart-order:status-url]</a>
<br />
<br />
<a href="[site:url]">Return to the front page</a>
';


$order_created_by_admin = '
The order #[dcart-order:id] was created for you on [site:name]!
View order online [dcart-order:customer-view-url]
--------------------------------------------
If you have any questions, please contact us [site:mail].
';
  
  $settings = array(
    'dcart_action_cart_redirect' => array('path' => variable_get('dcart_cart_url', 'cart')),
    'dcart_action_cart_message' => array(
      'message_en' => '<em>[node:title]</em> has been added to your <a href="[dcart:cart-url]">cart</a>. <a href="[dcart:checkout-url] ">Checkout</a>',
    ),
    'dcart_action_wishlist_message' => array(
      'message_en' => '<em>[node:title]</em> has been added to your <a href="[dcart:wishlist-url]">wishlist</a>',
    ),
    'dcart_action_checkout_completed_email_customer' => array(
      'message_en' => $checkout_completed_email_customer,
      'subject_en' => t('Your order at [site:name]'),
      'recipient' => '[dcart-order:customer-email]',
    ),
    'dcart_action_checkout_completed_email_admin' => array(
      'message_en' => $checkout_completed_email_admin,
      'subject_en' => t('New order has been created at [site:name]'),
      'recipient' => '[site:mail]',
    ),
    'dcart_action_checkout_completed_message' => array(
      'message_en' => $checkout_completed_message_auth,
      'filter' => 'plain_text',
    ),
    'dcart_action_checkout_completed_message_anonymous' => array(
      'message_en' => $checkout_completed_message_anon,
      'filter' => 'plain_text',
    ),
    'dcart_action_checkout_completed_email_customer_anonymous' => array(
      'message_en' => $checkout_completed_email_customer_anonymous,
      'subject_en' => t('Your order at [site:name]'),
      'email_field' => 'dcart_order_email',
    ),
    'dcart_action_order_created_by_admin' => array(
      'message_en' => $order_created_by_admin,
      'subject_en' => t('Your order at [site:name]'),
    ),
  );
  
  if(isset($action)) {
    return isset($settings[$action]) ? $settings[$action] : array();
  }
  return $settings;
}
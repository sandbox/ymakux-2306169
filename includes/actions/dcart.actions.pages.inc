<?php

function dcart_actions_overview() {

  drupal_set_title(t('Actions'));

  actions_synchronize();

  $rows = array();
  $header = array(
    array('data' => t('Label'), 'field' => 'label'),
    array('data' => t('Operations')),
  );

  $query = db_select('actions')
    ->extend('PagerDefault')
    ->extend('TableSort');
  
  $result = $query->fields('actions')
    ->condition('type', 'dcart')
    ->limit(50)
    ->orderByHeader($header)
    ->execute();

  $enabled_actions = array();
  foreach ($result as $action) {

    if(empty($action->parameters)) {
      continue;
    }
    
    $enabled_actions[] = $action->callback;

    $operations = l(t('configure'), "admin/dcart/settings/actions/$action->aid/edit", array(
      'query' => array('destination' => current_path())));
    $operations .= ' | ';
    $operations .= l(t('disable'), "admin/config/system/actions/delete/$action->aid", array(
      'query' => array('destination' => current_path())));
    
    $rows[] = array(
      array('data' => check_plain($action->label)),
      array('data' => $operations),
    );
  }

  $build['table']['#markup'] = empty($rows) ? t('No actions') : theme('table', array(
    'header' => $header, 'rows' => $rows)) . theme('pager');

  $actions_map = actions_actions_map(actions_list());
  
  $options = array();
  foreach ($actions_map as $key => $array) {
    if ($array['type'] == 'dcart' && $array['configurable'] && !in_array($array['callback'], $enabled_actions)) {
      $options[$key] = check_plain($array['label']);
    }
  }

  $build['form'] = drupal_get_form('dcart_actions_add_form', $options);
  return $build;
}

function dcart_actions_add_form($form, $form_state, $options = array()) {
  module_load_include('inc', 'system', 'system.admin');
  $form = system_actions_manage_form($form, $form_state, $options);

  $form['parent']['action']['#empty_option'] = t('- Select -');
  $form['parent']['#title'] = t('Enable action');
  $form['parent']['actions']['submit']['#value'] = t('Enable');
  $form['parent']['actions']['submit']['#submit'] = array('dcart_actions_add_form_submit');
  $form['#access'] = (bool) $options;
  return $form;
}

function dcart_actions_add_form_submit($form, &$form_state) {
  if ($form_state['values']['action']) {
    $form_state['redirect'] = array('admin/dcart/settings/actions/' . $form_state['values']['action'] . '/edit', array(
      'query' => array('destination' => current_path())));
  }
}

<?php

function dcart_action_checkout_completed_email_customer_anonymous($object, $context) {

  global $language;
  $default_settings = dcart_action_default_settings(__FUNCTION__);
  $subject = isset($context['subject_' . $language->language]) ? $context['subject_' . $language->language] : $default_settings['subject_en'];
  $message = isset($context['message_' . $language->language]) ? $context['message_' . $language->language] : $default_settings['message_en'];

  $subject = token_replace($subject, array('dcart_order' => $object));
  $message = token_replace($message, array('dcart_order' => $object));

  $email_field = dcart_order_address_field_names('email');
  if($email_field) {
    $email_field = reset($email_field);
    if(!empty($object->{$email_field}[LANGUAGE_NONE][0]['value'])) {
      $recipient = $object->{$email_field}[LANGUAGE_NONE][0]['value'];
      if((valid_email_address($recipient)) && $subject && $message) {
        $from = !empty($context['from']) ? $context['from'] : NULL;
        if (dcart_send_mail($recipient, $subject, $message, $from)) {
          watchdog('dcart', 'Sent email to %recipient', array('%recipient' => $recipient), WATCHDOG_INFO);
        }
        else {
          watchdog('dcart', 'Unable to send email to %recipient', array('%recipient' => $recipient), WATCHDOG_ERROR);
        }  
      }
    }
  }
}


function dcart_action_checkout_completed_email_customer_anonymous_form($context) {
  if(!dcart_order_address_field_names('email')) {
    drupal_set_message(t('You should define an order field that contains e-mail address'), 'warning');
  }
  $form['from'] = array(
    '#title' => t('From'),
    '#type' => 'textfield',
    '#default_value' => isset($context['from']) ? $context['from'] : variable_get('dcart_address_email', variable_get('site_mail', ini_get('sendmail_from'))),
  );
  $settings = dcart_action_default_settings('dcart_action_checkout_completed_email_customer_anonymous');
  foreach(dcart_language_list() as $code => $name) {
    $form['subject_' . $code] = array(
      '#title' => t('E-mail subject for language @language', array('@language' => $name)),
      '#type' => 'textfield',
      '#default_value' => isset($context['subject_' . $code]) ? $context['subject_' . $code] : $settings['subject_en'],
    );

    $form['message_' . $code] = array(
      '#title' => t('Text for language @language', array('@language' => $name)),
      '#type' => 'textarea',
      '#rows' => 3,
      '#default_value' => isset($context['message_' . $code]) ? $context['message_' . $code] : $settings['message_en'],
    );
  }
  return $form;
}

function dcart_action_checkout_completed_email_customer_anonymous_validate($form, &$form_state) {
  if(!valid_email_address($form_state['values']['from'])) {
    form_error($form['from'], t('%email is not a valid e-mail address.', array('%email' => $form_state['values']['from'])));
  }
}

function dcart_action_checkout_completed_email_customer_anonymous_submit($form, $form_state) {
  $return = array('from' => $form_state['values']['from']);
  foreach(dcart_language_list() as $code => $name) {
    $return['subject_' . $code] = trim($form_state['values']['subject_' . $code]);
    $return['message_' . $code] = trim($form_state['values']['message_' . $code]);
  }
  return $return;
}

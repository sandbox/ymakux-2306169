<?php

function dcart_action_checkout_completed_message_anonymous($object, $context) {
  global $language;
  $settings = dcart_action_get_settings(__FUNCTION__);
  $default_settings = dcart_action_default_settings(__FUNCTION__);
  $message = isset($settings['message_' . $language->language]) ? $settings['message_' . $language->language] : $default_settings['message_en'];
  return token_replace($message, array('dcart_order' => $object));
}

function dcart_action_checkout_completed_message_anonymous_form($context) {
  $settings = dcart_action_default_settings('dcart_action_checkout_completed_message_anonymous');
  foreach(dcart_language_list() as $code => $name) {
    $form['message_' . $code] = array(
      '#title' => t('Text for language @language', array('@language' => $name)),
      '#type' => 'textarea',
      '#default_value' => isset($context['message_' . $code]) ? $context['message_' . $code] : $settings['message_en'],
    );
  }
  
  $form['token']['#markup'] = theme('dcart_token_browser', array('types' => array('site', 'data', 'user', 'dcart', 'dcart-order')));
  
  $form['filter'] = array(
    '#title' => t('Filter'),
    '#type' => 'select',
    '#options' => drupal_map_assoc(array_keys(filter_formats())),
    '#default_value' => isset($context['filter']) ? $context['filter'] : $settings['filter'],
  );
  
  return $form;
}

function dcart_action_checkout_completed_message_anonymous_submit($form, $form_state) {
  $return = array('filter' => $form_state['values']['filter']);
  foreach(dcart_language_list() as $code => $name) {
    $return['message_' . $code] = $form_state['values']['message_' . $code];
  }
  return $return;
}

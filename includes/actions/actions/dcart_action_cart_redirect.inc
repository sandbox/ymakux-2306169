<?php

function dcart_action_cart_redirect_form($context) {
  $form['path'] = array(
    '#title' => t('Path'),
    '#type' => 'textfield',
    '#default_value' => isset($context['path']) ? $context['path'] : variable_get('dcart_cart_url', 'cart'),
    '#description' => t('Specify a relative path where customer will be redirected after product added to cart. Leave empty to disable redirection.'),
  );
  return $form;
}

function dcart_action_cart_redirect_submit($form, $form_state) {
  return array('path' => trim($form_state['values']['path']));
}

function dcart_action_cart_redirect($object, $context) {
  $settings = dcart_action_get_settings(__FUNCTION__);
  $path = trim($settings['path']);
  if($path) {
    drupal_goto($path);
  }
}
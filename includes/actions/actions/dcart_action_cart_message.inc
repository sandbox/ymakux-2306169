<?php

function dcart_action_cart_message_form($context) {
  $default_settings = dcart_action_default_settings('dcart_action_cart_message');
  foreach(dcart_language_list() as $code => $name) {
    $form['message_' . $code] = array(
      '#title' => t('Text for language @language', array('@language' => $name)),
      '#type' => 'textarea',
      '#rows' => 2,
      '#default_value' => isset($context['message_' . $code]) ? $context['message_' . $code] : $default_settings['message_en'],
    );
  }
  $form['token']['#markup'] =  theme('dcart_token_browser', array('types' => array('site', 'data', 'user', 'dcart', 'node')));
  return $form;
}

function dcart_action_cart_message_submit($form, $form_state) {
  $return = array();
  foreach(dcart_language_list() as $code => $name) {
    $return['message_' . $code] = $form_state['values']['message_' . $code];
  }
  return $return;
}

function dcart_action_cart_message($object, $context) {
  global $language;
  $settings = dcart_action_get_settings(__FUNCTION__);
  $default_settings = dcart_action_default_settings(__FUNCTION__);
  $message = isset($settings['message_' . $language->language]) ? $settings['message_' . $language->language] : $default_settings['message_en'];
  $text = token_replace($settings['message_' . $language->language], array('node' => $object));
  drupal_set_message(filter_xss($text));
}
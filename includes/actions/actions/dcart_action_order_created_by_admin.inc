<?php

function dcart_action_order_created_by_admin($order, $context) {

  global $language;
  $settings = dcart_action_get_settings(__FUNCTION__);
  $default_settings = dcart_action_default_settings(__FUNCTION__);
  $subject = isset($settings['subject_' . $language->language]) ? $settings['subject_' . $language->language] : $default_settings['subject_en'];
  $message = isset($settings['message_' . $language->language]) ? $settings['message_' . $language->language] : $default_settings['message_en'];

  $message = token_replace($message, array('dcart_order' => $object));
  $subject = token_replace($subject, array('dcart_order' => $object));

  $recipient = $order->mail;
  if (dcart_send_mail($recipient, $subject, $message)) {
    watchdog('dcart', 'Sent email to %recipient', array('%recipient' => $recipient));
  }
  else {
    watchdog('error', 'Unable to send email to %recipient', array('%recipient' => $recipient));
  }
}

function dcart_action_order_created_by_admin_form($context) {
  $settings = dcart_action_default_settings('dcart_action_order_created_by_admin');
  foreach(dcart_language_list() as $code => $name) {
    $form['subject_' . $code] = array(
      '#title' => t('E-mail subject for language @language', array('@language' => $name)),
      '#type' => 'textfield',
      '#default_value' => isset($context['subject_' . $code]) ? $context['subject_' . $code] : $settings['subject_en'],
    );
    $form['message_' . $code] = array(
      '#title' => t('Text for language @language', array('@language' => $name)),
      '#type' => 'textarea',
      '#rows' => 3,
      '#default_value' => isset($context['message_' . $code]) ? $context['message_' . $code] : $settings['message_en'],
    );
  }
  $form['token']['#markup'] = theme('dcart_token_browser', array('types' => array('site', 'data', 'user', 'dcart', 'dcart-order')));
  return $form;
}

function dcart_action_order_created_by_admin_submit($form, $form_state) {
  $return = array();
  foreach(dcart_language_list() as $code => $name) {
    $return['subject_' . $code] = trim($form_state['values']['subject_' . $code]);
    $return['message_' . $code] = trim($form_state['values']['message_' . $code]);
  }
  return $return;
}

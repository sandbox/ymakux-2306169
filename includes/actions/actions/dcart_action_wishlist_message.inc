<?php

function dcart_action_wishlist_message($object, $context) {
  global $language;
  $settings = dcart_action_get_settings(__FUNCTION__);
  $default_settings = dcart_action_default_settings(__FUNCTION__);
  $message = isset($settings['message_' . $language->language]) ? $settings['message_' . $language->language] : $default_settings['message_en'];
  $message = token_replace($message, array('node' => $object));
  drupal_set_message(filter_xss($message));
}

function dcart_action_wishlist_message_form($context) {
  $settings = dcart_action_default_settings('dcart_action_wishlist_message');
  foreach(dcart_language_list() as $code => $name) {
    $form['message_' . $code] = array(
      '#title' => t('Text for language @language', array('@language' => $name)),
      '#type' => 'textarea',
      '#rows' => 3,
      '#default_value' => isset($context['message_' . $code]) ? $context['message_' . $code] : $settings['message_en'],
    );
  }
  $form['token']['#markup'] = theme('dcart_token_browser', array('types' => array('site', 'data', 'user', 'dcart', 'node')));
  return $form;
}

function dcart_action_wishlist_message_submit($form, $form_state) {
  $return = array();
  foreach(dcart_language_list() as $code => $name) {
    $return['message_' . $code] = trim($form_state['values']['message_' . $code]);
  }
  return $return;
}
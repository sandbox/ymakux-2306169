<?php

require_once 'dcart.actions.default.inc';

foreach(dcart_action_info() as $file => $data) {
  $expected_file = drupal_get_path('module', 'dcart') . "/includes/actions/actions/$file.inc";
  if(file_exists($expected_file)) {
    require_once $expected_file;
  }
}

function dcart_action_info() {
  return array(
    'dcart_action_cart_message' => array(
      'type' => 'dcart',
      'label' => t('Cart: Message after item has been added to cart'),
      'configurable' => TRUE,
    ),
    'dcart_action_wishlist_message' => array(
      'type' => 'dcart',
      'label' => t('Wishlist: Message after item has been added to wishlist'),
      'configurable' => TRUE,
    ),
    'dcart_action_cart_redirect' => array(
      'type' => 'dcart',
      'label' => t('Cart: Redirect after item has been added to cart'),
      'configurable' => TRUE,
    ),
    'dcart_action_checkout_completed_message' => array(
      'type' => 'dcart',
      'label' => t('Checkout: Message after checkout was completed (logged in user)'),
      'configurable' => TRUE,
    ),
    'dcart_action_checkout_completed_message_anonymous' => array(
      'type' => 'dcart',
      'label' => t('Checkout: Message after checkout was completed (anonymous)'),
      'configurable' => TRUE,
    ),
    'dcart_action_checkout_completed_email_customer' => array(
      'type' => 'dcart',
      'label' => t('Checkout: Send e-mail to customer after checkout was completed'),
      'configurable' => TRUE,
    ),
    'dcart_action_checkout_completed_email_customer_anonymous' => array(
      'type' => 'dcart',
      'label' => t('Checkout: Send e-mail to customer after checkout was completed (anonymous)'),
      'configurable' => TRUE,
    ),
    'dcart_action_checkout_completed_email_admin' => array(
      'type' => 'dcart',
      'label' => t('Checkout: Send e-mail to admin after checkout was completed'),
      'configurable' => TRUE,
    ),
    'dcart_action_order_created_by_admin' => array(
      'type' => 'dcart',
      'label' => t('Order: Send e-mail to customer after order was created by admin'),
      'configurable' => TRUE,
    ),
  );
}

function dcart_action_get_id($callback) {
  return db_select('actions', 'a')
    ->fields('a', array('aid'))
    ->condition('callback', (array) $callback, 'IN')
    ->execute()
    ->fetchCol();
}

function dcart_action_perform($callback, $object, $context = array()) {
  if(!empty($callback)) {
    // TODO: check numeric values in query
    $ids = array();
    foreach(dcart_action_get_id($callback) as $id) {
      if(is_numeric($id)) {
        $ids[] = $id;
      }
    }
    if($ids) {
      return actions_do($ids, $object, $context);
    }
  }
}

function dcart_action_get_settings($callback) {
  $action = db_query("SELECT parameters FROM {actions} WHERE callback = :callback", array(':callback' => $callback))->fetchObject();
  return !empty($action->parameters) ? unserialize($action->parameters) : NULL;
}

function dcart_form_system_actions_configure_alter(&$form, &$form_state, $form_id) {
  if(arg(1) == 'dcart') {
    $form['actions_label']['#access'] = FALSE;
  }
}

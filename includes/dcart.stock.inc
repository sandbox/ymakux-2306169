<?php


function dcart_stock_set($sku, $level, $data = array()) {
  $current_level = dcart_stock_level_get($sku);
  
  foreach (module_implements('dcart_stock_presave') as $module) {
    $function = $module . '_dcart_stock_presave';
    $function($sku, $current_level, $level, $data);
  }
  
  if ($level == "") {
    return dcart_stock_delete($sku);
  }

  return dcart_stock_level_set($sku, $level, $data);
}


function dcart_stock_unset($sku) {
  $current_level = dcart_stock_level_get($sku);
  foreach (module_implements('dcart_stock_delete') as $module) {
    $function = $module . '_dcart_stock_delete';
    $function($sku, $current_level);
  }
  dcart_stock_delete($sku);
}

function dcart_stock_get($sku) {
  return db_select('dcart_stock', 's')
    ->fields('s')
    ->condition('sku', $sku)
    ->execute()
    ->fetchObject();
}

function dcart_stock_level_get($sku) {
  $stock = dcart_stock_get($sku);
  return isset($stock->level) ? (int) $stock->level : NULL;
}

function dcart_stock_level_set($sku, $level, $data = array()) {
  
  if(!is_numeric($level)) {
    return NULL;
  }
  
  $fields = array(
    'sku' => $sku,
    'level' => (int) $level,
    'changed' => REQUEST_TIME,
    'data' => serialize($data),
  );
  
  return db_merge('dcart_stock')
    ->key(array('sku' => $sku))
    ->fields($fields)
    ->execute();
}

function dcart_stock_delete($sku) {
  return db_delete('dcart_stock')->condition('sku', $sku)->execute();
}

function theme_dcart_stock_level($variables) {
  $level = $variables['level'];
  $text = t('Pre-order');
  $attributes = array('class' => array('stock-level', 'preorder'));
  
  if(is_numeric($level)) {
    if($level > 0) {
      $text = t('In stock @count', array('@count' => $level));
      $attributes['class'][1] = 'in-stock';
    }
    else {
      $text = t('Out of stock');
      $attributes['class'][1] = 'out-of-stock';
    }
  }
  return $text ? '<div' . drupal_attributes($attributes) . '>' . $text . '</div>' : '';
}

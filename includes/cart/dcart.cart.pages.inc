<?php

function dcart_cart_form($form, &$form_state) {

  $form['#tree'] = TRUE;

  $query = db_select('dcart_cart', 'c')
    ->fields('c')
    ->condition('c.status', DCART_CHECKOUT_READY)
    ->condition('c.uid', dcart_customer_get())
    ->orderBy('created', 'DESC')
    ->extend('PagerDefault')
    ->limit(variable_get('dcart_cart_items_per_page', 5));

  if($cart_content = $query->execute()) {
    $subtotal = array();
    foreach($cart_content as $data) {
      $id = $data->id;
      $sku = $data->sku;
      if ($node = dcart_get_node_by_sku($sku, TRUE)) {
        $attribute = dcart_product_attributes_get($node, $sku);
        $existing_node = node_load($node->nid);
        if(empty($existing_node->status)) {
          continue;
        }
      
        if (isset($attribute['data']['properties']['image_field']['field_name'])) {
          $image_field = $attribute['data']['properties']['image_field']['field_name'];
          if(!empty($node->{$image_field}[LANGUAGE_NONE][0]['uri'])) {
            $image_uri = $node->{$image_field}[LANGUAGE_NONE][0]['uri'];
          }
        }
        else {
          $instances = field_info_instances('node', $node->type);
          foreach ($instances as $field_name => $field_data) {
            if (!empty($field_data['dcart_settings']['cart_icon']) 
            && ($items = field_get_items('node', $node, $field_name, $node->language))) {
              $image_uri = $items[0]['uri'];
              break;
            }
          }        
        }

        $icon = '';     
        if (isset($image_uri)) {
          $icon = theme('image_style', array('style_name' => 'dcart_cart', 'path' => $image_uri));
          $icon = l($icon, 'node/' . $node->nid, array('html' => TRUE, 'query' => array('sku' => $sku)));
        }
    
        $title = l($node->title, 'node/' . $node->nid, array('query' => array('sku' => $sku)));
    
        $form['cart'][$id]['title']['#markup'] = $title;
        $form['cart'][$id]['image']['#markup'] = $icon;
        $form['cart'][$id]['node'] = array('#type' => 'value', '#value' => $node);
        $form['cart'][$id]['attribute'] = array('#type' => 'value', '#value' => $attribute);
    
        $form['cart'][$id]['count'] = array(
          '#title' => t('Count'),
          '#title_display' => 'invisible',
          '#type' => 'textfield',
          '#size' => 1,
          '#maxlength' => 2,
          '#default_value' => $data->count,
          '#attributes' => array('class' => array('product-quantity'), 'autocomplete' => 'off'),
        );
      
        if(!dcart_cart_access((array) $data, 'edit')) {
          $form['cart'][$id]['count']['#disabled'] = TRUE;
          $form['cart'][$id]['count']['#attributes'] = array('class' => array('not-editable'));
          $form['cart'][$id]['count']['#suffix'] = '';
          $form['cart'][$id]['count']['#default_value'] = 0;
        }
    
        $subtotal[$id] = $attribute['amount'] * $data->count;
        $form['cart'][$id]['price']['#markup'] = theme('dcart_price', array('amount' => $attribute['amount']));
        $form['cart'][$id]['amount']['#markup'] = theme('dcart_price', array('amount' => $subtotal[$id]));

        $form['cart'][$id]['attributes'] = array(
          '#markup' => theme('dcart_product_attributes', array('node' => $node,'attribute' => $attribute)),
        );

        $form['cart'][$id]['operations']['remove'] = array(
          '#type' => 'link',
          '#access' => dcart_cart_access((array) $data, 'edit'),
          '#href' => 'operations/cart/remove/' . $id,
          '#title' => '<i class="fa fa-trash-o"></i><span>' . t('Remove') . '</span>',
          '#attributes' => array('class' => array('remove'), 'title' => t('Remove from the cart')),
          '#options' => array('html' => TRUE, 'query' => array(
            'destination' => $_GET['q']),
          ),
        );

        $form['cart'][$id]['operations']['wishlist'] = array(
          '#type' => 'link',
          '#access' => (dcart_cart_access((array) $data, 'add_to_wishlist')),
          '#href' => 'operations/cart/to-wishlist/' . $id,
          '#title' => '<i class="fa fa-heart-o"></i><span>' . t('Move to wishlist') . '</span>',
          '#attributes' => array('class' => array('wishlist'), 'title' => t('Move to wishlist')),
          '#options' => array('html' => TRUE, 'query' => array(
            'destination' => $_GET['q']),
          ),
        );
       }

       $form['subtotal']['#markup'] = theme('dcart_price', array('amount' => array_sum($subtotal)));
       $form['recalculate'] = array(
         '#type' => 'submit',
         '#value' => t('Recalculate'),
         '#submit' => array('dcart_cart_form_recalculate'),
         '#attributes' => array('id' => 'recalculate'),
       );

       $form['checkout'] = array(
         '#type' => 'submit',
         '#value' => t('Proceed with order'),
         '#submit' => array('dcart_cart_form_submit'),
         '#validate' => array('dcart_cart_form_validate'),
         '#access' => user_access('dcart create order'),
         '#attributes' => array('id' => 'checkout', 'class' => array('checkout-button-next')),
       );
     }
   }
  return $form;
}

function dcart_cart_form_recalculate($form, &$form_state) {
  foreach($form_state['values']['cart'] as $id => $data) {
    dcart_cart_update($id, array('count' => $data['count']));
  }
}

function dcart_cart_form_validate($form, &$form_state) {

  $form_state['dcart_cart_products'] = array();
  if ($customer = dcart_customer_get()) {
    $check_stock_0 = variable_get('dcart_checkout_forbid_stock_0', 0);
    $check_stock_set = variable_get('dcart_checkout_forbid_stock_null', 0);
    
    foreach($form_state['values']['cart'] as $id => $data) {
      
      if (!filter_var($data['count'], FILTER_VALIDATE_INT)) {
        return form_error($form['cart'][$id]['count'], t('Quantity of products must be a positive integer.'));
      }
    
      if($check_stock_0 || $check_stock_set) {
        $level = dcart_stock_level_get($data['attribute']['sku']);
        if($check_stock_set && $level === NULL) {
          return form_error($form['cart'][$id]['count'], t('Not available'));
        }
      
        if($check_stock_0) {
          if($level == 0) {
            return form_error($form['cart'][$id]['count'], t('Out of stock'));
          }
          elseif($data['count'] > $level) {
            return form_error($form['cart'][$id]['count'], t('Only @num in stock', array('@num' => $level)));
          }
        }
      }
    
      $form_state['dcart_cart_products'][$id] = array(
        'uid' => $customer,
        'sku' => $data['attribute']['sku'],
        'count' => $data['count'],
        'amount' => $data['attribute']['amount'],
        'currency_code' => $data['attribute']['currency_code'],
      );
    }
  }
}

function dcart_cart_form_submit($form, &$form_state) {
  if($products = $form_state['dcart_cart_products']) {
    $order_id = dcart_checkout_order_save($products);
    drupal_goto('checkout/' . $order_id . '/' . DCART_CHECKOUT_START_PAGE);
  }
}

function dcart_cart_settings_form($form, &$form_state) {
  $form['dcart_cart_lifespan'] = array(
    '#title' => t('Cart lifespan'),
    '#type' => 'textfield',
    '#size' => 8,
    '#default_value' => variable_get('dcart_cart_lifespan', 31557600),
    '#element_validate' => array('element_validate_number'),
    '#description' => t('Set the length of time that products remain in the cart, in seconds. 31557600 seconds = 1year. Enter 0 to never delete cart items.'),
  );
  
  $form['dcart_cart_items_limit'] = array(
    '#title' => t('Max cart items'),
    '#type' => 'textfield',
    '#size' => 2,
    '#default_value' => variable_get('dcart_cart_items_limit', 0),
    '#element_validate' => array('element_validate_number'),
    '#description' => t('Limit maximum items in the cart by this value. If limit exceeded show warning message while user trying to add a new item. Enter 0 to disable limitation.'),
  );
  
  $form['dcart_wishlist_items_limit'] = array(
    '#title' => t('Max wishlist items'),
    '#type' => 'textfield',
    '#size' => 2,
    '#default_value' => variable_get('dcart_wishlist_items_limit', 20),
    '#element_validate' => array('element_validate_number'),
    '#description' => t('Limit maximum items in the wishlist. If limit exceeded show warning message while user trying to add a new item. Enter 0 to disable limitation.'),
  );
  
  $form['dcart_cart_items_per_page'] = array(
    '#title' => t('Items per page'),
    '#type' => 'textfield',
    '#size' => 2,
    '#default_value' => variable_get('dcart_cart_items_per_page', 10),
    '#element_validate' => array('element_validate_number'),
    '#description' => t('How many items to show on cart and wishlist page.'),
  );
  
  $form['dcart_cart_check_stock'] = array(
    '#title' => t('Check stock level'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('dcart_cart_check_stock', 1),
    '#description' => t('Check stock level for each attribute combination. It will cost at least one database query.'),
  );
  
  $form['dcart_cart_disable_stock_null'] = array(
    '#title' => t('Disable cart for unavailable products'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('dcart_cart_disable_stock_null', 0),
    '#description' => t('If SKU\'s stock level is 0 or not set, do not add product to cart.'),
    '#states' => array(
      'visible' => array(
        ':input[name="dcart_cart_check_stock"]' => array('checked' => TRUE),
      ),
    ),
  );

  return system_settings_form($form);
}

function theme_dcart_cart_short_formatter($variables) {
  $output = '';
  if(!empty($variables['items'])) {
    $attributes = $variables['items'];
    $node = $variables['node'];

    $output = '<div class="dcart-add-to-cart">';
    $output .= theme('dcart_price_range', array('items' => $attributes, 'price_components' => $variables['price_components']));
    
    $form = drupal_get_form('dcart_add_to_cart_short_form_' . $node->nid, $attributes, $node);
      
    if(variable_get('dcart_stock_inventory', 1) && variable_get('dcart_cart_check_stock', 1)) { 
      if(count($attributes) == 1) {
        $base_attribute = reset($attributes);
        $level = dcart_stock_level_get($base_attribute['sku']);
        $output .= theme('dcart_stock_level', array('attribute' => $base_attribute, 'level' => $level));

        if(variable_get('dcart_cart_disable_stock_null', 0) && empty($level)) {
          $form['#access'] = FALSE;
        }
      }
    }
    
    $output .= drupal_render($form);
    $output .= '</div>';
  }
  return $output;
}

function theme_dcart_cart_attributes_formatter($variables) {
  $form = drupal_get_form('dcart_add_to_cart_form', $variables['items'], $variables['node'], $variables['price_components']);
  return drupal_render($form);
}

function theme_dcart_cart_block($variables) {
  $output = '';
  if (user_access('dcart access cart') && !empty($variables['cart_content'])) {
    $products_total = 0;
    foreach($variables['cart_content'] as $item) {
      $amount = $item['attribute']['amount'];
      $quantity = $item['data']['count'];
      $total = $amount * $quantity;
      $products_total += $total;
    }
    $output = format_plural(count($variables['cart_content']),
      '<a href="@cart">In your cart 1 product</a>. Total: !total',
      '<a href="@cart">In your cart @count products</a>. Total: !total',
      array('!total' => theme('dcart_price', array(
        'amount' => $products_total)),
        '@cart' => url(variable_get('dcart_cart_url', 'cart'))));
  
    $output .= '<div class="checkout">' . l(t('Checkout'), 'checkout') . '</div>';
  }
  return $output;
}

function theme_dcart_empty_cart_block($variables) {
  return '<div class="cart-empty-block">' . t('Your shopping cart is empty.') . '</div>';
}

function theme_dcart_empty_cart_page($variables) {
  return '<div class="cart-empty-page">' . t('Your shopping cart is empty.') . ' ' . l(t('Continue shopping'),  '<front>') . '</div>';
}

function theme_dcart_wishlist_empty($variables) {
  return '<div class="wishlist-empty">' . t('Your wishlist is empty') . ' ' . l(t('Continue shopping'), '<front>') . '</div>';
}

function dcart_cart_wishlist() {
  return theme('dcart_wishlist');
}

function theme_dcart_cart_simple($variables) {
  $cart_items = dcart_cart_content_cached(dcart_customer_get());
  $count = count($cart_items);
  return '<span class="cart-simple">' . l(t('<span class="label">Cart</span> <span class="count">(@count)</span>', array(
    '@count' => $count ? $count : t('empty'))), variable_get('dcart_cart_url', 'cart'), array('html' => TRUE)) . '</span>';
}

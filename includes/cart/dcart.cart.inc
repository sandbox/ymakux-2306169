<?php

function dcart_cart_add_item($item, $combine = TRUE) {

  if(empty($item['uid'])) {
    $current_customer = dcart_customer_get();
    $lifespan = variable_get('dcart_cart_lifespan', 31557600);
    $uid = isset($current_customer) ? $current_customer : dcart_cookie_set('dcart_cart', $lifespan);
    $item['uid'] = $uid;
  }

  $item += dcart_cart_item_default();

  if(empty($item['count'])) {
    $item['count'] = 1;
  }

  if ($combine) {
    $item['count'] = dcart_cart_combine(DCART_CHECKOUT_READY, $item);
  }

  $result = dcart_cart_save($item);
  if($result !== TRUE) {
    return $result;
  }
  dcart_cart_clear_cache($item['uid']);
  return TRUE;
}

function dcart_cart_save($item) {

  $item += dcart_cart_item_default();
  
  $errors = array();
  foreach (module_implements('dcart_cart_presave') as $module) {
    $function = $module . '_dcart_cart_presave';
    $function($item, $errors);
  }
  
  if(!empty($item) && empty($errors)) {
    if(isset($item['data'])) {
      $item['data'] = serialize($item['data']);
    }
    
    db_merge('dcart_cart')
      ->key(array('id' => $item['id']))
      ->fields($item)
      ->execute();

    foreach (module_implements('dcart_cart_save') as $module) {
      $function = $module . '_dcart_cart_save';
      $function($item, $errors);
    }
  }
  return $errors ? $errors : TRUE;
}

function dcart_cart_item_default() {
  return array(
    'status' => DCART_CHECKOUT_READY,
    'created' => REQUEST_TIME,
    'uid' => dcart_customer_get(),
    'sku' => '',
    'count' => 1,
    'data' => array('ip' => $_SERVER['REMOTE_ADDR']),
    'id' => NULL,
  );
}

function dcart_dcart_cart_presave(&$item, &$errors) {
  if($error = dcart_cart_check_limit($item)) {
    $errors[] = $error;
    drupal_set_message($error, 'warning');
  }
}

function dcart_cart_check_limit($item) {
  if($limit = variable_get('dcart_cart_items_limit', 0)) {
    $existing = count(dcart_cart_items_get($item['uid'], DCART_CHECKOUT_READY));
    if($existing >= $limit) {
      return t('You can only add @count items to your cart. Please <a href="@url">remove</a> some items from your cart to add a new one.', array(
        '@count' => $existing, '@url' => url(variable_get('dcart_cart_url', 'cart'))));
    }
  }
}

function dcart_cart_clear_cache($uid) {
  $cid = DCART_CHECKOUT_READY . ':' . $uid;
  cache_clear_all($cid, 'cache_dcart_cart');
}

function dcart_cart_combine($status, $item) {
  $new_count = $item['count'];
  $similar = dcart_cart_items_get($item['uid'], $status, $item['sku']);
  if ($similar) {
    foreach ($similar as $id => $data) {
      $new_count += $data['count'];
    }
    dcart_cart_delete(array_keys($similar));
  }
  return $new_count;
}

function dcart_cart_items_get($customer = NULL, $status = NULL, $sku = NULL) {

  $cache_key = 'dcart_cart_items_' . implode('_', func_get_args());
  
  $results = &drupal_static($cache_key);
  if (!isset($results)) {

    $results = array();
    $query = db_select('dcart_cart', 'c')->fields('c');
  
    if(isset($status)) {
      $query->condition('c.status', $status);
    }

    if (isset($customer)) {
      $query->condition('c.uid', $customer);
    }
  
    if (isset($sku)) {
      $query->condition('c.sku', $sku);
    }
  
    $query->orderBy('created', 'DESC');

    if ($result = $query->execute()->fetchAll()) {
      foreach ($result as $key => $data) {
        $results[$data->id] =  (array) $data;
        $results[$data->id]['data'] = unserialize($data->data);
      }
    }
  }
  return $results;
}

function dcart_cart_load($id) {
  $result = db_select('dcart_cart', 'c')
    ->fields('c')
    ->condition('c.id', $id)
    ->execute()
    ->fetchObject();
    
  if($result) {
    $result->data = unserialize($result->data);
    return (array) $result;
  }
  return array();
}

function dcart_cart_content($customer = NULL, $status = DCART_CHECKOUT_READY) {
  $content = &drupal_static(__FUNCTION__);
  if (!isset($content)) {
    if (!isset($customer)) {
      $customer = dcart_customer_get();
    }
    if (isset($customer) && ($items = dcart_cart_items_get($customer, $status))) {
      $content = $items;
    }  
  }
  return $content;
}

function dcart_cart_content_build($customer = NULL, $type = DCART_CHECKOUT_READY, $cart_content = NULL) {
  
  if (!isset($cart_content)) {
    $cart_content = dcart_cart_content($customer, $type);
  }
  
  $build = $default_image = array();
  $properties = dcart_attribute_properties();
  if($cart_content) {
  foreach ($cart_content as $id => $data) {
    $sku = $data['sku'];
    if ($node = dcart_get_node_by_sku($sku, TRUE)) {
      $attribute = dcart_product_attributes_get($node, $sku);
      $instances = field_info_instances('node', $node->type);
      
      foreach ($instances as $field_name => $field_data) {
        if (!empty($field_data['dcart_settings']['cart_icon'])) {
          if ($items = field_get_items('node', $node, $field_name, $node->language)) {
            $default_image = reset($items);
          }
          break;
        }
      }

      $build[$id] = array(
        'node' => $node,
        'attribute' => $attribute,
        'data' => $data,
      );
      
      if (isset($attribute['data']['properties']['image_field']['fid'])) {
        if ($function = dcart_hook_callback($properties['image_field'], 'get')) {

          $property = $function($attribute, $node);

          $image_field = $property['field_name'];
          $instance = $instances[$image_field];
          if (!empty($instance['dcart_settings']['cart_icon']) && isset($property['field_item'])) {
            $build[$id]['image'] = $property['field_item'];
          }
        }
      }
      else {
        $build[$id]['image'] = $default_image;
      }      
    }
  }
  }
  return $build;
}

function dcart_cart_content_cached($customer, $status = DCART_CHECKOUT_READY, $cart_content = NULL, $expire = CACHE_PERMANENT) {
  $cache_key = $status . ':' . $customer;
  $content = &drupal_static('dcart_cart_content_cached:' . $cache_key);
  if(!isset($content)) {
    if($cached = cache_get($cache_key, 'cache_dcart_cart'))  {
      $content = $cached->data;
    }

    if(!isset($content)) {
      $content = dcart_cart_content_build($customer, $status, $cart_content);
      cache_set($cache_key, $content, 'cache_dcart_cart', $expire);
    }
  }
  return $content;
}

function dcart_cart_delete($id = NULL, $uid = NULL, $status = NULL, $sku = NULL) {
  
  foreach (module_implements('dcart_cart_delete') as $module) {
    $function = $module . '_dcart_cart_delete';
    $function($id, $uid, $status);
  }

  if(isset($id)) {
    $item = dcart_cart_load($id);
    dcart_cart_clear_cache($item['uid']);
  }
  elseif(isset($uid)) {
    dcart_cart_clear_cache($uid);
  }
  
  $query = db_delete('dcart_cart');
  if (isset($id)) {
    $query->condition('id', (array) $id, 'IN');
  }
  
  if(isset($uid)) {
    $query->condition('uid', $uid);
  }
  
  if(isset($sku)) {
    $query->condition('sku', (array) $sku, 'IN');
  }
  
  if(isset($status)) {
    $query->condition('status', $status);
  }
  
  return $query->execute();
}

function dcart_cart_update($id, $fields) {
  
  $ids = (array) $id;
  foreach (module_implements('dcart_cart_update') as $module) {
    $function = $module . '_dcart_cart_update';
    $function($ids, $fields);
  }

  return db_update('dcart_cart')
    ->fields($fields)
    ->condition('id', $ids, 'IN')
    ->execute();
}

function dcart_add_to_cart_short_form($form, &$form_state, $attributes, $node) {

  $form_state['attributes'] = $attributes;
  $form_state['node'] = $node;

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add to cart'),
    '#access' => user_access('dcart add to cart'),
    '#submit' => array('dcart_add_to_cart_short_form_submit'),
    '#validate' => array(),
    '#limit_validation_errors' => array(),
  );
  return $form;
}


function dcart_add_to_cart_short_form_submit($form, &$form_state) {

  if(count($form_state['attributes']) > 1) {
    drupal_set_message('Please select product options before adding to cart.', 'warning');
    $form_state['redirect'] = array('node/' . $form_state['node']->nid, array('query' => drupal_get_destination()));
  }
  else {
    $form_state['sku'] = $form_state['attributes'][0]['sku'];
    dcart_add_to_cart_form_submit($form, $form_state);
  }
}

function dcart_add_to_cart_form($form, &$form_state, $items, $node, $price_components = FALSE) {

  form_load_include($form_state, 'inc', 'dcart', 'includes/cart/dcart.cart.pages');
  
  $form_state['dcart_price_components'] = $price_components;
  $form['#prefix'] = '<div class="dcart-attributes-wrapper">';
  $form['#suffix'] = '</div>';
  
  $form['sku']['#markup'] = $form['amount']['#markup'] = $form['stock']['#markup'] = '';
  $form['node'] = array('#type' => 'value', '#value' => $node);
  
  $attributes = array();
  foreach ($items as $delta => $item) {
    if ($delta === 0) {
      $sku = $item['sku'];
      $amount = theme('dcart_price_components', array('item' => $item, 'price_components' => $price_components));
      if(variable_get('dcart_stock_inventory', 1)) {
        $stock = theme('dcart_stock_level', array('sku' => $sku));
      }
    }
    $attributes[$delta] = $item;
  }
  
  if(isset($sku) && isset($amount)) {
    $form['sku']['#markup'] = '<div class="dcart-sku-refreshable">' . t('Item #!sku', array('!sku' => $sku)) . '</div>';
    $form['amount']['#markup'] = '<div class="dcart-amount-refreshable">' . $amount . '</div>';
  
    if(variable_get('dcart_stock_inventory', 1)) {
      $form['stock']['#markup'] = '<div class="dcart-stock-refreshable">' . $stock . '</div>';
    }
  }
  
  $form_state['node'] = $node;
  $form_state['attribute_properties'] = dcart_attribute_properties();
  $form_state['attributes'] = $attributes;

  $form_state['instances'] = field_info_instances('node', $node->type);
  $attribute_widget_types = dcart_attribute_widget_types();
  
  $preset_attribute_fields = array();
  foreach ($attributes as $delta => $attribute) {
    if (!empty($attribute['status']) && !empty($attribute['data']['fields'])) {
      foreach ($attribute['data']['fields'] as $field_name => $field_value) {
        $attribute_fields[$field_name][$field_value] = $field_value;
      }

      if(isset($_GET['sku']) && $attribute['sku'] == $_GET['sku']) {
        $preset_attribute = $attribute;
        $preset_attribute_fields[$field_name][$field_value] = $field_value;
      }
    }
  }

  if(isset($attribute_fields)) {
    foreach ($form_state['instances'] as $field_name => $instance) {
      if(isset($attribute_fields[$field_name])) {
        $field = field_info_field($field_name);
        if ($field && $field['cardinality'] == 1 && !empty($instance['dcart_settings']['attribute_field'])) {
          if(empty($attribute_widget_types[$instance['dcart_settings']['attribute_widget']])) {
            continue;
          } 

          $attribute_widget_type = $attribute_widget_types[$instance['dcart_settings']['attribute_widget']];

          if(!empty($attribute_widget_type['file']) && file_exists($attribute_widget_type['file'])) {
            $form_state['build_info']['files'][] = $attribute_widget_type['file'];
          }

          $properties = _options_properties($attribute_widget_type['type'], FALSE, TRUE, TRUE);
          if($properties_callback = dcart_hook_callback($attribute_widget_type, 'properties')) {
            $properties = $properties_callback();
          }

          if($allowed_values = _dcart_field_allowed_values($field_name, $instance, $properties)) {

            if($options = array_intersect_key($allowed_values, $attribute_fields[$field_name])) {
              $default_value = dcart_attribute_default_value($field_name, $node, $allowed_values);

              if(isset($default_value)) {
                $options = array($default_value => $allowed_values[$default_value]) + $options;
              }

              $default_attribute_fields[$field_name] = $default_value;
            
              if(isset($preset_attribute_fields[$field_name])) {
                $default_value = key($preset_attribute_fields[$field_name]);
              }

              array_walk($options, function(&$value, $index){
                if(dcart_translatable_string($value)) {
                  $value = t($value);
                }
              }); 
            
              $element = array(
                '#title' => dcart_translatable_string($instance['label']) ? t($instance['label']) : check_plain($instance['label']),
                '#options' => $options,
                '#weight' => $instance['widget']['weight'],
                '#default_value' => isset($default_value) ? $default_value : key($options),
                '#ajax' => array(
                  'callback' => 'dcart_cart_attributes_form_refresh',
                  'progress' => array('message' => ''),
                ),
              );
               
              if($element_callback = dcart_hook_callback($attribute_widget_type, 'element')){
                $form['attributes'][$field_name] = $element_callback($element, $attributes, $node);
              }
              else {
                $element['#type'] = 'select';
                $form['attributes'][$field_name] = $element;
              }
            }
          }
        }
      }
    }
  }
  
  if(isset($default_attribute_fields)) {
    $form_state['attributes'][0]['data']['fields'] = $default_attribute_fields;
  }

  if (!empty($form['attributes'])) {
     $form['attributes'] += array(
       '#tree' => TRUE,
       '#prefix' => '<div id="product-attributes" class="attribute-widgets">',
       '#suffix' => '</div>',
       '#weight' => 0,
    );
  }
  
  $form['count'] = array(
    '#title' => t('Number of products'),
    '#title_display' => 'invisible',
    '#type' => 'textfield',
    '#weight' => 40,
    '#size' => 2,
    '#access' => user_access('dcart add to cart'),
    '#attributes' => array('autocomplete' => 'off'),
    '#default_value' => 1,
  ); 

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add to cart'),
    '#weight' => 50,
    '#access' => user_access('dcart add to cart'),
    '#attributes' => array('id' => 'add-cart'),
    '#submit' => array('dcart_add_to_cart_form_submit'),
    '#validate' => array('dcart_add_to_cart_form_validate'),
    '#prefix' => '<div class="add-cart">',
    '#suffix' => '</div>',
  );
  
  $form['wishlist'] = array(
    '#type' => 'submit',
    '#value' => t('Add to wishlist'),
    '#weight' => 60,
    '#access' => user_access('dcart add to wishlist'),
    '#attributes' => array('id' => 'add-wishlist'),
    '#submit' => array('dcart_add_to_wishlist_submit'),
    '#validate' => array('dcart_add_to_cart_form_validate'),
    '#prefix' => '<div class="add-wishlist">',
    '#suffix' => '</div>',
  );

  $form['#attributes']['class'][] = drupal_html_class('dcart-add-to-cart');
  $form['#attributes']['class'][] = drupal_html_class(dcart_cart_form_id(array_keys($attributes)));
  return $form;
}

function dcart_add_to_wishlist_submit($form, $form_state) {
  dcart_cart_add_to_wishlist($form_state['sku'], $form_state['node']);
}

function _dcart_form_get_attribute($form_state) {
  if(empty($form_state['attributes'])) {
    return NULL;
  }
  if(empty($form_state['values']['attributes'])) {
    return reset($form_state['attributes']);
  }
  $submitted_fields = array_filter($form_state['values']['attributes']);
  foreach ($form_state['attributes'] as $attribute) {
    if(empty($attribute['status'])) {
      continue;
    }
    if(!empty($attribute['data']['fields'])) {
      $attribute_fields = $attribute['data']['fields'];
      if (!array_diff($submitted_fields, $attribute_fields)) {
        return $attribute;
      }
    }
  }
  return NULL;
}

function _dcart_form_refresh_fields($attribute, &$commands, &$form_state) {
  if (!empty($attribute['data']['properties'])) {
    $node = $form_state['node'];
    foreach ($attribute['data']['properties'] as $property_name => $property_data) {
      if($function = dcart_hook_callback($form_state['attribute_properties'][$property_name], 'get')) {
        $field = $function($attribute, $node);
        $settings = array('dcart' => array(
          'attribute_properties' => array($property_name => $property_data),
        ));
        $commands[] = ajax_command_settings($settings, TRUE);
      }

      if (isset($field['field_name'])) {
        $field_name = $field['field_name'];
        $form_state['attribute_properties'][$property_name]['field'] = $field;
        
        if(isset($field['field_item'])) {
          $field_item = $field['field_item'];
        }
        elseif(isset($field['field_items'])) {
          $field_item = reset($field['field_items']);
        }
        
        if(isset($field_item)) {        
          $display_settings = array('settings' => $form_state['instances'][$field_name]['display']['default']['settings']);
          $replacement = render(field_view_value('node', $node, $field_name, $field_item, $display_settings, $node->language));
          $field_class = 'dcart-field-refreshable-' . str_replace('_', '-', $field_name);
          $replacement = '<div class="' . $field_class . '">' . $replacement . '</div>';
          $commands[] = ajax_command_replace('div.' . $field_class, $replacement);
        }
      }
    }
  }
}

function _dcart_form_refresh_block_buttons(&$commands, $block = FALSE) {
  $input_selector = 'form#dcart-add-to-cart-form input[type="submit"]';
  $form_selector = 'form#dcart-add-to-cart-form';
 
  if($block) {
    $commands[] = ajax_command_invoke($input_selector, 'addClass', array('disabled'));
    $commands[] = ajax_command_invoke($form_selector, 'addClass', array('disabled'));
  }
  else {
    $commands[] = ajax_command_invoke($input_selector, 'removeClass', array('disabled'));
    $commands[] = ajax_command_invoke($form_selector, 'removeClass', array('disabled'));
  }
}

function _dcart_form_refresh_default($attribute, &$commands, $form_state) {
  _dcart_form_refresh_block_buttons($commands, FALSE);
  $commands[] = ajax_command_replace('div.dcart-sku-refreshable', '<div class="dcart-sku-refreshable">' . t('Item #!sku', array('!sku' => $attribute['sku'])) . '</div>');
  $price = theme('dcart_price_components', array('item' => $attribute, 'price_components' => $form_state['dcart_price_components']));
  $commands[] = ajax_command_replace('div.dcart-amount-refreshable', '<div class="dcart-amount-refreshable">' . $price . '</div>');
}

function _dcart_form_refresh_stock($attribute, &$commands, $form_state) {
  if(variable_get('dcart_stock_inventory', 1) && variable_get('dcart_cart_check_stock', 1)) {
    $level = dcart_stock_level_get($attribute['sku']);
    $commands[] = ajax_command_replace('div.dcart-stock-refreshable', '<div class="dcart-stock-refreshable">' . theme('dcart_stock_level', array('attribute' => $attribute, 'level' => $level)) . '</div>');

    if(variable_get('dcart_cart_disable_stock_null', 0)) {
      $disabled = ($level > 0);
    }
  }
}

function _dcart_form_refresh_unavailable($attribute, &$commands, $form_state) {
  _dcart_form_refresh_block_buttons($commands, TRUE);
  $commands[] = ajax_command_replace('div.dcart-sku-refreshable', '<div class="dcart-sku-refreshable">' . t('This combination unavailable.') . '</div>');
  $commands[] = ajax_command_replace('div.dcart-amount-refreshable', '<div class="dcart-amount-refreshable"></div>');
  $commands[] = ajax_command_replace('div.dcart-stock-refreshable', '<div class="dcart-stock-refreshable"></div>');
}

function dcart_cart_attributes_form_refresh($form, $form_state) {
  $commands = array();
  $attribute = _dcart_form_get_attribute($form_state);
  $settings = array('dcart' => array(
    'attribute' => $attribute,
    'attributes' => $form_state['attributes'],
  ));
  $commands[] = ajax_command_settings($settings, TRUE);

  if ($attribute) {
    _dcart_form_refresh_default($attribute, $commands, $form_state);
    _dcart_form_refresh_stock($attribute, $commands, $form_state);
    _dcart_form_refresh_fields($attribute, $commands, $form_state);
  }
  else {
    _dcart_form_refresh_unavailable($attribute, $commands, $form_state);
  }
  // Allow other modules to add arbitrary AJAX commands on the refresh.
  drupal_alter('dcart_refresh_attributes', $attribute, $commands, $form_state);
  return array('#type' => 'ajax', '#commands' => $commands);
}

function dcart_add_to_cart_form_validate($form, &$form_state) {
  $attribute = _dcart_form_get_attribute($form_state);
  if(empty($attribute['sku'])) {
    return form_set_error('', t('An error occurred adding product to your cart. Please try again.'));
  }
  $form_state['sku'] = $attribute['sku'];
  $form_state['values']['count'] = !empty($form_state['values']['count']) ? intval($form_state['values']['count']) : 1;
}

function dcart_add_to_cart_form_submit($form, $form_state) {
  $sku = $form_state['sku'];
  $count = !empty($form_state['values']['count']) ? intval($form_state['values']['count']) : 1;
  $added = dcart_cart_add_item(array('sku' => $sku, 'count' => $count), variable_get('dcart_cart_combine_items', 1));
  if($added === TRUE) {
    $actions = array('dcart_action_cart_message', 'dcart_action_cart_redirect');
    dcart_action_perform($actions, $form_state['node'], array('sku' => $sku));
  }
}

function dcart_cart_form_id($skus) {
  $string = implode('_', $skus);
  if (strlen($string) > 50) {
    $string = drupal_hash_base64($string);
  }
  return 'dcart_add_to_cart_form_' . $string;
}

function dcart_cart_access($item, $op) {
  $customer = dcart_customer_get();
  switch($op) {
    case 'edit':
      if(user_access('dcart edit any cart')) {
        return TRUE;
      }
      if(user_access('dcart edit own cart')) {
        if($item['uid'] == $customer) {
          return TRUE;
        }
      }
      break;
    case 'add_to_wishlist' :
      if(user_access('dcart add to wishlist')) {
        if($item['uid'] == $customer) {
          return TRUE;
        }
      }
     break;
  }
  return FALSE;
}

function dcart_cart_add_to_wishlist($sku, $node, $message = TRUE) {
  if($error = _dcart_cart_wishlist_validate($sku)) {
    return $message ? drupal_set_message($error, 'warning') : FALSE;
  }
  $result = dcart_cart_save(array('status' => DCART_CHECKOUT_POSTPONED, 'sku' => $sku));
  if($result !== TRUE) {
    return $message ? drupal_set_message($result, 'error') : FALSE;
  }
  dcart_action_perform('dcart_action_wishlist_message', $node, array('sku' => $sku));
}

function _dcart_cart_wishlist_validate($sku) {
  if($wishlist_items = dcart_cart_items_get(dcart_customer_get(), DCART_CHECKOUT_POSTPONED)) {
    foreach($wishlist_items as $wishlist_item) {
      if($sku == $wishlist_item['sku']) {
        return t('Already in <a href="@url">wishlist</a>', array('@url' => url('cart/wishlist')));
      }
    }

    $max_wishlist_size = variable_get('dcart_wishlist_items_limit', 20);
    if(count($wishlist_items) >= $max_wishlist_size) {
      $plural = 'Sorry but you can\'t have more than @count items in your wishlist. Please <a href="@url">remove</a> some items from wishlist before adding a new one.';
      return format_plural($max_wishlist_size, $plural, $plural, array('@url' => url('cart/wishlist')), 'warning');
    }
  }
  return FALSE;
}


function dcart_cart_move_to_wishlist($cart_item, $message = TRUE) {
  
  if(is_numeric($cart_item)) {
    $cart_item = dcart_cart_load($cart_item);
  }

  if(!empty($cart_item['sku'])) {
    if($error = _dcart_cart_wishlist_validate($cart_item['sku'])) {
      return drupal_set_message($error, 'warning');
    }
    
    dcart_cart_update($cart_item['id'], array('status' => DCART_CHECKOUT_POSTPONED));
    dcart_cart_clear_cache($cart_item['uid']);
  }
}

function dcart_cart_move_from_wishlist($cart_item, $message = TRUE) {
  if(is_numeric($cart_item)) {
    $cart_item = dcart_cart_load($cart_item);
  }
  $cart_item['status'] = DCART_CHECKOUT_READY;
  $add = dcart_cart_add_item($cart_item);

  if($add === TRUE && $message) {
    $message = t('Product has been moved to <a href="@cart">your cart</a>', array(
      '@cart' => url(variable_get('dcart_cart_url', 'cart'))));
    drupal_set_message($message);
  }
  return array('result' => $add, 'item' => $cart_item);
}

function dcart_preprocess_dcart_wishlist(&$variables) {

  $variables['rows'] = array();
  $variables['empty'] = theme('dcart_wishlist_empty');
  $default_image_field = variable_get('dcart_product_image_field', 'dcart_product_image');

  $query = db_select('dcart_cart', 'c')
    ->fields('c')
    ->condition('c.status', DCART_CHECKOUT_POSTPONED)
    ->condition('c.uid', dcart_customer_get())
    ->orderBy('created', 'DESC')
    ->extend('PagerDefault')
    ->limit(variable_get('dcart_cart_items_per_page', 5));

  if($items = $query->execute()) {
    foreach($items as $id => $item) {
      $id = $item->id;
      $sku = $item->sku;
      if($node = dcart_get_node_by_sku($sku, TRUE)) {
        $attribute = dcart_product_attributes_get($node, $sku);
        $existing_node = node_load($node->nid);
        $item_unavailable = empty($existing_node->status);
      
        if (isset($attribute['data']['properties']['image_field']['field_name'])) {
          $image_field = $attribute['data']['properties']['image_field']['field_name'];
          if(!empty($node->{$image_field}[LANGUAGE_NONE][0]['uri'])) {
            $image_uri = $node->{$image_field}[LANGUAGE_NONE][0]['uri'];
          }
        }
        else {
          $instances = field_info_instances('node', $node->type);
          foreach ($instances as $field_name => $field_data) {
            if (!empty($field_data['dcart_settings']['cart_icon']) 
            && ($items = field_get_items('node', $node, $field_name, $node->language))) {
              $image_uri = $items[0]['uri'];
              break;
            }
          }        
        }

        if(isset($image_uri)) {
          $variables['rows'][$id]['image'] = array(
            '#theme' => 'image_style',
            '#style_name' => 'dcart_cart',
            '#path' => $image_uri,
          );
        }

        if(user_access('dcart add to cart')) {
          $variables['rows'][$id]['add_to_cart'] = array(
            '#theme' => 'link',
            '#text' => '<i class="fa fa-shopping-cart"></i><span>' . t('Move to cart') . '</span>',
            '#path' => 'operations/cart/to-cart/' . $id,
            '#options' => array(
              'query' => array('destination' => current_path()),
              'attributes' => array('class' => array('cart'), 'title' => t('Move to cart')),
              'html' => TRUE,
            ),
          );
        }
      
        if(dcart_cart_access((array) $item, 'edit')) {
          $variables['rows'][$id]['remove'] = array(
            '#theme' => 'link',
            '#text' => '<i class="fa fa-trash-o"></i><span>' . t('Remove') . '</span>',
            '#path' => 'operations/cart/remove/' . $id,
            '#options' => array(
              'query' => array('destination' => current_path()),
              'attributes' => array('class' => array('remove'), 'title' => t('Remove from the cart')),
              'html' => TRUE,
            ),
          );
        }
      
        $title = l($node->title, 'node/' . $node->nid, array('query' => array('sku' => $sku)));
        if($item_unavailable) {
          $title = $node->title . ' ' . t('(unavailable)');
        }
      
        $variables['rows'][$id]['title'] = $title;
        $variables['rows'][$id]['attributes'] = array(
          '#theme' => 'dcart_product_attributes',
          '#node' => $node,
          '#attribute' => $attribute,
        );
      }
    }
  }
}

function dcart_preprocess_dcart_cart(&$variables) {
  $variables['items'] = array();
  
  $variables['empty'] = theme('dcart_empty_cart_page');
  $default_image_field = variable_get('dcart_product_image_field', 'dcart_product_image');
  $content = isset($variables['cart_content']) ? $variables['cart_content'] : dcart_cart_content_build();
  
  if (user_access('dcart access cart') && $content) {
    $products_total = 0;
    foreach($content as $key => $item) {
      $image = '';
      if (isset($item['image']['uri'])) {
        $image_uri = $item['image']['uri'];
      }
      elseif(field_info_field($default_image_field)) {
        if(!empty($item['node']->{$default_image_field}[LANGUAGE_NONE][0]['uri'])) {
          $image_uri = $item['node']->{$default_image_field}[LANGUAGE_NONE][0]['uri'];
        }
      }
 
      $amount = $item['attribute']['amount'];
      $quantity = $item['data']['count'];
      
      $total = $amount * $quantity;
      $products_total += $total;
      
      $variables['items'][$key]['total'] = array(
         '#theme' => 'dcart_price',
         '#amount' => $total,
       );

      $variables['items'][$key]['image'] = '';
      
      if(isset($image_uri)) {
        $image = theme('image_style', array('style_name' => 'dcart_cart', 'path' => $image_uri));
        $image = l($image, 'node/' . $item['node']->nid, array(
          'html' => TRUE,
          'attributes' => array(
            'target' => '_blank'),
          'query' => array('sku' => $item['attribute']['sku'])));
        $variables['items'][$key]['image'] = $image;
      }

      $variables['items'][$key]['title'] = array(
        '#theme' => 'link',
        '#text' => $item['node']->title,
        '#path' => 'node/' . $item['node']->nid,
        '#options' => array(
          'attributes' => array('class' => array('product'), 'target' => '_blank'),
          'html' => FALSE,
        ),
      );

      $variables['items'][$key]['attributes'] = array(
        '#theme' => 'dcart_product_attributes',
        '#node' => $item['node'],
        '#attribute' => $item['attribute'],
      );
      $variables['items'][$key]['price'] = array(
        '#theme' => 'dcart_price',
        '#amount' => $item['attribute']['amount'],
      );
      $variables['items'][$key]['count'] = (int) $item['data']['count'];
    }
    $variables['total'] = array(
      '#theme' => 'dcart_price',
      '#amount' => $products_total,
    );
  }
}

<?php

function dcart_catalog_filter_form($form, $form_state) {
  
  $parameters = drupal_get_query_parameters();
  list($default_filter_type, $default_sort_order) = explode(':', variable_get('dcart_catalog_default_sort', 'sort_price:asc'));
  $view_default = variable_get('dcart_catalog_default_view', 'grid');
  
  foreach(dcart_catalog_filters() as $filter_type => $filter_data) {
    
    $default_value = key($filter_data['options']);
    if(isset($parameters[$filter_type]) && isset($filter_data['options'][$parameters[$filter_type]])) {
      $default_value = $parameters[$filter_type];
    }
    elseif($filter_type == $default_filter_type) {
      $default_value = $default_sort_order;
    }
    elseif($filter_type == 'view') {
      $default_value = $view_default;
    }
    foreach($filter_data['options'] as $name => &$label) {
      $filter_data['options'][$name] = $filter_data['title'] . ': ' . $label;
    }
    
    $form['elements'][$filter_type] = array(
      '#type' => 'select',
      '#options' => $filter_data['options'],
      '#default_value' => $default_value,
    );    
  }
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Ok'),
  );
  
  $js = "
    (function ($) {
      Drupal.behaviors.dCartCatalog = {
        attach: function (context, settings) {
          var submit = $('#dcart-catalog-filter-form input:submit');
          submit.hide();
          $('#dcart-catalog-filter-form select').change(function(){
            submit.click();
          });
        }
      }
    })(jQuery);";
  
  $form['#attached']['js'][] = array('data' => $js, 'type' => 'inline');
  return $form;
}

function dcart_catalog_filter_form_submit($form, $form_state) {
  form_state_values_clean($form_state);
  $query = $form_state['values'] + $_GET;
  $q = $query['q'];
  unset($query['q']);
  drupal_goto($q, array('query' => $query));
}

function dcart_catalog_settings_form($form, $form_state) {

  $form['#submit'][] = 'dcart_catalog_settings_form_submit';
  
  $builders = array(0 => t('- Select -'));
  foreach(module_invoke_all('dcart_catalog_build') as $name => $builder) {
    $builders[$name] = t($builder['title']);
  }

  $form['dcart_catalog_builder'] = array(
    '#type' => 'select',
    '#title' => t('Catalog builder'),
    '#options' => $builders,
    '#default_value' => dcart_catalog_builder(),
    '#description' => t('Select a builder that will replace standart output of taxonomy terms for product catalog'),
  );
  
  $options = array();
  foreach(field_info_fields() as $field_name => $field_data) {
    if($field_data['type'] == 'taxonomy_term_reference') {
      $options[$field_name] = $field_name;
    }
  }

  $form['dcart_catalog_category_field_name'] = array(
    '#type' => 'select',
    '#title' => t('Category field'),
    '#options' => $options,
    '#default_value' => variable_get('dcart_catalog_category_field_name', ''),
    '#description' => t('Select taxonomy reference field to be used as category tag.'),
  );
  
  $form['dcart_catalog_products_per_page'] = array(
    '#type' => 'textfield',
    '#title' => t('Products per page'),
    '#size' => 2,
    '#maxlength' => 2,
    '#default_value' => variable_get('dcart_catalog_products_per_page', 20),
    '#element_validate' => array('dcart_element_validate_numeric_positive'),
    '#description' => t('Maximum of products to show per page. Must be integer positive value'),
  );

  $sort_options = $view_options = array();
  foreach(module_invoke_all('dcart_catalog_filters') as $filter_name => $filter_data) {
    if(strpos($filter_name, 'sort_') !== FALSE) {
      foreach($filter_data['options'] as $order => $label) {
        $sort_options[$filter_name . ':' . $order] = check_plain($filter_data['title'] . ': ' . $label);
      }
    }
    elseif($filter_name == 'view') {
      foreach($filter_data['options'] as $view_name => $label) {
        $view_options[$view_name] = check_plain($label);
      }
    }
  }

  $form['dcart_catalog_default_view'] = array(
    '#type' => 'select',
    '#title' => t('Default view'),
    '#options' => $view_options,
    '#default_value' => variable_get('dcart_catalog_default_view', 'grid'),
    '#description' => t('Choose how to display catalog items. Core provides grid and list view.'),
  );

  return system_settings_form($form);
}

function dcart_catalog_settings_form_submit($form, $form_state) {
  if($catalog_field = $form_state['values']['dcart_catalog_category_field_name']) {
    $field_info = field_info_field($catalog_field);
    $name = $field_info['settings']['allowed_values'][0]['vocabulary'];
    $vocab = taxonomy_vocabulary_machine_name_load($name);
    variable_set('dcart_catalog_vocabulary', $vocab->machine_name . '|' . $vocab->vid);
  }
}

function dcart_catalog_default_build($term) {

   $build = array('#markup' => theme('dcart_catalog_noresults'));
   $children = taxonomy_get_children($term->tid, $term->vid);
   
   if(!$children) {
     $per_page = variable_get('dcart_catalog_products_per_page', 20);
     if ($nids = taxonomy_select_nodes($term->tid, TRUE, $per_page)) {
        $build = array();
        if($filter_form = dcart_catalog_get_filter()) {
          $build['dcart_filter'] = array('#markup' => $filter_form);
        }
        
        $nodes = node_load_multiple($nids);
        $url_view = dcart_catalog_filter_parameters('view');
        $view = $url_view ? $url_view : variable_get('dcart_catalog_default_view', 'grid');
        $display = ($view == 'list') ? 'dcart_catalog_list' : 'dcart_catalog';
        
        $build['dcart_catalog'] = node_view_multiple($nodes, $display);
        $build['pager'] = array('#theme' => 'pager', '#weight' => 5);
     }
   }
   else {
     $build = array('categories' => taxonomy_term_view_multiple($children, 'dcart_category'));
   }
   return $build;
}


function dcart_catalog_builder_settings_form($form, &$form_state, $name = NULL, $builder = NULL) {
  if($builder) {
    drupal_set_title(t('Settings for %builder', array('%builder' => $builder['title'])), PASS_THROUGH);
    $form_state['dcart_catalog_builder'] = array($name => $builder);
    $callback = $builder['callback'] . '_settings_form';
    $form += $callback($form, $form_state);
  }

  $form['submit'] = array(
    '#value' => t('Submit'),
    '#type' => 'submit',
    '#access' => $form,
  );
  return $form;
}

function dcart_catalog_builder_settings_form_submit($form, $form_state) {

  form_state_values_clean($form_state);
  $bulder_name = key($form_state['dcart_catalog_builder']);

  $settings = dcart_catalog_builder_settings();
  $settings[$bulder_name] = $form_state['values'];
  variable_set('dcart_catalog_builder_settings', $settings);

  drupal_set_message(t('The configuration options have been saved.'));
}

function theme_dcart_catalog_noresults($variables) {
  return t('No content to display. If the problem persists, please contact the site administrator.');
}

function theme_dcart_solr_results_found($variables) {
  return '<div class="catalog-results">' . t('Showing @num from @total', array('@num' => $variables['number'], '@total' => $variables['total'])) . '</div>';
}

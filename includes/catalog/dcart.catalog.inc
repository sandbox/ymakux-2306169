<?php

function dcart_dcart_catalog_filters() {
  return array(
    'view' => array(
      'title' => t('Show as'),
      'options' => array(
        'grid' => t('Grid'),
        'list' => t('List'),
      ),
      'mapping' => array(
        'list' => 'dcart_catalog_list',
        'grid' => 'dcart_catalog',
      ),
    ),
  );
}

function dcart_catalog_filters() {
  $filters = &drupal_static(__FUNCTION__);
  if (!isset($filters)) {
    $filters = module_invoke_all('dcart_catalog_filters');
    drupal_alter('dcart_catalog_filters', $filters);
  }
  return $filters;
}

function dcart_catalog_builder() {
  return variable_get('dcart_catalog_builder', 'default');
}

function dcart_catalog_term($term) {
  return ($term->vid == dcart_catalog_vocabulary(1));
}

function dcart_catalog_vocabulary($part) {
  $parts = explode('|', variable_get('dcart_catalog_vocabulary', ''), 2);
  return isset($parts[$part]) ? $parts[$part] : FALSE;
}

function dcart_catalog_filter_parameters($parameter = NULL) {
  $parameters = array();
  $filter_keys = array_keys(dcart_catalog_filters());
  foreach($_GET as $key => $value) {
    if(in_array($key, $filter_keys)) {
      $parameters[$key] = check_plain(rawurldecode($value));
    }
  }
  
  if(isset($parameter)) {
    return isset($parameters[$parameter]) ? $parameters[$parameter] : NULL;
  }
  
  ksort($parameters);
  return $parameters;
}

function dcart_catalog_get_filter($exclude = array()) {
  module_load_include('inc', 'dcart', 'includes/catalog/dcart.catalog.pages');
  
  $form = drupal_get_form('dcart_catalog_filter_form');

  if($exclude) {
    foreach($exclude as $element_name) {
      $string = $element_name;
      if(substr($element_name, -1) == "*") {
        $string = substr_replace($element_name, "_", -1);
      }
      
      foreach($form['elements'] as $name => $data) {
        if(substr($name, 0, strlen($string)) == $string) {
          unset($form['elements'][$name]);
        }
      }
    }
  }
  return empty($form['elements']) ? '' : drupal_render($form);
}

function dcart_catalog_set_breadcrumb($term) {
  $breadcrumb = array();
  $current = (object) array('tid' => $term->tid);
  while ($parents = taxonomy_get_parents($current->tid)) {
    $current = array_shift($parents);
    $breadcrumb[] = l($current->name, 'taxonomy/term/' . $current->tid);
  }
  $breadcrumb[] = l(t('Home'), NULL);
  $breadcrumb = array_reverse($breadcrumb);
  drupal_set_breadcrumb($breadcrumb);
}

function dcart_catalog_build($builder, $term) {
  $builders = dcart_catalog_builders_get();
  if(isset($builders[$builder]) && ($callback = dcart_hook_callback($builders[$builder]))) {
    return $callback($term);
  }
}

function dcart_catalog_builder_settings($builder_name = NULL) {
  $settings = variable_get('dcart_catalog_builder_settings', array());
  if(isset($builder_name)) {
    return isset($settings[$builder_name]) ? $settings[$builder_name] : FALSE;
  }
  return $settings;
}




function dcart_catalog_builders_get() {
  $builders = &drupal_static(__FUNCTION__);
  if(!isset($builders)) {
    $builders = module_invoke_all('dcart_catalog_build');
  }
  return $builders;
}


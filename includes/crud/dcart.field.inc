<?php


function dcart_dcart_field_getter_info() {
  return array(
    'callback' => 'dcart_field_getter',
    'file' => drupal_get_path('module', 'dcart') . '/includes/crud/callbacks/getter.inc',
  );
}

function dcart_dcart_field_setter_info() {
  $file = drupal_get_path('module', 'dcart') . '/includes/crud/callbacks/setter.inc';
  return array(
    'callback' => 'dcart_field_setter',
    'file' => $file,
  );
}


function dcart_field_clean_text($text) {
  return strip_tags(check_markup($text, 'plain_text'));
}

function dcart_field_getters() {
  $getters = &drupal_static(__FUNCTION__);
  if(!isset($getters)) {
    $getters = module_invoke_all('dcart_field_getter_info');
    drupal_alter('dcart_field_getter_info', $getters);
  }
  return $getters;
}

function dcart_field_setters() {
  $setters = &drupal_static(__FUNCTION__);
  if(!isset($setters)) {
    $setters = module_invoke_all('dcart_field_setter_info');
    drupal_alter('dcart_field_setter_info', $setters);
  }
  return $setters;
}

function dcart_field_getter_callback($getter, $field_name, $field_info) {
  
  $field_type = !empty($field_info['type']) ? $field_info['type'] : '';
  
  $callback = dcart_hook_callback($getter, 'name_' . $field_name);
  if($callback === FALSE) {
    $callback = dcart_hook_callback($getter, 'type_' . $field_type);
  }
  if($callback === FALSE) {
    $callback = dcart_hook_callback($getter);
  }
  return $callback;
}


function dcart_field_setter_callback($setter, $field_name, $field_info) {

  $field_type = !empty($field_info['type']) ? $field_info['type'] : '';
  $callback = dcart_hook_callback($setter, 'name_' . $field_name);

  if($callback === FALSE) {
    $callback = dcart_hook_callback($setter, 'type_' . $field_type);
  }
  if($callback === FALSE) {
    $callback = dcart_hook_callback($setter);
  }
  return $callback;
}
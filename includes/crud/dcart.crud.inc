<?php

function dcart_crud_update_node($node, $values) {
  if(is_numeric($node)) {
    $node = node_load($node);
  }

  module_load_include('inc', 'dcart', 'includes/crud/dcart.field');
  $setter_info = dcart_field_setters();
  foreach($values as $field_name => $field_values) {
    if(isset($node->{$field_name})) {
      $field_info = field_info_field($field_name);
      $callback = dcart_field_setter_callback($setter_info, $field_name, $field_info);
      if($callback !== FALSE) {
        foreach($field_values as $langcode => $field_value) {
          $callback($field_name, $field_value, $field_info, $node, $langcode);
        }
      }
    }
  }
  node_save($node);
}
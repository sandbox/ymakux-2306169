<?php


function dcart_field_setter($field_name, $field_value, $field_info, $entity, $language = LANGUAGE_NONE) {

  $field_type = isset($field_info['type']) ? $field_info['type'] : FALSE;

  $allowed_types = array(
    'list_integer',
    'list_float',
    'list_text',
    'number_integer',
    'number_decimal',
    'number_float',
    'text_with_summary',
  );

  if(in_array($field_type, $allowed_types)) {
    
    if($field_info['module'] == 'list') {
      $instance = field_info_instance('node', $field_name, $entity->type);
      $allowed_values = list_allowed_values($field_info, $instance, 'node', $entity);
    }    

    $values = array();
    foreach ((array) $field_value as $delta => $value) {
      if(isset($allowed_values) && !isset($allowed_values[$value])) {
        continue;
      }
      $values[$language][$delta]['value'] = trim($value); 
    }
    
    if($values) {
      $entity->{$field_name} = $values;
    }
  }
}

function dcart_field_setter_type_taxonomy_term_reference($field_name, $field_value, $field_info, $entity, $language = LANGUAGE_NONE) {

  $allowed_values = taxonomy_allowed_values($field_info);
  
  $values = array();
  foreach ((array) $field_value as $delta => $value) {
    if(isset($allowed[$value])) {
      $values[$language][$delta]['tid'] = intval($value);
    }
  }
  
  if($values) {
    $entity->{$field_name} = $values;
  }
}


function dcart_field_setter_type_dcart_attributes($field_name, $field_value, $field_info, $entity, $language = NULL) {
  foreach ((array) $field_value as $delta => $value) {
    $entity->{$field_name}[LANGUAGE_NONE][$delta] = (array) $value;
  }
}

function dcart_field_setter_name_body($field_name, $field_value, $field_info, $entity, $language = LANGUAGE_NONE) {
  foreach ((array) $field_value as $delta => $value) {
    $entity->{$field_name}[$language][$delta] = array(
      'value' => $field_value,
      'summary' => text_summary($field_value),
      'format' => 'plain_text',
    );
  }
}

function dcart_field_setter_name_title($field_name, $field_value, $field_info, $entity, $language = LANGUAGE_NONE) {
  $entity->{$field_name} = truncate_utf8($field_value, 255, TRUE, FALSE, 1);
}

function dcart_field_setter_name_status($field_name, $field_value, $field_info, $entity, $language = LANGUAGE_NONE) {
  $entity->{$field_name} = filter_var($field_value, FILTER_VALIDATE_BOOLEAN);
}

function dcart_field_setter_name_title_field($field_name, $field_value, $field_info, $entity, $language = LANGUAGE_NONE) {
  $entity->title = truncate_utf8($field_value, 255, TRUE, FALSE, 1);
}

function dcart_field_setter_type_image($field_name, $field_value, $field_info, $entity, $language = LANGUAGE_NONE) {
  foreach ((array) $field_value as $delta => $fid) {
    if (is_integer($fid) && ($file = file_load($fid))) {
      $entity->{$field_name}[$language][$delta] = (array) $file;
    }
  }
}

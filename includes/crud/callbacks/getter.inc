<?php

function dcart_field_getter($entity, $field_name, $field_info, $language = LANGUAGE_NONE) {
  $return = FALSE;
  if (!empty($entity->{$field_name}[$language])) {
 
    $values = $entity->{$field_name}[$language];
    $field_type = !empty($field_info['info']['type']) ? $field_info['info']['type'] : NULL;

    switch ($field_type) {
      case 'image' :
      case 'file' :
        $key = 'fid';
        $function = 'intval';
      break;
      case 'taxonomy_term_reference' :
        $key = 'tid';
        $function = 'intval';
      break;
      case 'list_integer' :
      case 'number_integer' :
        $key = 'value';
        $function = 'intval';
      break;
      case 'list_float' :
      case 'number_decimal' :
      case 'number_float' :
        $key = 'value';
        $function = 'floatval';
      break;
      case 'list_boolean' :
        $key = 'value';
        $function = 'boolval';
      break;
    }
    
    if(isset($key) && isset($function)) {
      $return = array();
      for ($i = 0; $i < count($values); $i++) {
        $return[] = $function($values[$i][$key]);
      }
    }
  }
  return $return;
}
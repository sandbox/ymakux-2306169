<?php

function dcart_dcart_price_components_info() {
  $path = drupal_get_path('module', 'dcart') . '/includes/price/price_components';
  return array(
    'discount_role' => array(
      'title' => t('Role discount'),
      'display_title' => t('Discount'),
      'description' => t('Sitewide discount depending on <a href="@url">user role</a>', array('@url' => url('admin/people/permissions/roles'))),
      'callback' => 'dcart_price_component_discount_role',
      'file' => $path . '/discount_role.inc',
    ),
  );
}

function dcart_price_components() {
  $price_components = &drupal_static(__FUNCTION__);
  if (!isset($price_components)) {
    $price_components = module_invoke_all('dcart_price_components_info');
    $settings = dcart_price_component_settings();
    $weight = 0;
    foreach($price_components as $name => &$info) {
      if(empty($info['name'])) {
        $info['name'] = $name;
      }
      if(!isset($info['weight'])) {
        $info['weight'] = $weight;
      }
      if(!isset($info['active'])) {
        $info['active'] = TRUE;
      }
      if(!empty($settings[$name])) {
        $price_components[$name] = $settings[$name] + $price_components[$name];
      }
      $weight += 10;
    }
    drupal_alter('dcart_price_components', $price_components);
    uasort($price_components, 'drupal_sort_weight');
  }
  return $price_components;
}

function dcart_price_components_get($component_name = NULL, $active = NULL) {
  $price_components = dcart_price_components();
  if(isset($component_name)) {
    return isset($price_components[$component_name]) ? $price_components[$component_name] : array();
  }
  if(isset($active)) {
    foreach($price_components as $name => $info) {
      if($active === TRUE && empty($info['active'])) {
        unset($price_components[$name]);
      }
      if($active === FALSE && !empty($info['active'])) {
        unset($price_components[$name]);
      }
    }
  }
  return $price_components;
}

function dcart_price_component_load($component_name) {
  return dcart_price_components_get($component_name);
}


function dcart_price_component_access($price_component) {
  return user_access('dcart edit price component ' . $price_component['name']);
}


function dcart_price_component_settings($component_name = NULL) {
  $return = array();
  $query = db_select('dcart_price_component_settings', 's')->fields('s');
  if(isset($component_name)) {
    $query->condition('name', $component_name);
  }
  if($results = $query->execute()->fetchAll()) {
    foreach($results as $data) {
      $return[$data->name] = (array) $data;
      $return[$data->name]['settings'] = unserialize($return[$data->name]['settings']);
    }
    if(isset($component_name)) {
      $return = reset($return);
    }
  }
  return $return;
}


function dcart_price_component_save($component) {
  $component += dcart_price_component_default();
  foreach (module_implements('dcart_price_component_presave') as $module) {
    $function = $module . '_dcart_price_component_presave';
    $function($component);
  }
  if(is_array($component['settings'])) {
    $component['settings'] = serialize($component['settings']);
  }
  db_merge('dcart_price_component_settings')->key(array('name' => $component['name']))->fields($component)->execute();
}

function dcart_price_component_default() {
  return array(
    'name' => NULL,
    'active' => 1,
    'weight' => 0,
    'settings' => array(),
  );
}


function dcart_price_decimal_to_amount($decimal, $currency_code = NULL, $round = FALSE) {
  static $factors;
  if (empty($factors[$currency_code])) {
    $currency = dcart_currency_load($currency_code);
    $factors[$currency_code] = pow(10, $currency['decimals']);
  }

  if ($round) {
    $decimal = dcart_price_amount_round($decimal, dcart_currency_load($currency_code));
    return (int) round($decimal * $factors[$currency_code]);
  }
  else {
    return $decimal * $factors[$currency_code];
  }
}



function dcart_price_amount_to_decimal($amount, $currency_code = NULL) {
  static $divisors;
  if (empty($divisors[$currency_code])) {
    $currency = dcart_currency_load($currency_code);
    $divisors[$currency_code] = pow(10, $currency['decimals']);
  }
  return $amount / $divisors[$currency_code];
}

function dcart_price_format_amount($amount, $currency = NULL, $convert = TRUE) {
  
  if (is_null($currency) || is_string($currency)) {
    $currency = dcart_currency_load($currency);
  }

  if ($convert == TRUE) {
    $amount = dcart_price_amount_to_decimal($amount, $currency['code']);
  }

  if (!empty($currency['format_callback']) && function_exists($currency['format_callback'])) {
    return $currency['format_callback']($amount, $currency);
  }

  return number_format(dcart_price_amount_round(abs($amount), $currency), $currency['decimals'], $currency['decimal_separator'], $currency['thousands_separator']);
}

function dcart_price_amount_round($amount, $currency) {
  if (!$currency['rounding_step']) {
    return round($amount, $currency['decimals']);
  }
  $modifier = 1 / $currency['rounding_step'];
  return round($amount * $modifier) / $modifier;
}

<?php

function dcart_price_component_form($form, $form_state) {
  $form['#tree'] = TRUE;
  $settings = dcart_price_component_settings();
  $form_state['price_components'] = $price_components = dcart_price_components();
  uasort($price_components, 'drupal_sort_weight');
  foreach ($price_components as $name => $info) {
    $access = dcart_price_component_access($info);
    $form['price_components'][$name]['title'] = array(
      '#markup' => $info['title'],
    );
    $form['price_components'][$name]['description'] = array(
      '#markup' => !empty($info['description']) ? '<div class="description">' . filter_xss_admin($info['description']) . '</div>' : '',
    );
    $summary = '--';
    if($summary_callback = dcart_hook_callback($info, 'summary')) {
      $summary = $summary_callback($info);
    }
    $form['price_components'][$name]['summary'] = array(
      '#markup' => $summary,
    );
    $form['price_components'][$name]['active'] = array(
      '#type' => 'checkbox',
      '#default_value' => isset($settings['active']) ? $settings['active'] : $info['active'],
      '#disabled' => !$access,
    );
    $form['price_components'][$name]['operations']['settings'] = array(
      '#type' => 'link',
      '#title' => t('Settings'),
      '#href' => 'admin/dcart/pricing/component/' . $name,
      '#options' => array('query' => array('destination' => current_path())),
      '#access' => $access,
    );  
    $form['price_components'][$name]['weight'] = array(
      '#type' => 'weight',
      '#delta' => 10,
      '#attributes' => array('class' => array('price-component-weight')),
      '#default_value' => $price_components[$name]['weight'],
    );
  }
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
    '#access' => $access,
  );
  return $form;
}

function dcart_price_component_form_submit($form, $form_state) {
  foreach($form_state['values']['price_components'] as $component_name => $params) {
    $save = (array) db_select('dcart_price_component_settings', 's')
      ->fields('s')
      ->condition('name', $component_name)
      ->execute()
      ->fetchObject();
    dcart_price_component_save(array_replace($save, $params));
  }
}

function dcart_price_component_settings_form($form, &$form_settings, $price_component) {

  $form['active'] = array(
    '#title' => t('Active'),
    '#type' => 'checkbox',
    '#default_value' => !empty($price_component['active']),
  );
  
  $form['settings'] = array(
    '#type' => 'container',
    '#weight' => 99,
    '#tree' => TRUE,
  );

  if($settings_form_callback = dcart_hook_callback($price_component, 'settings_form')) {
    if($settings_elements = $settings_form_callback($form, $form_settings, $price_component)) {
      $form['settings'] += $settings_elements;
    }
  }
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
    '#weight' => 100,
  );
  return $form;
}

function dcart_price_component_settings_form_validate($form, &$form_state) {
  $price_component = $form_state['build_info']['args'][0];
  form_state_values_clean($form_state);
  if($validate_callback = dcart_hook_callback($price_component, 'settings_form_validate')) {
    $validate_callback($form, $form_state);
  }
}

function dcart_price_component_settings_form_submit($form, &$form_state) {
  $price_component = $form_state['build_info']['args'][0];
  if($submit_callback = dcart_hook_callback($price_component, 'settings_form_submit')) {
    $submit_callback($form, $form_state);
  }
  $params = array(
    'name' => $price_component['name'],
    'active' => $form_state['values']['active'],
    'weight' => $price_component['weight'],
    'settings' => isset($form_state['values']['settings']) ? $form_state['values']['settings'] : $price_component['settings'],
  );
  dcart_price_component_save($params);
}

function theme_dcart_price_component_form($variables) {

  $form = $variables['form'];
  $header = array(t('Price component'), t('Summary'), t('Active'), t('Weight'), t('Operations'));

  $rows = array();
  foreach(element_children($form['price_components']) as $element) {
    $rows[] = array(
      'data' => array(
        render($form['price_components'][$element]['title']) . render($form['price_components'][$element]['description']),
        filter_xss_admin(render($form['price_components'][$element]['summary'])),
        render($form['price_components'][$element]['active']),
        render($form['price_components'][$element]['weight']),
        render($form['price_components'][$element]['operations']),
      ),
      'class' => array('draggable'),
    );
  }
  
  $table_id = 'price-components';
  drupal_add_tabledrag($table_id, 'order', 'sibling', 'price-component-weight');
  
  $output = theme('table', array(
    'rows' => $rows,
    'header' => $header,
    'empty' => t('No price components.'),
    'attributes' => array('id' => $table_id),
  ));

  return $output . drupal_render_children($form);
}


function theme_dcart_price($variables) {

  if($variables['convert_currency'] && ($variables['convert_currency'] != $variables['currency_code'])) {
    $variables['amount'] = dcart_currency_convert($variables['amount'], $variables['currency_code'], $variables['convert_currency']);
    $variables['currency_code'] = $variables['convert_currency'];
  }
  
  $currency = dcart_currency_load($variables['currency_code']);

  $parts = array(
    !empty($variables['negative']) ? '-' : '',
    $currency['symbol_placement'] == 'before' ? $currency['symbol'] : '', 
    dcart_price_format_amount($variables['amount'], $currency, $variables['convert']),
    $currency['symbol_spacer'],
    $currency['symbol_placement'] == 'after' ? $currency['symbol'] : '',
  );
  return '<span class="dcart-price">' . implode('', $parts) . '</span>';
}

function theme_dcart_price_components($variables) {
  $price = '';
  $item = $variables['item'];
  if($variables['price_components'] == 1) {
    if(isset($item['data']['price_components']['_original_amount'])) {
      $original_amount = $item['data']['price_components']['_original_amount'];
      if($original_amount != $item['amount']) {
        $price .= theme('dcart_price_original', array('amount' => $original_amount, 'currency_code' => $item['currency_code']));
      }
    }
  }
  elseif($variables['price_components'] == 2) {
    
  }

  $price .= theme('dcart_price', array('amount' => $item['amount'], 'currency_code' => $item['currency_code']));
  return $price;
}

function theme_dcart_price_original($variables) {
  return '<span class="original">' . theme('dcart_price', $variables) . '</span>';
}

function theme_dcart_price_range($variables) {
  $currency_code = NULL;
  if(isset($variables['values'])) {
    $values = $variables['values'];
    if(count($values) == 1) {
      return theme('dcart_price', array(
        'amount' => reset($values),
        'currency_code' => $currency_code,
      ));
    }
  }
  else {
    $values = array();
    foreach($variables['items'] as $item) {
      $values[] = $item['amount'];
    }
    $currency_code = $variables['items'][0]['currency_code'];
  }
  $min = min($values);
  $max = max($values);
  if($min != $max) {
    $output = theme('dcart_price', array('amount' => $min, 'currency_code' => $currency_code));
    $output .= ' - ';
    $output .= theme('dcart_price', array('amount' => $max, 'currency_code' => $currency_code));
  }
  else {
    $output = theme('dcart_price_components', array('item' => $variables['items'][0]));
  }
  return '<div class="price-range">' . $output . '</div>';
}


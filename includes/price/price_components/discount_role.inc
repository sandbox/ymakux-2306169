<?php

function dcart_price_component_discount_role_set(&$item, $node, $info) {
  global $user;
  foreach($user->roles as $rid => $name) {
    if(isset($info['settings']['roles'][$rid]) 
    && !empty($info['settings']['roles'][$rid]['active']) 
    && !empty($info['settings']['roles'][$rid]['amount'])) {
      $type = $info['settings']['roles'][$rid]['type'];
      $amount = $info['settings']['roles'][$rid]['amount'];
      $currency_code = $info['settings']['roles'][$rid]['currency_code'];
      if($type == 'fixed') {
        if($currency_code != $item['currency_code']) {
          $discount = dcart_currency_convert($amount, $currency_code, $item['currency_code']);
          if($item['amount'] >= $discount) {
            $item['amount'] -= $discount;
          }
        }
      }
      elseif($type == 'percent' && $amount <= 100) {
        $item['amount'] -= (($amount / 100) * $item['amount']);
      }
      break;
    }
  }
}

function dcart_price_component_discount_role_settings_form($form, $form_state, $price_component) {

  $elements = array();
  $form_state['price_component'] = $price_component;

  $elements['roles'] = array('#type' => 'vertical_tabs');
  foreach(user_roles() as $rid => $name) {
    $elements['roles'][$rid] = array(
      '#title' => check_plain($name),
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#group' => 'roles',
    );
    $elements['roles'][$rid]['active'] = array(
      '#title' => t('Active'),
      '#type' => 'checkbox',
      '#default_value' => !empty($price_component['settings']['roles'][$rid]['active']),
    );
    $elements['roles'][$rid]['type'] = array(
      '#title' => t('Type'),
      '#type' => 'select',
      '#options' => array('fixed' => t('Fixed amount'), 'percent' => t('Percent')),
      '#default_value' => isset($price_component['settings']['roles'][$rid]['type']) ? $price_component['settings']['roles'][$rid]['type'] : 'percent',
    );
 
    $currency_code = isset($price_component['settings']['roles'][$rid]['currency_code']) ? $price_component['settings']['roles'][$rid]['currency_code'] : dcart_default_currency();

    $amount = '';
    if(isset($price_component['settings']['roles'][$rid]['type'])) {
      if($price_component['settings']['roles'][$rid]['type'] == 'fixed') {
        $amount = dcart_price_amount_to_decimal($price_component['settings']['roles'][$rid]['amount'], $currency_code);
      }
      else {
        $amount = $price_component['settings']['roles'][$rid]['amount'];
      }
    }
    $elements['roles'][$rid]['amount'] = array(
      '#title' => t('Amount'),
      '#type' => 'textfield',
      '#size' => 4,
      '#default_value' => $amount,
      '#element_validate' => array('element_validate_number'),
    );
    $elements['roles'][$rid]['currency_code'] = array(
      '#title' => t('Currency code'),
      '#type' => 'select',
      '#options' => dcart_currency_options(TRUE),
      '#default_value' => $currency_code,
      '#states' => array(
        'visible' => array(
          ':input[name="settings[roles][' . $rid . '][type]"]' => array('value' => 'fixed'),
        ),
      ),
    );
  }
  return $elements;
}

function dcart_price_component_discount_role_settings_form_validate($form, &$form_state) {
  unset($form_state['values']['settings']['roles']['settings__roles__active_tab']);
  if(!empty($form_state['values']['settings']['roles'])) {
    foreach($form_state['values']['settings']['roles'] as $rid => &$data) {
      if(empty($data['amount'])) {
        unset($form_state['values']['settings']['roles'][$rid]);
        continue;
      }
      $data['amount'] = abs($data['amount']);
      
      switch($data['type']) {
        case 'fixed':
          $data['amount'] = dcart_price_decimal_to_amount($data['amount'], $data['currency_code']);
          break;
        case 'percent':
          if($data['amount'] > 100) {
            form_error($form['settings']['roles'][$rid]['amount'], t('Percent value can not be greater than 100'));
          }
          break;
      }
    }
  }
}

function dcart_price_component_discount_role_summary($price_component) {
  $items = array();
  $roles = user_roles();
  if(!empty($price_component['settings']['roles'])) {
    foreach($price_component['settings']['roles'] as $rid => $data) {
      if($data['active'] && isset($roles[$rid])) {
        switch($data['type']) {
          case 'fixed':
            $discount = theme('dcart_price', array('amount' => $data['amount'], 'currency_code' => $data['currency_code']));
            break;
          case 'percent':
            $discount = $data['amount'] . '%';
            break;
        }
        $items[] = t('@role_name - !discount', array('@role_name' => $roles[$rid], '!discount' => $discount));
      }
    }
  }
  return theme('item_list', array('items' => $items));
}


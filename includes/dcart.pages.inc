<?php

function dcart_admin_dashboard() {
  $links = menu_tree_all_data('dcart');
  $columns = variable_get('dcart_dashboard_columns', 2);
  return theme('dcart_dashboard', array('links' => $links, 'columns' => $columns));
}

function dcart_settings_overview() {
  $links = array();
  foreach (menu_tree_all_data('dcart') as $link) {
    if ($link['link']['link_path'] == 'admin/dcart/settings') {
      $links = $link['below'];
      break;
    }
  }
  $columns = variable_get('dcart_dashboard_columns', 2);
  return theme('dcart_dashboard', array('links' => $links, 'columns' => $columns, 'exclude_hidden' => FALSE));
}

function dcart_catalog() {
  return theme('dcart_catalog', array());
}

function dcart_operations() {
  $args = func_get_args();
  
  if (empty($args)) {
    return MENU_ACCESS_DENIED;
  }

  switch ($args[0]) {
    case 'autocomplete':
    
      if (!isset($_GET['term'])) {
        return MENU_ACCESS_DENIED;
      }

      if (isset($args[1])) {
          $output = array();
        if ($args[1] == 'product-by-sku' || $args[1] == 'product-by-title') {
          if ($product_types = dcart_product_node_types()) {
            $query = new EntityFieldQuery();
            $query->entityCondition('entity_type', 'node');
            $query->entityCondition('bundle', $product_types, 'IN');
            
            if ($args[1] == 'product-by-sku') {
              $query->fieldCondition(DCART_ATTRIBUTES_FIELD_NAME, 'sku', '%' . trim(rawurldecode($_GET['term'])) . '%', 'LIKE');
            }
            else {
              $query->propertyCondition('title', '%' . trim(rawurldecode($_GET['term'])) . '%', 'LIKE');
            }
            
            $query->range(0, 10);
            $result = $query->execute();
          }
        }

        if ($args[1] == 'product-by-sku') {
          if (!empty($result['node'])) {
            foreach ($result['node'] as $nodedata) {
              if ($node = node_load($nodedata->nid)) {
                if ($items = field_get_items('node', $node, DCART_ATTRIBUTES_FIELD_NAME)) {
                  $skus = array();
                  foreach ($items as $delta => $item) {
                    if (substr($item['sku'], 0, strlen($_GET['term'])) == $_GET['term']) {
                      $output[] = array(
                        'sku' => check_plain($item['sku']),
                        'title' => check_plain($node->title),
                        'nid' => $node->nid,
                      );
                    }
                  }
                }
              }
            }
          }
          return drupal_json_output($output);
        }

        if ($args[1] == 'product-by-title') { 
          if (!empty($result['node'])) {
            foreach ($result['node'] as $nodedata) {
              if ($node = node_load($nodedata->nid)) {
                if ($items = field_get_items('node', $node, DCART_ATTRIBUTES_FIELD_NAME)) {
                  foreach ($items as $delta => $item) {
                    $output[] = array(
                      'sku' => check_plain($item['sku']),
                      'title' => check_plain($node->title),
                      'nid' => $nodedata->nid,
                    );
                  }
                }
              }
            }
          }
          return drupal_json_output($output);
        }
      }
    break;
    case 'bulk' :
      if (isset($args[1]) && !empty($_SESSION['dcart_bulk_operations']['action'])) {
        $action = $_SESSION['dcart_bulk_operations']['action'];
        if (user_access('dcart perform bulk operation ' . $action)) {
          return drupal_get_form('dcart_bulk_operations_form');
        }
      }
    break;
    
    case 'cart' :
      if (isset($args[1]) && isset($args[2])) {
        $id = (int) $args[2];
        $cart_item = dcart_cart_load($id);
        switch($args[1]) {
          case 'remove':
            if(dcart_cart_access($cart_item, 'edit')) {
              dcart_cart_delete($id);
              return drupal_goto($_GET['destination']);
            }
            break;
          case 'to-wishlist':
            if(dcart_cart_access($cart_item, 'add_to_wishlist')) {
              dcart_cart_move_to_wishlist($cart_item);
              return drupal_goto($_GET['destination']);
            }
            break;
          case 'to-cart':
            if(user_access('dcart add to cart')) {
              dcart_cart_move_from_wishlist($cart_item);
              return drupal_goto($_GET['destination']);
            }
            break;
        }
      }
    break;
    case 'token-list' :
      if (user_access('administer dcart') && !empty($_GET['types'])) {
        print theme('dcart_token_list', array('types' => $_GET['types']));
        return NULL;
      }
    break;
  }
  return MENU_ACCESS_DENIED;
}


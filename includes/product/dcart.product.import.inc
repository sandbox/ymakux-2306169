<?php

function dcart_product_import_form($form, $form_state) {
  $form['file'] = array(
    '#type' => 'file',
    '#title' => t('Choose a file'),
  );
  
  $form['submit'] = array(
    '#type' => 'submit', 
    '#value' => t('Upload'),
  );
  return $form;
}

function dcart_product_import_form_validate($form, &$form_state) {
  if(empty($_FILES['files']['type']['file'])) {
    form_error($form['file'], '');
    return;
  }
  $file = file_save_upload('file', array('file_validate_extensions' => array('csv')));
  if(empty($file->uri)) {
    form_error($form['file'], '');
  }
  else {
    $form_state['uploaded_file'] = drupal_realpath($file->uri);
  }
}

function dcart_product_import_form_submit($form, &$form_state) {

  $batch = array(
		 'operations' => array(),
		 'finished' => 'dcart_product_import_batch_finished',
		 'title' => t('Importing data from CSV file'),
		 'init_message' => t('Starting import...'),
		 'progress_message' => t('Imported @percentage%.'),
		 'error_message' => t('An error occurred during the import.'),
		 'operations' => array(array('dcart_product_import_batch_chunk', array($form_state['uploaded_file']))),
     'file' => drupal_get_path('module', 'dcart') . '/includes/product/dcart.product.import.inc',
   );
   batch_set($batch);
}

function dcart_product_import_batch_chunk($filepath, &$context) {

  module_load_include('inc', 'dcart', 'ReadCSV');

  if (empty($context['sandbox'])) {
    $context['sandbox']['file_offset'] = 0;
    $context['sandbox']['first_line'] = TRUE;
    $context['results'] = array('errors' => array(), 'count' => 0);
  };

  $first = &$context['sandbox']['first_line'];
  $results = & $context['results'];

  try {
    $csv_reader = new ReadCSV($filepath, DCART_CSV_DELIMETER, "\xEF\xBB\xBF", $context['sandbox']['file_offset']);
  }
  catch (Exception $e) {
    $results['errors'][] = check_plain($e->getMessage());
    return; // terminate batch processing
  }

  $end_time = time() + DCART_PRODUCT_IMPORT_CHUNK_MAX_TIME;
  while (($line = $csv_reader->get_row()) !== NULL) {
    if (empty($line) || (count($line) == 1 && $line[0] == NULL)) {
      continue;
    }

    if ($first) {
      $mapping = dcart_product_csv_mapping();
      $header = $mapping['header'];
      foreach ($mapping['keys'] as $fi => $k) {
	      $fn = $header[$fi];
	      $line_index = array_search($fn, $line);
	      if ($line_index === FALSE) {
	        $results['errors'][] = t('File does not contain required @name column heading', array('@name' => $fn));
	        return;
	      }
      }
      
      $first = FALSE;
      continue;
    }
    dcart_product_import_update($line, $context);
    if (time() > $end_time) {
      break;
    }
  }

  if ($line === NULL) {
    $context['finished'] = 1;
  }
  else {
    $o = $csv_reader->offset();
    $context['sandbox']['file_offset'] = $o;
    $context['finished'] = $o / $csv_reader->size();
  }
}

function dcart_product_import_batch_finished($success, $results, $operations) {
  if (!$success) {
    drupal_set_message(t('Finished with an error.'), 'error');
  }
  else {
    drupal_set_message(format_plural($results['count'], 'Imported 1 record', 'Imported @count records'));
  }
}

function dcart_product_import_update($line, &$context) {

  $update = FALSE;
  $line = str_replace(" ", "", $line);
  if(empty($line[1])) {
    $context['results']['errors'][] = t('SKU identifier is empty');
    return FALSE;
  }

  $sku = $line[1];
  $product = dcart_get_node_by_sku($sku, TRUE);
  if(empty($product->nid)) {
    $context['results']['errors'][] = t('Product %title (SKU %sku) - not found in database', array('%title' => $product->title, '%sku' => $sku));
    return FALSE;
  }

  if(empty($line[2])) {
    $context['results']['errors'][] = t('Product %title (SKU %sku) - amount is not numeric', array('%title' => $product->title, '%sku' => $sku));
    return FALSE;
  }

  if(empty($line[3])) {
    $context['results']['errors'][] = t('Product %title (SKU %sku) - wrong currency code', array('%title' => $product->title, '%sku' => $sku));
    return FALSE;
  }

  if(!dcart_currencies_get(FALSE, $line[3])) {
    $context['results']['errors'][] = t('Product %title (SKU %sku) - wrong currency code', array('%title' => $product->title, '%sku' => $sku));
    return FALSE;
  }

  $amount = $line[2];
  $currency_code = $line[3]; 
  if(!is_numeric($amount)) {
    $context['results']['errors'][] = t('Product %title (SKU %sku) - amount is not numeric', array('%title' => $product->title, '%sku' => $sku));
    return FALSE;
  }

  if(isset($line[4]) && ($line[4] == 0 || $line[4] == 1)) {
    $product->status = $line[4];
    $update = TRUE;
  }

  $minor_units = dcart_price_decimal_to_amount($amount, $currency_code);
  $items = dcart_attribute_field($product);

  if($minor_units && $items) {
    foreach($items as $delta => $item) {
      if($item['sku'] == $sku) {
        $product->{DCART_ATTRIBUTES_FIELD_NAME}[LANGUAGE_NONE][$delta]['amount'] = $minor_units;
        $product->{DCART_ATTRIBUTES_FIELD_NAME}[LANGUAGE_NONE][$delta]['currency_code'] = $currency_code;
        if(isset($line[5])) {
          dcart_stock_set($sku, $line[5]);
        }
        $update = TRUE;
        break;
      }
    }
  }

  if($update) {
    node_save($product);
    return $product->nid;
  }

  return FALSE;
}

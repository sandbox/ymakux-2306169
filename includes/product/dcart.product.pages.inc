<?php

function dcart_product_add_page($product_type = NULL){
  if (!dcart_product_node_types()) {
    drupal_set_message(t('In order to use Drupal Cart you must enable at least one product type. Choose a content type, click "edit", select "Is product" checkbox under "Drupal cart" tab'), 'warning');
    return drupal_goto('admin/structure/types');
  }
  
  if($product_type && user_access('create ' . $product_type . ' content')) {
    return drupal_goto('node/add/' . str_replace('_', '-', $product_type));
  }

  drupal_set_title(t('Add product'));
  return dcart_product_add_links();
}

function dcart_product_add_links() {
  $links = system_admin_menu_block(array(
    'path'=>'node/add', 'tab_root' => 'node/add'));
  
  $content = array();
  foreach($links as $link){
    $args = unserialize($link['access_arguments']);
    $type = $args[1];
    
    if(dcart_is_product_node_type($type)){
      $content[] = $link;
    }
  }
  return theme('node_add_list', array('content' => $content));
}

function dcart_product_overview($product_type = NULL) {

  if (!dcart_product_node_types()) {
    drupal_set_message(t('In order to use Drupal Cart you must enable at least one product type. Choose a content type, click "edit", select "Is product" checkbox under "Drupal cart" tab'), 'warning');
    return drupal_goto('admin/structure/types');
  }

  return drupal_get_form('dcart_product_overview_form', $product_type);
}

function dcart_product_overview_form($form, &$form_state, $product_type) {

  $form['#tree'] = TRUE;
  $parameters = drupal_get_query_parameters();
  $product_types = isset($product_type) ? (array) $product_type : dcart_product_node_types();

  $node_type_names = node_type_get_names();
  
  $form['search'] = array(
    '#type' => 'fieldset',
    '#theme_wrappers' => array('dcart_product_search_form'),
    '#weight' => -99,
    '#attached' => array(
      'library' => array(
        array('system', 'ui.autocomplete'),
      ),
    ),
  );
  
  $form['search']['title'] = array(
    '#type' => 'textfield',
    '#default_value' => isset($parameters['search']['title']) ? rawurldecode($parameters['search']['title']) : '',
    '#size' => 20,
    '#attributes' => array('id' => 'product-title-autocomplete'),
  );
  
  $form['search']['sku'] = array(
    '#type' => 'textfield',
    '#default_value' => isset($parameters['search']['sku']) ? $parameters['search']['sku'] : '',
    '#size' => 10,
    '#attributes' => array('id' => 'product-sku-autocomplete'),
  );
  
  $form['search']['price_min'] = array(
    '#type' => 'textfield',
    '#default_value' => isset($parameters['search']['price_min']) ? dcart_price_amount_to_decimal($parameters['search']['price_min']) : '',
    '#size' => 3,
  );
  
  $form['search']['price_max'] = array(
    '#type' => 'textfield',
    '#default_value' => isset($parameters['search']['price_max']) ? dcart_price_amount_to_decimal($parameters['search']['price_max']) : '',
    '#size' => 3,
  );
  

  $form['search']['uid'] = array(
    '#type' => 'textfield',
    '#default_value' => '',
    '#autocomplete_path' => 'user/autocomplete',
    '#default_value' => (isset($parameters['search']['uid']) && ($user = user_load($parameters['search']['uid']))) ? $user->name : '',
    '#size' => 10,
  );
  
  if(!user_access('dcart access all products overview')) {
    $form['search']['uid'] = array('#markup' => $GLOBALS['user']->name);
  }

  $form['search']['status'] = array(
    '#type' => 'checkbox',
    '#default_value' => isset($parameters['search']['status']) ? $parameters['search']['status'] : 1,
  );
  
  $form['search']['search'] = array(
    '#type' => 'submit',
    '#value' => t('Search'),
    '#parents' => array(),
    '#submit' => array('dcart_product_search_submit'),
    '#validate' => array('dcart_product_search_validate'),
    '#limit_validation_errors' => array(array('search')),
  );
  
  $form['search']['reset'] = array(
    '#type' => 'submit',
    '#value' => t('Reset'),
    '#submit' => array('dcart_product_search_reset_submit'),
    '#parents' => array(),
    '#access' => !empty($parameters['search']),
    '#limit_validation_errors' => array(array('search')),
  );
  
  $selected_products = !empty($_SESSION['dcart_bulk_operation_selected']['products']) ? $_SESSION['dcart_bulk_operation_selected']['products'] : array();

  $form['reset_selected'] = array(
    '#prefix' => format_plural(count($selected_products), 'Selected 1 product', 'Selected @count products'),
    '#type' => 'submit',
    '#value' => t('Reset'),
    '#submit' => array('dcart_product_reset_selected_submit'),
    '#access' => (bool) $selected_products,
    '#limit_validation_errors' => array(),
    '#weight' => -90,
  );

  $header = array(

    'title' => array(
      'data' => t('Title'),
      'specifier' => 'title',
      'type' => 'property',
    ),

    'sku' => array(
      'data' => t('Base SKU'),
      'specifier' => array('field' => DCART_ATTRIBUTES_FIELD_NAME, 'column' => 'sku'),
      'type' => 'field',
    ),
    
    'price' => array(
      'data' => t('Base price'),
      'specifier' => array('field' => DCART_ATTRIBUTES_FIELD_NAME, 'column' => 'amount'),
      'type' => 'field',
    ),

    'type' => array(
      'data' => t('Type'),
      'specifier' => 'type',
      'type' => 'property',
    ),

    'uid' => array(
      'data' => t('Author'),
      'specifier' => 'uid',
      'type' => 'property',
    ),

    'status' => array(
      'data' => t('Status'),
      'specifier' => 'status',
      'type' => 'property',
    ),
    'created' => array(
      'data' => t('Created'),
      'specifier' => 'created',
      'type' => 'property',
      'sort' => 'desc',
    ),
    'changed' => array(
      'data' => t('Updated'),
      'specifier' => 'changed',
      'type' => 'property',
    ),
    'operations' => t('Operations'),
  );

  $nodes = $options = array();
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node');
  $query->entityCondition('bundle', $product_types, 'IN');

  if (isset($parameters['search']['status'])){
    $query->propertyCondition('status', $parameters['search']['status']);
  }

  if (isset($parameters['search']['uid'])){
    $query->propertyCondition('uid', '%' . trim(rawurldecode($parameters['search']['uid'])) . '%', 'LIKE');
  }

  if (isset($parameters['search']['title'])) {
    $query->propertyCondition('title', "%" . trim(rawurldecode($parameters['search']['title'])) . "%", "LIKE");
  }
  
  if (isset($parameters['search']['sku'])) {
    $query->fieldCondition(DCART_ATTRIBUTES_FIELD_NAME, 'sku', '%' . $parameters['search']['sku'] . '%', 'LIKE');
  }

  if (isset($parameters['search']['price_min']) && isset($parameters['search']['price_max'])) {
    $range = array((int) $parameters['search']['price_min'], (int) $parameters['search']['price_max']);
    $query->fieldCondition(DCART_ATTRIBUTES_FIELD_NAME, 'amount', $range, 'BETWEEN', 0);
  }
  elseif (isset($parameters['search']['price_min']) && !isset($parameters['search']['price_max'])) {
    $query->fieldCondition(DCART_ATTRIBUTES_FIELD_NAME, 'amount', (int) $parameters['search']['price_min'], '>=', 0);
  }
  elseif (!isset($parameters['search']['price_min']) && isset($parameters['search']['price_max'])) {
    $query->fieldCondition(DCART_ATTRIBUTES_FIELD_NAME, 'amount', (int) $parameters['search']['price_max'], '<=', 0);
  }

  if(!user_access('dcart access all products overview')) {
    $query->propertyCondition('uid', $GLOBALS['user']->uid);
  }
  
  $query->tableSort($header);
  $query->pager(variable_get('dcart_admin_products_limit', 20));
  $result = $query->execute();

  if (!empty($result['node'])) {
    $nids = array_keys($result['node']);
    $nodes = entity_load('node', $nids);
  }
  
  if ($nodes) {

    foreach ($nodes as $node) {
    
      $author = '';
      if ($user = user_load($node->uid)) {
        $author = check_plain($user->name);
      }

      $links = array();
      
      if(node_access('view', $node)) {
        $links[] = array(
          'title' => t('View'),
          'href' => 'node/' . $node->nid . '/view',
          'query' => array('dialog' => TRUE),
          'attributes' => array('class' => array('node-view-dialog'), 'title' => check_plain($node->title)),
        );
      }
      
      if(node_access('update', $node)) {
        $links[] = array(
          'title' => t('Edit'),
          'href' => 'node/' . $node->nid . '/edit',
          'query' => array('destination' => 'admin/dcart/products'),
        );
        
        if(dcart_product_has_attribute_fields($node->type)) {
          $links[] = array(
            'title' => t('Attributes'),
            'href' => 'node/' . $node->nid . '/attributes',
            'query' => array('destination' => 'admin/dcart/products'),
          );
        }
      }
        
      if(node_access('delete', $node)) {
        $links[] = array(
          'title' => t('Delete'),
          'href' => 'node/' . $node->nid . '/delete',
          'query' => array('destination' => 'admin/dcart/products'),
        );
      }
      
      $main_attribute = dcart_product_attributes_get($node, NULL, 0);
      
      $options[$node->nid] = array(
        'title' => check_plain($node->title),
        'sku' => $main_attribute['sku'],
        'price' => theme('dcart_price', array('amount' => $main_attribute['amount'], 'currency_code' => $main_attribute['currency_code'])),
        'status' => $node->status ? t('published') : t('not published'),
        'type' => check_plain($node_type_names[$node->type]),
        'uid' => $author,
        'created' => format_date($node->created, 'custom', 'Y-m-d H:i:s'),
        'changed' => format_date($node->changed, 'custom', 'Y-m-d H:i:s'),
        'operations' => theme('links', array('links' => $links, 'attributes' => array('class' => array('links', 'inline')))),
      );
    }

    foreach(dcart_bulk_operations_options('product') as $operation_name => $operation_title) {
      if(user_access('dcart perform bulk operation ' . $operation_name)) {
        $form['action'][$operation_name] = array(
          '#type' => 'submit',
          '#value' => $operation_title,
          '#submit' => array('dcart_bulk_operations_submit'),
          '#validate' => array('dcart_product_bulk_operations_validate', 'dcart_bulk_operations_validate'),
        );
      }
    }
    
    if(!empty($form['action'])) {
      $form['action']['remember'] = array(
        '#type' => 'submit',
        '#value' => t('Remember selected'),
        '#submit' => array('dcart_product_remember_selected_submit'),
        '#limit_validation_errors' => array(array('items')),
        '#weight' => -99,
      );
    } 
  }
  
  $form['type'] = array(
    '#type' => 'value',
    '#value' => 'product',
  );
    
  $form['nodes'] = array(
    '#type' => 'value',
    '#value' => $nodes,
  );
  
  $form['items'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $options,
    '#empty' => t('No products to display'),
    '#suffix' => theme('pager'),
    '#weight' => -50,
    '#default_value' => $selected_products,
    '#disabled' => empty($form['action']),
    '#attached' => array(
      'library' => array(
        array('system', 'ui.dialog'),
      ),
    ),
  );

  $form['add_product'] = array(
    '#markup' => dcart_product_add_links(),
    '#access' => _node_add_access(),
    '#prefix' => '<div id="menu-add-product" style="display:none;">',
    '#suffix' => '</div>',
  );
  return $form;
}

function dcart_product_reset_selected_submit($form, $form_state) {
  unset($_SESSION['dcart_bulk_operation_selected']['products']);
}

function dcart_product_remember_selected_submit($form, $form_state) {
  if($items = array_filter($form_state['values']['items'])) {
    foreach($items as $id) {
      $_SESSION['dcart_bulk_operation_selected']['products'][$id] = $id;
    }
  }
}

function dcart_product_search_validate($form, &$form_state) {
  $form_state['values']['search'] = array_filter(array_map('trim', $form_state['values']['search']));

  if (isset($form_state['values']['search']['price_max'])) {
    if(is_numeric($form_state['values']['search']['price_max'])) {
      $form_state['values']['search']['price_max'] = dcart_price_decimal_to_amount($form_state['values']['search']['price_max']);
    }
    else {
      unset($form_state['values']['search']['price_max']);
    }
  }
  
  if (isset($form_state['values']['search']['price_min'])) {
    if(is_numeric($form_state['values']['search']['price_min'])) {
      $form_state['values']['search']['price_min'] = dcart_price_decimal_to_amount($form_state['values']['search']['price_min']);
    }
    else {
      unset($form_state['values']['search']['price_min']);
    }
  }
  
  if(isset($form_state['values']['search']['uid']) && $user = user_load_by_name($form_state['values']['search']['uid'])) {
    $form_state['values']['search']['uid'] = $user->uid;
  }
  
  $form_state['values']['search']['status'] = !empty($form_state['values']['search']['status']);
  
  if(isset($form_state['values']['search']['title'])) {
    $form_state['values']['search']['title'] = rawurlencode($form_state['values']['search']['title']);
  }
}

function dcart_product_search_submit($form, &$form_state) {
  $query = drupal_get_query_parameters();
  if (!empty($form_state['values']['search'])) {
    $query  = array_replace($query, array('search' => $form_state['values']['search']));
  }
  $_SESSION['dcart_product_search'] = $query;
  $form_state['redirect'] = array(current_path(), array('query' => array($query)));
}

function dcart_product_search_reset_submit($form, &$form_state) {
  unset($_SESSION['dcart_product_search']);
  $form_state['redirect'] = current_path();
}

function dcart_product_bulk_operations_validate($form, &$form_state) {
  $form_state['values']['action'] = $form_state['triggering_element']['#parents'][1];
  
  if(!empty($_SESSION['dcart_bulk_operation_selected']['products'])) {
    $form_state['values']['items'] = $_SESSION['dcart_bulk_operation_selected']['products'];
  }
}

function dcart_product_attributes_form($form, &$form_state, $node) {
  $form_state['node'] = $node;
  if(module_exists('entity_translation')) {
    // Prevent error on submit
    $form['name'] = array(
      '#type' => 'value',
      '#value' => $node->name,
    );
  }
  $options = array('field_name' => DCART_ATTRIBUTES_FIELD_NAME);
  field_attach_form('node', $node, $form, $form_state, NULL, $options);
  return $form;
}

function dcart_product_attribute_properties_form($form, &$form_state, $node, $sku) {
  
  $attribute = dcart_product_attributes_get($node, $sku);
  $context = compact(array('form_state', 'node', 'sku', 'attribute'));
  
  $form['#tree'] = TRUE;
  $form['tabs'] = array('#type' => 'vertical_tabs');
  
  $form_state['attribute_properties'] = dcart_attribute_properties();
  $form_state['node'] = $node;
  $form_state['sku'] = $sku;
  $form_state['attribute'] = $attribute;

  foreach ($form_state['attribute_properties'] as $property_name => $property_data) {
    if (!empty($property_data['node_types']) && !in_array($node->type, (array) $property_data['node_types'])) {
      continue;
    }

    if (!empty($property_data['file']) && file_exists($property_data['file'])) {
      include_once $property_data['file'];
      $form_state['build_info']['files'][$property_name] = $property_data['file'];  
    }

    $callback_form = $property_data['callback'] . '_form';

    if (function_exists($callback_form) && ($elements = $callback_form($context))) {

      $form['combination'] = array(
        '#markup' => theme('dcart_attribute_property_combination', array('attribute' => $attribute, 'node' => $node)),
      );
      
      $form['properties'][$property_name] = array(
        '#title' => $property_data['title'],
        '#type' => 'fieldset',
        '#access' => user_access('dcart edit attribute property ' . $property_name),
        '#group' => 'tabs',
        '#description' => !empty($property_data['description']) ? $property_data['description'] : '',
        '#weight' => (isset($_GET['tab']) && $_GET['tab'] == $property_name) ? -999 : 0,
      );

      foreach ($elements as $element_name => $element) {
        $form['properties'][$property_name][$element_name] = $element;
      }
      
      $form['properties'][$property_name]['actions'] = array('#type' => 'actions');
      $form['properties'][$property_name]['actions']['submit'] = array(
        '#type' => 'submit',
        '#name' => 'submit_' . $property_name,
        '#value' => t('Save'),
      );
      
      $callback_validate = $callback_form . '_validate';
      $callback_submit = $callback_form . '_submit';

      if (function_exists($callback_validate)) {
        $form['properties'][$property_name]['actions']['submit']['#validate'] = array($callback_validate);
      }
    
      if (function_exists($callback_submit)) {
        $form['properties'][$property_name]['actions']['submit']['#submit'] = array($callback_submit);
      }
    }
  }
  return $form;
}

function dcart_product_default_attribute_field_values($node) {
  $field_values = array();
  $attributes = dcart_build_attributes($node->type, $node);
  if(!empty($attributes['default'])) {
    foreach($attributes['default'] as $field) {
      $exploded = explode('::', $field);
      $field_values[$exploded[0]] = $exploded[1];
    }
  }
  return $field_values;
}

function theme_dcart_product_attributes($variables) {

  $output = '';
  $node = $variables['node'];
  $attribute_field_types = dcart_attributes_field_types();

  if(empty($variables['attribute']['data']['fields'])){
    $attribute_fields = dcart_product_default_attribute_field_values($node);
  }
  else {
    $attribute_fields = $variables['attribute']['data']['fields'];
  }
  
  $items = array();
  foreach ($attribute_fields as $field_name => $field_value) {
    if($field_info = field_info_field($field_name)) {
      $field_type = $field_info['type'];
      $instance = field_info_instance('node', $field_name, $node->type);
      if(!empty($attribute_field_types[$field_type])) {
        if($view_callback = dcart_hook_callback($attribute_field_types[$field_type], 'attribute_view')) {
          $items[] = $view_callback($field_name, $field_value, $field_info, $node, $instance);
        }
      }
    }
  }

  $output .= theme('item_list', array('items' => $items));
  return $output;
}

function theme_dcart_product_search_form($variables) {

  $element = $variables['element'];

  $rows[] = array(
    render($element['title']),
    render($element['sku']),
    array(
      'data' => '<span>' . t('from') . '</span>' . render($element['price_min']) . '<span>' . t('to') . '</span>' . render($element['price_max']),
      'class' => array('fields-inline'),
    ),
    render($element['uid']),
    render($element['status']),
    render($element['search']) . render($element['reset']),
  );

  $header = array(
    t('Title'),
    t('SKU'),
    t('Price, @currency_code', array('@currency_code' => dcart_default_currency())),
    t('Author'),
    t('Published'),
    '',
  );

  $output = theme('table', array('rows' => $rows, 'header' => $header));
  return $output;
}

function dcart_product_settings_form($form, $form_state) {

  $options = array();
  foreach(field_info_fields() as $field_name => $field_data) {
    if($field_data['type'] == 'image') {
      $options[$field_name] = check_plain($field_name);
    }
  }
  
  $form['dcart_product_image_field'] = array(
    '#type' => 'select',
    '#title' => t('Image field'),
    '#options' => $options,
    '#default_value' => variable_get('dcart_product_image_field', 'dcart_product_image'),
    '#description' => t('Specify an image field that represents product.'),
  );

  $form['dcart_product_attributes_short_form'] = array(
    '#type' => 'checkbox',
    '#title' => t('Shortened attributes form'),
    '#default_value' => variable_get('dcart_product_attributes_short_form', 1),
    '#description' => t('On add/edit product form show only active attributes. Saves page space. Full attributes form available under "Attributes" tab.'),
  );
  return system_settings_form($form);
}


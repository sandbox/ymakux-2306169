<?php

function dcart_attribute_property_image_field_get($attribute, $node = NULL) {
  return (array) db_select('dcart_attribute_property_image_field', 'i')
    ->fields('i')
    ->condition('sku', $attribute['sku'])
    ->execute()
    ->fetchObject();
}

function dcart_attribute_property_image_field_form($context) {

  extract($context);
  if($image_field = dcart_product_image_field_name($node->type)) {
    $form['field'] = array('#type' => 'value', '#value' => $image_field);
    if ($items = field_get_items('node', $node, $image_field)) {
      $options = array();
      foreach ($items as $item) {
        $options[$item['fid']] = $item['uri'];
      }
      $form['fid'] = array(
        '#type' => 'radios',
        '#title' => t('Select image'),   
        '#title_display' => 'invisible',        
        '#options' => $options,
        '#theme' => 'dcart_radios_image',
        '#default_value' => isset($attribute['data']['properties']['image_field']['fid']) ? $attribute['data']['properties']['image_field']['fid'] : NULL,      
      ); 
    }
    return $form;
  }
  return array();
}

function dcart_attribute_property_image_field_widget_form($item, $node) {

  $form = array();
  if (!empty($item['data']['properties']['image_field']['fid'])) {
    $fid = $item['data']['properties']['image_field']['fid'];
    if ($file = file_load($fid)) {
      $image_url = file_create_url($file->uri);
      $icon = l(t('View'), $image_url, array('attributes' => array('target' => '_blank')));
      if (variable_get('dcart_attribute_widget_image', 'image') == 'image') {    
        $icon = l(theme('image_style', array('path' => $file->uri, 'style_name' => 'dcart')), $image_url, array(
          'html' => TRUE,
          'attributes' => array('target' => '_blank')));
      }
    }
    
    $form['fid'] = array('#type' => 'hidden', '#default_value' => $fid);
    $form['field_name'] = array(
      '#type' => 'hidden',
      '#default_value' => $item['data']['properties']['image_field']['field_name'],
    );
  }

  $no_image = '';
  if (!empty($item['sku']) && isset($node->nid)) {
    $no_image = l(t('Select'), 'node/' . $node->nid . '/attributes/' . $item['sku'], array(
      'query' => array('tab' => 'image_field', 'destination' => $_GET['q'])));
  }

  $form['image'] = array('#markup' => isset($icon) ? $icon : $no_image);
  return $form;

}

function dcart_attribute_property_image_field_form_submit($form, &$form_state) {
  if(!empty($form_state['values']['properties']['image_field']['field'])) {
    $node = $form_state['node'];
    $field_name = $form_state['values']['properties']['image_field']['field'];

    $file_id = 0;
    if(isset($form_state['values']['properties']['image_field']['fid'])) {
      $file_id = $form_state['values']['properties']['image_field']['fid'];
    }

    $sku = $form_state['sku'];
    _dcart_attribute_property_image_field_save($sku, $file_id, $field_name);

    $cid = 'field:node:' . $form_state['node']->nid;
    cache_clear_all($cid, 'cache_field');
  }
}

function dcart_attribute_property_image_field_preprocess_field(&$variables) {
  $node = $variables['element']['#object'];
  if(isset($node->nid) && $attributes = dcart_product_attributes_get($node, NULL, NULL, $node->language)) { // Deal only with node entity
    foreach($attributes as $attribute) {
      if(isset($attribute['data']['properties']['image_field']['field'])) {
        $field_name = $attribute['data']['properties']['image_field']['field'];
        if(isset($variables['element']['#field_name']) && $variables['element']['#field_name'] == $field_name) {
          $variables['classes_array'][] = 'dcart-field-refreshable-' . $variables['field_name_css'];
          $default_item = array(reset($variables['items']));
          $variables['items'] = $default_item;
        }
      }
    }
  }
}

function _dcart_attribute_property_image_field_save($sku, $fid, $field_name) {
  if($fid == 0) {
    return db_delete('dcart_attribute_property_image_field')->condition('sku', $sku)->execute();
  }
  return db_merge('dcart_attribute_property_image_field')
    ->key(array('sku' => $sku))
    ->fields(array('fid' => $fid, 'field_name' => $field_name))
    ->execute();
}

<?php

function dcart_attribute_property_stock_set($attribute, $node = NULL) {
  $sku = $attribute['sku'];
  $level = $attribute['data']['properties']['stock']['level'];
  $save = ($level == '') ? dcart_stock_unset($sku) : dcart_stock_set($sku, $level);
}

function dcart_attribute_property_stock_get($attribute, $node = NULL) {
  return (array) dcart_stock_get($attribute['sku']);
}

function dcart_attribute_property_stock_form($context) {
  return array(
    'level' => array(
      '#title' => t('Stock level'),
      '#type' => 'textfield',
      '#size' => 3,
      '#default_value' => isset($context['attribute']['data']['properties']['stock']['level']) ? $context['attribute']['data']['properties']['stock']['level'] : '',
      '#maxlength' => 16,
    ),
  );
}

function dcart_attribute_property_stock_form_validate($form, $form_state) {
  if(isset($form_state['values']['properties']['stock']['level'])) {
    $level = trim($form_state['values']['properties']['stock']['level']);
    if ($level && !is_numeric($level)) {
      form_set_error('', t('Stock level should be either empty string or numeric value. If you left empty string, the SKU will not be taken into account in the stock.'));
    }
  }
}

function dcart_attribute_property_stock_form_submit($form, $form_state) {
  $level = $form_state['values']['properties']['stock']['level'];
  $sku = $form_state['sku'];
  $save = ($level == "") ? dcart_stock_unset($sku) : dcart_stock_set($sku, $level);
  
  // Clear field cache to see changes. We only clear cache for this node ID.
  $cid = 'field:node:' . $form_state['node']->nid;
  cache_clear_all($cid, 'cache_field');
}

function dcart_attribute_property_stock_widget_form($item, $node) {
  return array(
    'level' => array(
      '#title' => t('Stock level'),
      '#title_display' => 'invisible',
      '#type' => 'textfield',
      '#size' => 3,
      '#default_value' => isset($item['data']['properties']['stock']['level']) ? $item['data']['properties']['stock']['level'] : '',
    ),
  );
}

function dcart_attribute_property_stock_widget_form_validate($context, &$errors) {
  foreach ($context['items']['container']['attributes'] as $delta => $item) {
    if ($item['sku']) {
      if (!empty($item['data']['properties']['stock']['level'])) {
        $level = $item['data']['properties']['stock']['level'];
        if (!filter_var($level, FILTER_VALIDATE_INT)) {
          $errors[$context['field']['field_name']][$context['langcode']][$delta][] = array(
            'error' => "attribute_property:stock:level:$delta",
            'message' => t('Stock level should be integer.'),
          );
        }
      }
    }
  }
}
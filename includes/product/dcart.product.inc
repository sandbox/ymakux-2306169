<?php

function dcart_dcart_attribute_properties_info() {

  $path = drupal_get_path('module', 'dcart') . '/includes/product/attribute_properties';

  return array(
    'image_field' => array(
      'title' => t('Image'),
      'description' => t('Image field item that linked with the SKU'),
      'callback' => 'dcart_attribute_property_image_field',
      'widget' => TRUE,
      'full_object' => TRUE,
      'file' => $path . '/image_field.inc',
    ),
    'stock' => array(
      'title' => t('Stock level'),
      'description' => t('Stock level'),
      'callback' => 'dcart_attribute_property_stock',
      'file' => $path . '/stock.inc',
      'widget' => TRUE,
    ),
    'product_discount' => array(
      'title' => t('Discount'),
      'callback' => 'dcart_attribute_property_product_discount',
      'file' => $path . '/product_discount.inc',
      'widget' => TRUE,
    ),
  );
}

function dcart_attribute_properties() {
  $properties = &drupal_static(__FUNCTION__);
  if (!isset($properties)) {
    $properties = module_invoke_all('dcart_attribute_properties_info');
    drupal_alter('dcart_attribute_properties_info', $properties);
  }
  return $properties;
}

function dcart_attributes_combinations($data, &$all, $group = array(), $val = NULL, $i = 0){

  if (isset($val)){
    array_push($group, $val);
  }

  if ($i >= count($data)){
    array_push($all, $group);
  }
  else{
    foreach ($data[$i] as $v){
      dcart_attributes_combinations($data, $all, $group, $v, $i + 1);
    }
  }
  return $all;
}

function dcart_product_attributes_get($node, $sku = NULL, $delta = NULL) {
  
  $return = FALSE;
  if (is_numeric($node)) {
    $node = node_load($node);
  }

  if ($items = dcart_attribute_field($node)) {
    if (isset($sku)) {
      foreach ($items as $item) {
        if ($item['sku'] == $sku) {
          $return = $item;
          break;
        }
      }
    }
    elseif (isset($delta) && !empty($items[$delta])) {
      $return = $items[$delta];
    }
    else {
      $return = $items;
    }
  }

  return $return;
}

function dcart_product_price($node, $sku = NULL, $delta = NULL, $display = 'components') {
  $return = FALSE;
  if ($attribute = dcart_product_attributes_get($node, $sku, $delta)) {
    switch ($display) {
      case 'value' :
        $return = (int) $attribute['amount'];
      break;
      case 'components' :
        $return = array(
          'amount' => $attribute['amount'],
          'currency_code' => $attribute['currency_code']);
      break;
      case 'formatted' :
        $return = theme('dcart_price', array(
          'amount' => $attribute['amount'],
          'currency_code' => $attribute['currency_code']));
      break;
    }
  }
  return $return;
}

function dcart_product_access($node, $op, $account = NULL) {

  if (empty($node->dcart_product_attributes)) {
    return FALSE;
  }
  
  if (!is_object($account)) {
    global $user;
    $account = $user;
  }

  switch ($op) {
    case 'attributes' :
      if(!dcart_product_has_attribute_fields($node->type)) {
        return FALSE;
      }
      if (user_access('edit any ' . $node->type . ' content', $account) || (user_access('edit own ' . $node->type . ' content', $account) && ($account->uid == $node->uid))) {
        return TRUE;
      }
    break;
  }
  return FALSE;
}

function dcart_product_node_type_access($node_type, $operation) {
  if($operation == 'line_items') {
    if (dcart_is_product_node_type($node_type->type)) {
      return TRUE;
    }
  }
  return FALSE;
}

function dcart_product_has_attribute_fields($node_type) {
  foreach (field_info_instances('node', $node_type) as $field_name => $instance) {
    if(!empty($instance['dcart_settings']['attribute_field'])) {
      return TRUE;
    }
  }
  return FALSE;
}


function dcart_product_set_breadcrumb($node) {
  if(variable_get('dcart_product_set_breadcrumb', TRUE) && dcart_is_product_node_type($node->type)) {

    $cid = array('node_taxonomy_breadcrumb', $node->nid);
    if(isset($node->language)) {
      $cid[] = $node->language;
    }
    
    $cid = implode(':', $cid);
    if($cached = cache_get($cid, 'cache_dcart_catalog')) {
      $breadcrumbs = $cached->data;
    }
    
    if(empty($breadcrumbs)) {
      $breadcrumbs = array();
      $category_field_name = variable_get('dcart_catalog_category_field_name', '');
      if($category_field_name && !empty($node->{$category_field_name}[LANGUAGE_NONE][0]['tid'])) {
        $tid = $node->{$category_field_name}[LANGUAGE_NONE][0]['tid'];
        if($parents = taxonomy_get_parents($tid)) {
          foreach($parents as $parent) {
            $breadcrumbs[] = l(entity_label('taxonomy_term', $parent), 'taxonomy/term/' . $parent->tid);
          }
        }
      }
      
      if($breadcrumbs) {
        cache_set($cid, $breadcrumbs, 'cache_dcart_catalog');
      }
    }
    array_unshift($breadcrumbs, l(t('Home'), '<front>'));
    drupal_set_breadcrumb($breadcrumbs);
  }
}

function dcart_product_image_field_instance($bundle_name) {
  foreach(field_info_instances('node', $bundle_name) as $field_name => $field_data) {
    if(!empty($field_data['dcart_settings']['cart_icon'])) {
      return $field_data;
    }
  }
  return FALSE;
}

function dcart_product_image_field_name($bundle_name) {
  if($instance = dcart_product_image_field_instance($bundle_name)) {
    return $instance['field_name'];
  }
  return FALSE;
}

function dcart_product_attributes_form_validate($form, &$form_state) {
  $node = $form_state['node'];
  form_state_values_clean($form_state);
  unset($form_state['values'][DCART_ATTRIBUTES_FIELD_NAME][LANGUAGE_NONE]['add']);
  
  $options = array('field_name' => DCART_ATTRIBUTES_FIELD_NAME);
  field_attach_form_validate('node', $node, $form, $form_state, $options);
}

function dcart_product_attributes_form_submit($form, &$form_state) {
  $node = $form_state['node'];
  entity_form_submit_build_entity('node', $node, $form, $form_state);
  node_save($node);
}

function dcart_product_csv_mapping() {
  $columns = array();
  $columns['header'] = array(
    'Title',
    'SKU',
    'Price',
    'Currency',
    'Published',
    'Stock',
  );
  return $columns;
}

function dcart_product_attributes_set($data, $minor_units = TRUE) {

}


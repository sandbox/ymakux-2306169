<?php

function dcart_product_export_page() {
  if (!empty($_SESSION['csv_download_filename']) && !empty($_SESSION['csv_download_file'])) {
    $download_link = l(t('click here to download the file'), 'admin/dcart/products/export/download');
    return '<p>' . t('Your CSV file is now ready for download. !download_link.', array('!download_link' => $download_link)) . '</p>';
  }
  return drupal_get_form('dcart_product_export_form');
}

function dcart_product_export_form($form, $form_state) {
  $product_types = dcart_product_node_types();
  $options = array_intersect_key(node_type_get_names(), $product_types);

  $form['bundles'] = array(
    '#title' => t('Product types'),
    '#type' => 'checkboxes',
    '#options' => $options,
    '#default_value' => array_keys($options),
    '#ajax' => array(
      'wrapper' => 'taxonomy-wrapper',  
      'callback' => 'dcart_product_export_form_ajax',
    ),
  );

  $form['taxonomy'] = array(
   '#type' => 'container',
   '#attributes' => array('id' => 'taxonomy-wrapper'),
  );

  if(isset($form_state['values']['bundles'])) {
    $product_types = $form_state['values']['bundles'];
  } 

  $taxonomy_fields = array(0 => t('Any'));
  foreach(field_info_instances('node') as $bundle_name => $fields) {
    if(!empty($product_types[$bundle_name])) {
      foreach($fields as $field_name => $field_data) {
        $field_info = field_info_field($field_name);
        if($field_info['type'] == 'taxonomy_term_reference') {
          $taxonomy_fields[$field_name] = check_plain($field_data['label']);
        }
      }
    }
  }

  $taxomomy_field = key($taxonomy_fields);

  $form['taxonomy']['taxonomy'] = array(
    '#title' => t('Category field'),
    '#type' => 'select',
    '#options' => $taxonomy_fields,
    '#default_value' => $taxomomy_field,
    '#ajax' => array(
      'wrapper' => 'taxonomy-terms-wrapper',
      'callback' => 'dcart_product_export_form_terms_ajax',
    ),
  );

  $form['terms'] = array(
   '#type' => 'container',
   '#attributes' => array('id' => 'taxonomy-terms-wrapper'),
  );

  $terms = array(0 => t('Any'));
  if($taxonomy_fields) {
    if(isset($form_state['values']['taxonomy'])) {
      $taxomomy_field = $form_state['values']['taxonomy'];
    }
    if($taxomomy_field) {
      $terms = taxonomy_allowed_values(field_info_field($taxomomy_field));
    }
  }
  $form['terms']['terms'] = array(
    '#title' => t('Category'),
    '#type' => 'select',
    '#options' => $terms,
    '#default_value' => array(0),
    '#multiple' => TRUE,
    '#size' => 10,
  );

  $form['export'] = array(
    '#value' => t('Export to CSV'),
    '#type' => 'submit',
  );
  return $form;
}

function dcart_product_export_form_ajax($form, &$form_state) {
  return $form['taxonomy'];
}

function dcart_product_export_form_terms_ajax($form, &$form_state) {
  return $form['terms'];
}

function dcart_product_export_form_submit($form, $form_state) {

  $bundles = array_filter($form_state['values']['bundles']);
  $taxonomy_field = $form_state['values']['taxonomy'];
  $terms = array_filter($form_state['values']['terms']);

  if(!empty($bundles)) {
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', array_keys($bundles), 'IN');

    if($taxonomy_field && $terms) {
      $query->fieldCondition($taxonomy_field, 'tid', array_keys($terms));
    }

    $query->propertyOrderBy('title', 'DESC');
    $result = $query->execute();

    if(!empty($result['node'])) {
      $batch = array(
        'operations' => array(array('dcart_product_export_batch', array(array_keys($result['node'])))),
        'finished' => 'dcart_product_export_batch_finished',
        'file' => drupal_get_path('module', 'dcart') . '/includes/product/dcart.product.export.inc',
      );
      batch_set($batch);
      batch_process('admin/dcart/products/export');
    }
  }
}

function dcart_product_export_batch($nids, &$context) {
  
  $limit = 10;
  $context['finished'] = 0;
  if (!isset($context['sandbox']['file'])) {

    $mapping = dcart_product_csv_mapping();

    // Create the file and print the labels in the header row.
    $filename = 'product_export.csv';
    
    $file_path = file_directory_temp() . '/' . $filename;
    // Create the file.
    $handle = fopen($file_path, 'w'); 
    // Write the labels to the header row.
    fputcsv($handle, $mapping['header'], DCART_CSV_DELIMETER);
    fclose($handle);

    $context['sandbox']['file'] = $file_path;
    $context['sandbox']['rows'] = $nids;
    $context['sandbox']['rows_total'] = count($context['sandbox']['rows']);

    // Store some values in the results array for processing when finished.
    $context['results']['filename'] = $filename;
    $context['results']['file'] = $file_path;
  }

  // Accounting.
  if (!isset($context['results']['count'])) {
    $context['results']['count'] = 0;
  }

  // Loop until we hit the batch limit.
  for ($i = 0; $i < $limit; $i++) {
    $number_remaining = count($context['sandbox']['rows']);
    if ($number_remaining) {
      $node = node_load($context['sandbox']['rows'][$context['results']['count']]);
      if($attribute_items = dcart_attribute_field($node)) {
        $handle = fopen($context['sandbox']['file'], 'a');
        $stock = dcart_stock_level_get($attribute_items[0]['sku']);
        $amount = dcart_price_amount_to_decimal($attribute_items[0]['amount'], $attribute_items[0]['currency_code']);
        $row_data = array($node->title, $attribute_items[0]['sku'], $amount, $attribute_items[0]['currency_code'], intval($node->status), $stock);
        fputcsv($handle, $row_data, DCART_CSV_DELIMETER);
        fclose($handle);
        if(count($attribute_items) > 1) {
          foreach($attribute_items as $delta => $attribute_item) {
            if($delta > 0) {
              $stock = dcart_stock_level_get($attribute_item['sku']);
              $handle = fopen($context['sandbox']['file'], 'a');
              $amount = dcart_price_amount_to_decimal($attribute_item['amount'], $attribute_item['currency_code']);
              $attribute_data = array(DCART_CSV_ATTRIBUTE_CHAR, $attribute_item['sku'], $amount, $attribute_item['currency_code'], $stock);
              fputcsv($handle, $attribute_data, DCART_CSV_DELIMETER);
              fclose($handle);
            }
          }
        }
      }

      // Remove the row from $context.
      unset($context['sandbox']['rows'][$context['results']['count']]);

      // Increment the counter.
      $context['results']['count']++;
      $context['finished'] = $context['results']['count'] / $context['sandbox']['rows_total'];
    }
    // If there are no rows remaining, we're finished.
    else {
      $context['finished'] = 1;
      break;
    }
  }

  // Show message how many rows have been exported.
  $context['message'] = t('Exported @count of @total products.', array(
    '@count' => $context['results']['count'],
    '@total' => $context['sandbox']['rows_total'],
  ));
}

function dcart_product_export_batch_finished($success, $results, $operations) {
  if ($success) {
    $message = format_plural($results['count'], 'One product exported.', '@count products exported.');
    drupal_set_message($message);
  }
  else {
    $message = t('There were errors during the export of products.');
    drupal_set_message($message, 'warning');
  }

  // Set some session variables for the redirect to the file download page.
  $_SESSION['csv_download_file'] = $results['file'];
  $_SESSION['csv_download_filename'] = $results['filename'];
}

function dcart_product_export_download_csv_file() {
  if(empty($_SESSION['csv_download_filename']) || empty($_SESSION['csv_download_file'])) {
    return drupal_goto('admin/dcart/products/export');
  }
  if (strpos($_SESSION['csv_download_file'], file_directory_temp()) !== 0) {
    return MENU_ACCESS_DENIED;
  }

  drupal_add_http_header('Content-Type', 'text/csv; utf-8');
  drupal_add_http_header('Content-Disposition', 'attachment; filename=' . $_SESSION['csv_download_filename'], TRUE);

  // Allow caching, otherwise IE users can't dl over SSL (see issue #294).
  drupal_add_http_header('Cache-Control', 'max-age=300; must-revalidate');

  // Read the file to the output buffer and exit.
  readfile($_SESSION['csv_download_file']);
  unset($_SESSION['csv_download_filename'], $_SESSION['csv_download_file']);
  exit;
}



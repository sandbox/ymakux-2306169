<?php

function dcart_dcart_line_item_type_info() {
  $path = drupal_get_path('module', 'dcart') . '/includes/line_item/callbacks';
  return array(
    'shipping' => array(
      'title' => t('Shipping'),
      'description' => t('Shipping'),
      'callback' => 'dcart_line_item_type_shipping',
      'file' => $path . '/shipping.inc',
    ),
    'payment' => array(
      'title' => t('Payment'),
      'description' => t('Payment method'),
      'callback' => 'dcart_line_item_type_payment',
      'file' => $path . '/payment.inc',
    ),
    'tax' => array(
      'title' => t('Tax'),
      'description' => t('Tax'),
      'callback' => 'dcart_line_item_type_tax',
      'file' => $path . '/tax.inc',
    ),
  );
}

function dcart_line_item_types() {
  $line_item_types = &drupal_static(__FUNCTION__);
  if (!isset($line_item_types)) {
    $line_item_types = module_invoke_all('dcart_line_item_type_info');
    
    $settings = dcart_line_item_type_settings();
    
    foreach($line_item_types as $id => &$line_item_type) {
      
      if(empty($line_item_type['name'])) {
        $line_item_type['name'] = $id;
      }
      
      if(!isset($line_item_type['weight'])) {
        $line_item_type['weight'] = 0;
      }
      
      if(!isset($line_item_type['active'])) {
        $line_item_type['active'] = TRUE;
      }
      
      if(isset($settings[$id])) {
        $line_item_type = $settings[$id] + $line_item_type;
      }
    }
    
    drupal_alter('dcart_line_item_type_info', $line_item_types);
    uasort($line_item_types, 'drupal_sort_weight');
  }
  return $line_item_types;
}

function dcart_line_item_types_get($line_item = NULL) {
  $line_item_types = dcart_line_item_types();
  if(isset($line_item)) {
    return isset($line_item_types[$line_item]) ? $line_item_types[$line_item] : array();
  }
  return $line_item_types;
}

function dcart_line_item_type_options() {
  $options = array();
  foreach (dcart_line_item_types_get() as $id => $line_item_type) {
    $options[$id] = $line_item_type['title'];
  }
  return $options;
}

function dcart_line_item_type_settings($line_item_type = NULL) {
  $settings = variable_get('dcart_line_item_type_settings', array());
  if(isset($line_item_type)) {
    return isset($settings[$line_item_type]) ? $settings[$line_item_type] : array();
  }
  return $settings;
}


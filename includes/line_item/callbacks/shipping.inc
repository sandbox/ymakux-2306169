<?php

function dcart_line_item_type_shipping_calculate($service_id, $context) {
  $shipping_service = dcart_shipping_services_get($service_id);
  if ($rate_callback = dcart_hook_callback($shipping_service, 'rate')) {
    return (int) $rate_callback();
  }
}

function dcart_line_item_type_shipping_widget_form($form, $form_state, $order) {
  $form = array();
  $form['value'] = array(
    '#title' => t('Shipping service'),
    '#type' => 'select',
    '#options' => dcart_shipping_service_options(NULL, TRUE),
    '#empty_value' => 0,
    '#default_value' => isset($order->data['line_items']['shipping']['value']) ? $order->data['line_items']['shipping']['value'] : 0,
  ); 
  return $form;
}

function dcart_line_item_type_shipping_widget_form_validate($form, &$form_state) {
  $trigger = $form_state['triggering_element']['#parents'];
  if((isset($trigger[1]) && $trigger[1] == 'line_item') && (isset($trigger[2]) && $trigger[2] == 'shipping')) {
    if(empty($form_state['values']['add']['line_item']['shipping']['value'])) {
      form_error($form['add']['line_item']['shipping']['value'], t('Please choose a shipping method'));
    }
  }
}

function dcart_line_item_type_shipping_widget_form_submit($form, &$form_state) {
  $sipping_service = $form_state['values']['add']['line_item']['shipping']['value'];
  if($rate_callback = dcart_hook_callback(dcart_shipping_load($sipping_service), 'rate')) {
    $amount = $rate_callback();
    if(isset($amount)) {
      $form_state['storage']['dcart_order']['add']['line_items']['shipping']['amount'] = $amount;
      $form_state['storage']['dcart_order']['add']['line_items']['shipping']['value'] = $sipping_service;
    }
  }
}

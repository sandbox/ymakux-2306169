<?php

function dcart_line_item_type_payment_calculate($payment_method, $context) {
  $amount = 0;
  $settings = dcart_payment_method_settings($payment_method);
  
  if(isset($settings['amount'])) {
    $amount = (int) $settings['amount'];
    $currency_code = $settings['currency_code'];
    if($currency_code != dcart_default_currency()) {
      $amount = dcart_currency_convert($amount, $currency_code);
    }
  }
  return $amount;
}

function dcart_line_item_type_payment_widget_form($form, $form_state, $order) {
  $form = array();
  $form['value'] = array(
    '#title' => t('Payment method'),
    '#type' => 'select',
    '#options' => dcart_payment_method_options(FALSE, FALSE),
    '#empty_value' => 0,
    '#default_value' => isset($order->data['line_items']['payment']['value']) ? $order->data['line_items']['payment']['value'] : 0,
  );
  return $form;
}

function dcart_line_item_type_payment_widget_form_validate($form, &$form_state) {
 $trigger = $form_state['triggering_element']['#parents'];
  if((isset($trigger[1]) && $trigger[1] == 'line_item') && (isset($trigger[2]) && $trigger[2] == 'payment')) {
    if(empty($form_state['values']['add']['line_item']['payment']['value'])) {
      form_error($form['add']['line_item']['payment']['value'], t('Please choose a payment method'));
    }
  }
}

function dcart_line_item_type_payment_widget_form_submit($form, &$form_state) {
  $payment_method = $form_state['values']['add']['line_item']['payment']['value'];
  if($rate_callback = dcart_hook_callback(dcart_payment_load($payment_method), 'rate')) {
    $amount = $rate_callback();
    if(isset($amount)) {
      $form_state['storage']['dcart_order']['add']['line_items']['payment']['amount'] = $amount;
      $form_state['storage']['dcart_order']['add']['line_items']['payment']['value'] = $payment_method;
    }
  }
}

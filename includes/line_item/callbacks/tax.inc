<?php

function dcart_line_item_type_tax_calculate($line_item_name, $context) {
  $amount = 0;
  $line_item = dcart_line_item_types_get($line_item_name);
  if(isset($line_item['amount'])) {
    $amount = (int) $line_item['amount'];
    $currency_code = $line_item['currency_code'];
    if($currency_code != dcart_default_currency()) {
      $amount = dcart_currency_convert($amount, $currency_code);
    }
  }
  return $amount;
}

function dcart_line_item_type_tax_form($context) {
  $form = array();
  $form['amount'] = array(
    '#title' => t('Amount, @type', array('@type' => '%')),
    '#type' => 'textfield',
    '#size' => 3,
    '#default_value' => isset($context['amount']) ? $context['amount'] : '',
    '#element_validate' => array('dcart_numeric_form_validate'),
    '#required' => TRUE,
  );
  
  $form['currency_code'] = array(
    '#title' => t('Currency'),
    '#type' => 'select',
    '#options' => dcart_currency_options(TRUE),
    '#default_value' => isset($line_item['currency_code']) ? $line_item['currency_code'] : dcart_default_currency(),
    '#description' => t('Choose a currency from <a href="@url">enabled currencies</a>', array('@url' => url('admin/dcart/settings/currency'))),
  );
  
  $form['operator'] = array(
    '#title' => t('Operator'),
    '#type' => 'select',
    '#options' => drupal_map_assoc(array('+', '-')),
    '#default_value' => isset($context['data']['operator']) ? $context['data']['operator'] : '',
    '#required' => TRUE,
  );
  return $form;
}

function dcart_line_item_type_tax_form_validate($form, &$form_state) {
  $element = $form_state['values'][DCART_LINE_ITEM_FIELD_NAME][LANGUAGE_NONE];
  $data = $element['data'];
  
  $form_state['storage']['line_items'][] = array(
    'amount' => dcart_price_decimal_to_amount($data['amount']),
    'type' => $element['type'],
    'operator' => $data['operator'],
  );
  $form_state['rebuild'] = TRUE;
}
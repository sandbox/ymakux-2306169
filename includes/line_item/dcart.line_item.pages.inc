<?php

function dcart_line_items_form($form, $form_state) {

  $form['#tree'] = TRUE;
  $settings = dcart_line_item_type_settings();
  $form_state['line_item_types'] = $line_item_types = dcart_line_item_types_get();
  uasort($line_item_types, 'drupal_sort_weight');

  foreach ($line_item_types as $name => $info) {

    
    $form['line_items'][$name]['title'] = array(
      '#markup' => $info['title'],
    );
    
    $form['line_items'][$name]['active'] = array(
      '#markup' => $info['active'] ? t('Active') : t('Inactive'),
    );

    $form['line_items'][$name]['operations']['settings'] = array(
      '#type' => 'link',
      '#title' => t('Settings'),
      '#href' => 'admin/dcart/settings/order/line_items/' . $name,
      '#options' => array('query' => array('destination' => current_path())),
    );
        
    $form['line_items'][$name]['weight'] = array(
      '#type' => 'weight',
      '#delta' => 10,
      '#attributes' => array('class' => array('line-item-weight')),
      '#default_value' => $line_item_types[$name]['weight'],
    );
  }
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  
  return $form;
}

function dcart_line_items_form_submit($form, $form_state) {
  $settings = array();
  foreach ($form_state['values']['line_items'] as $name => $info) {
    $settings[$name] = $info + dcart_line_item_type_settings($name);
  }
  
  variable_set('dcart_line_item_type_settings', $settings);
  drupal_set_message(t('The configuration options have been saved.'));
}

function dcart_line_item_settings_form($form, $form_state, $line_item) {

  $form_state['line_item_type'] = $line_item;
  $form_state['line_item_types'] = $line_item_types = dcart_line_item_types();
  $line_item_info = isset($line_item_types[$line_item]) ? $line_item_types[$line_item] : array();
  $form_state['line_item_info'] = $line_item_info;
  
  $settings = dcart_line_item_type_settings($line_item);
  $form['settings'] = array(
    '#type' => 'container',
    '#weight' => 99,
    '#tree' => TRUE,
  );
  
  if ($form_function = dcart_hook_callback($line_item_info, 'settings_form')) {
    if($elements = $form_function($settings)) {
      $form['settings'] += $elements;
    }
  }

  $form['active'] = array(
    '#title' => t('Active'),
    '#type' => 'checkbox',
    '#default_value' => isset($settings['active']) ? $settings['active'] : $line_item_info['active'],
  );

  $form['actions'] = array(
    '#type' => 'actions',
  );
  
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

function dcart_line_item_settings_form_validate($form, &$form_state) {
  form_state_values_clean($form_state);
  if ($validate_function = dcart_hook_callback($form_state['line_item_info'], 'settings_form_validate')) {
    $validate_function($form, $form_state);
  }
}

function dcart_line_item_settings_form_submit($form, $form_state) {
  
  $line_item = $form_state['line_item_type'];
  $existing_settings = dcart_line_item_type_settings();

  $existing_line_item_settings = isset($existing_settings[$line_item]) ? $existing_settings[$line_item] : array();
  $existing_settings[$line_item] = $form_state['values'] + $existing_line_item_settings;
  
  if ($submit_function = dcart_hook_callback($form_state['line_item_info'], 'settings_form_submit')) {
    $submit_function($form, $form_state);
  }

  variable_set('dcart_line_item_type_settings', $existing_settings);
  drupal_set_message(t('The configuration options have been saved.'));
}

function theme_dcart_line_items_form($variables) {

  $form = $variables['form'];
  $header = array(t('Line item'), t('Active'), t('Weight'), t('Operations'));

  $rows = array();
  foreach(element_children($form['line_items']) as $element) {
    $rows[] = array(
      'data' => array(
        render($form['line_items'][$element]['title']),
        render($form['line_items'][$element]['active']),
        render($form['line_items'][$element]['weight']),
        render($form['line_items'][$element]['operations']),
      ),
      'class' => array('draggable'),
    );
  }
  
  $table_id = 'order-line-items';
  drupal_add_tabledrag($table_id, 'order', 'sibling', 'line-item-weight');
  
  $output = theme('table', array(
    'rows' => $rows,
    'header' => $header,
    'empty' => t('No line items.'),
    'attributes' => array('id' => $table_id),
  ));

  return $output . drupal_render_children($form);
}
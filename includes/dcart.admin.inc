<?php

function dcart_store_settings_form($form, $form_state) {
  $form['address'] = array(
    '#title' => t('Address'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  
  $form['address']['dcart_address_city'] = array(
    '#title' => t('City'),
    '#type' => 'textfield',
    '#default_value' => variable_get('dcart_address_city', ''),
  );
  
  $form['address']['dcart_address_street'] = array(
    '#title' => t('Street address'),
    '#type' => 'textfield',
    '#default_value' => variable_get('dcart_address_street', ''),
  );
  
  $form['address']['dcart_address_building'] = array(
    '#title' => t('Building'),
    '#type' => 'textfield',
    '#default_value' => variable_get('dcart_address_building', ''),
  );
  
  $form['address']['dcart_address_office'] = array(
    '#title' => t('Office'),
    '#type' => 'textfield',
    '#default_value' => variable_get('dcart_address_office', ''),
  );
  
  $form['address']['dcart_address_email'] = array(
    '#title' => t('Email'),
    '#type' => 'textfield',
    '#default_value' => variable_get('dcart_address_email', variable_get('site_mail', ini_get('sendmail_from'))),
  ); 
  
  $form['address']['dcart_address_phone'] = array(
    '#title' => t('Phone'),
    '#type' => 'textfield',
    '#default_value' => variable_get('dcart_address_phone', ''),
  );
  
  $form['address']['dcart_address_fax'] = array(
    '#title' => t('Fax'),
    '#type' => 'textfield',
    '#default_value' => variable_get('dcart_address_fax', ''),
  );
  
  $form['address']['dcart_address_zip'] = array(
    '#title' => t('Postal code / ZIP Code'),
    '#type' => 'textfield',
    '#default_value' => variable_get('dcart_address_zip', ''),
  );
  
  return system_settings_form($form);
}
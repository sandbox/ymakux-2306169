(function ($) {
  Drupal.behaviors.dCart = {
    attach: function (context, settings) {
      $('form#checkout-form').addClass('js-enabled');
      $(document).ajaxStart(function(){
        $(this).find('input[type="submit"]').attr('disabled', 'disabled').addClass('wait');
      }).ajaxSuccess(function(){
        $(this).find('input[type="submit"]').removeAttr('disabled').removeClass('wait');
      });
      
      $('input[type="submit"]').click(function(){
        if($(this).hasClass('disabled')) {
          return false;
        }
      });

      $('.checkout-pane.checkout_shipping_method input:checked, .checkout-pane.checkout_shipping_address input:checked, .checkout-pane.checkout_payment_method input:checked').next('label').addClass('active');
      $('.checkout-pane.checkout_shipping_method input, .checkout-pane.checkout_shipping_address input, .checkout-pane.checkout_payment_method input').live('change', function(){
        $(this).closest('.form-radios').find('label').removeClass('active');
        if($(this).is(':checked')) {
          $(this).next('label').addClass('active');
        }
      });

      var loading = '<div class="loading">' + Drupal.t('Loading...') + '</div>';

      if ($.fn.autocomplete) {
        var searchingMessage = '<div class="searching-status" style="position:absolute;">' + Drupal.t('Searching...') + '</div>';
        $('input:text#product-sku-autocomplete, input:text#product-title-autocomplete').live('focus', function(e) {
          $(this).each(function(){
            var path = $(this).attr('id') == 'product-sku-autocomplete' ? 'product-by-sku' : 'product-by-title'; 
            $(this).autocomplete({
              search: function( event, ui) {
                $("div.searching-status").remove();
                $(this).after(searchingMessage);
              },
              source: function(request, response) {
                $.ajax({
                  url: settings.basePath + 'operations/autocomplete/' + path,
                  dataType: "json",
                  data: {
                    term : request.term,
                  },
                  success: function(data) {
                    response(data);
                    $("div.searching-status").remove();
                  },
                });
              },
              select: function(event, ui) {
                $("div.searching-status").remove();
                $('input:text#product-sku-autocomplete').val(ui.item.sku);
                $('input:text#product-title-autocomplete').val(ui.item.title);
                return false;
              },
            });
          
            if ($(this).data()) {
              var ac = $(this).data('autocomplete');
              if (ac) {
                ac._renderItem = function(ul, item) {
                  var append = '';
                  if (path == 'product-by-sku') {
                    append = '<a><span class="autocomplete-item-wrapper">' + Drupal.t('SKU: @sku, node: @title', {'@sku': item.sku, '@title': item.title}) + '</span></a>';
                  }
                  else{
                    append = '<a><span class="autocomplete-item-wrapper">' + Drupal.t('node: @title, SKU: @sku', {'@sku': item.sku, '@title': item.title}) + '</span></a>';
                  }
                  return $('<li></li>').data('item.autocomplete', item).append(append).appendTo(ul);
                };
              }
            }
          });
        });
      }
      
      if ($.fn.datepicker) {
        $('input.datepicker-min, input.datepicker-max, input#datepicker').datepicker({dateFormat: 'mm/dd/yy'});
      }
  
    $('#purchase-order').after('<a href="#" class="print-order"><i class="fa fa-print"></i>' + Drupal.t('Print') + '</a>');
    $('a.print-order').click(function(){
      $("#purchase-order").printElement({
        printMode:"popup",
        overrideElementCSS:[settings.basePath + 'sites/all/modules/dcart/dcart.css'],
      });
      return false;
    });
      
      if ($.fn.dialog) {
        $('a.open-dialog').click(function(){
          var dialog = $(loading).appendTo('body');
          var height = $(this).data('dialog-height') ? $(this).data('dialog-height') : 500;
          var width = $(this).data('dialog-width') ? $(this).data('dialog-width') : 800;
          
          dialog.dialog({
            close: function(event, ui) {
              dialog.remove();
            },
            height: height,
            width: width,
            title: $(this).attr('title'),
          });
        
          dialog.load(
            this.href, 
            {},
            function (responseText, textStatus, XMLHttpRequest) {
              dialog.removeClass('loading');
            }
          );
          return false;
        });
      }
      
      $('fieldset#edit-panes-checkout-total #checkout-calculate').hide();

      if($.fn.drupalSetSummary) {
        $('fieldset.attribute-price-sku', context).drupalSetSummary(function (context) {
          var amount = $('input.amount', context).val();
          var sku = $('input.attribute-sku');
          var vals = [];
          if(amount == "") {
            vals.push(Drupal.t('Price') + ': <span style="color:red;">' + Drupal.t('Not set') + '</span>');
          }
          else if(isNaN(amount)) {
            vals.push(Drupal.t('Price') + ': <span style="color:red;">' + Drupal.t('Invalid value') + '</span>');
          }
          else {
            vals.push(Drupal.t('Price') + ': ' + amount);
          }
          
          if(sku.is(':disabled')) {
            vals.push(Drupal.t('Automatic SKU generation is on'));
          }
          else {
            vals.push(Drupal.t('SKU: @sku', {'@sku' : sku.val()}));
          }
          return vals.join("<br />");
        });
    
       $('fieldset.attribute-properties.image_field', context).drupalSetSummary(function (context) {
         var radio = $('.image-radios .option input:checked');
         if(radio.val()) {
          var src = radio.closest('div').find('img').attr('src');
          return '<img src="' + src + '" height="32px" width="32px" />';
        }
      });
    
      $('fieldset.attribute-fields', context).drupalSetSummary(function (context) {
        var vals = [];
        $('.form-type-radios').each(function(){
          var radio = $(this).find('input:checked');
          if(radio.val()) {
            vals.push($('<div>').append(radio.next('label').eq(0).clone()).html());
          }
        });
        return vals.join("<br />");
       });
      }
    }
  }
})(jQuery);





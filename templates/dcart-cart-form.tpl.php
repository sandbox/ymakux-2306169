<?php if(empty($form['cart'])): ?>
  <?php print theme('dcart_empty_cart_page'); ?>
<?php else: ?>
  
  <?php
    $index = 1;
    $cart_items = element_children($form['cart']);
    foreach($cart_items as $id): ?>
    
    <?php
      $class = '';
      if(count($cart_items) == $index) {
        $class = ' last';
      }
      elseif($index == 1) {
        $class = ' first';
      }
    ?>
    
    <div class="row<?php print $class; ?>">
    <div class="image">
      <?php print render($form['cart'][$id]['image']); ?>
    </div>
    <div class="product">
      <div class="title item">
      <?php print render($form['cart'][$id]['title']); ?>
      </div>
      
      <div class="sku item">
      <?php print t('Item #!sku', array('!sku' => theme('dcart_sku', array('sku' => $form['cart'][$id]['attribute']['#value']['sku'])))); ?>
      </div>

      <div class="attributes item">
        <?php print render($form['cart'][$id]['attributes']); ?>
        <div style="clear:both;"></div>
      </div>

      <div class="price item">
      <?php print t('Unit price: !price', array('!price' => render($form['cart'][$id]['price']))); ?>
      </div>

      <div class="quantity item">
      <div class="label"><?php print t('Quantity'); ?> </div>
        <?php print render($form['cart'][$id]['count']); ?>

        <div class="remove">
          <?php print render($form['cart'][$id]['operations']['remove']); ?>
        </div>
      </div>
      <div class="wishlist">
        <?php print render($form['cart'][$id]['operations']['wishlist']); ?>
      </div>
      <div class="amount item">
      <?php print t('Total: !total', array('!total' => render($form['cart'][$id]['amount']))); ?>
      </div>
    </div>
    </div>
    
  <?php $index++; endforeach; ?>
  
  <?php print theme('pager'); ?>
  
  <div class="subtotal">
  <?php print t('Subtotal: !num', array('!num' => render($form['subtotal']))); ?>
  </div>

  <div class="actions">
  <div class="continue"><?php print l(t('Continue shopping'), '<front>'); ?></div>
  <div class="buttons">
    <?php print render($form['recalculate']); ?><?php print render($form['checkout']); ?>
  </div>
  </div>
<?php endif; ?>
<?php print drupal_render_children($form); ?>

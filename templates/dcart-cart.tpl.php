<?php if(empty($items)): ?>
  <?php print $empty; ?>
<?php else: ?>
  <div id="cart">
    <table>  
    <?php foreach($items as $item): ?>
      <tr>
        <td><div class="title"><?php print render($item['title']); ?></div></td>
        <td>
          <?php print t('!price X @count = !total', array('!price' => render($item['price']), '@count' => $item['count'], '!total' => render($item['total'])));?>
        </td>
      </tr>
    <?php endforeach; ?>
    </table>
  </div>
  <div class="product-total">
  <?php print t('Product total: !total', array('!total' => render($total))); ?>
  </div>
<?php endif; ?>
<div id="purchase-order">
<table class="info">
  <tr class="summary">
    <td class="company">
      <div class="logo"><?php print $site_logo; ?></div>
      <div class="name"><?php print $site_name; ?></div>
      <div class="slogan"><?php print $site_slogan; ?></div>
      <div class="address"><?php print $address; ?></div>
    </td>
    <td class="order">
    <div class="title"><?php print t('Order @order_id', array('@order_id' => $order_id)); ?></div>
      <table class="summary">
        <tbody>
          <tr class="created">
            <td class="label"><?php print t('Date'); ?>:</td>
            <td class="value"><?php print $created; ?></td>
          </tr>
          <tr class="number">
            <td class="label"><?php print t('Order #'); ?></td>
            <td class="value"><?php print $order_id; ?></td>
          </tr>
          <tr class="status">
            <td class="label"><?php print t('Order status'); ?></td>
            <td class="value"><?php print $order_status; ?></td>
          </tr>
        </tbody>
      </table>
    </td>
  </tr>
  <tr class="shipping">
    <td class="from">
      <div class="header"><?php print t('Ship from'); ?></div>
      <div class="address"><?php print $address; ?></div>
    </td>
    <td class="to">
      <div class="header"><?php print t('Ship to'); ?></div>
      <div class="address">
        <table class="ship-to">
          <?php foreach($shipping_address as $address): ?>
          <tr>
            <td><?php print $address['title']; ?></td>
            <td><?php print $address['value']; ?></td>
          </tr>
          <?php endforeach; ?>
        </table>
      </div>
    </td>
  </tr>
  <tr class="items">
    <td colspan="2">
      <table class="products">
        <thead>
          <tr>
            <th class="sku"><?php print t('Item #'); ?></th>
            <th class="title"><?php print t('Description'); ?></th>
            <th class="count"><?php print t('Count'); ?></th>
            <th class="price"><?php print t('Price'); ?></th>
            <th class="total"><?php print t('Total'); ?></th>
          </tr>
        </thead>
        <tbody>
          <?php foreach($order_items as $key => $item): ?>
          <?php $type = is_numeric($key) ? 'product' : 'line-item'; ?>
          <tr class="<?php print $type; ?> item-<?php print $key; ?>">
            <?php if($type == 'product'): ?>
            <td class="sku"><?php print $item['sku']; ?></td>
            <td class="title"><?php print $item['title']; ?></td>
            <td class="count"><?php print $item['count']; ?></td>
            <td class="price"><?php print $item['price']; ?></td>
            <td class="total"><?php print $item['total']; ?></td>
            <?php else: ?>
            <td colspan="3"></td>
            <td class="title"><?php print $item['title']; ?></td>
            <td class="value"><?php print $item['amount']; ?></td>          
            <?php endif; ?>
          </tr>
         <?php endforeach; ?>
        </tbody>
      </table> 
    </td>
  </tr>
  <?php if(!empty($order_extra)): ?>
  <tr class="extra">
   <td colspan="2">
     <h3><?php print t('Additional notes'); ?></h3>
     <?php print render($order_extra); ?>
   </td>
  </tr>
  <?php endif; ?>
</table>
</div>
<div id="login-continue">
  <div id="login">
    <h2><?php print t('Returning guest'); ?></h2>
    <?php print render($login); ?>
  </div>
  <div id="continue">
  <h2><?php print t('New guest'); ?></h2>
  <?php print $text; ?>
  <?php print render($continue); ?>
  </div>
</div>
<?php if(empty($rows)): ?>
  <?php print $empty; ?>
<?php else: ?>
  <div id="wishlist">
    <?php foreach($rows as $row): ?>
    <div class="row">
      <div class="image"><?php print render($row['image']); ?></div>
      <div class="product">
       <div class="title">
          <?php print $row['title']; ?>
       </div>

       <div class="attributes">
         <?php print render($row['attributes']); ?>
       </div>
       <?php if(!empty($row['add_to_cart']) || !empty($row['remove'])): ?>
         <div class="actions">
           <?php if(!empty($row['add_to_cart'])): ?>
             <?php print render($row['add_to_cart']); ?>
           <?php endif; ?>
           <?php if(!empty($row['remove'])): ?>
             <?php print render($row['remove']); ?>
           <?php endif; ?>
        </div>
      <?php endif; ?>
    </div>
  </div>
  <?php endforeach; ?>
  </div>
  <?php print theme('pager'); ?>
<?php endif; ?>
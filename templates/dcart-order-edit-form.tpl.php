<table id="order-edit-form">
  <tr>
    <td><?php print render($status); ?></td>
    <td colspan="4"><h1><?php print render($total); ?></h1></td>
  </tr>

  <tr>
    <td colspan="4"><?php print render($products); ?></td>
  </tr>
  <tr>
    <td colspan="4"><?php print render($line_items); ?></td>
  </tr>
  
  <tr>
    <td colspan="4"><?php print render($update); ?></td>
  </tr>

  <tr>
    <td colspan="4"><?php print render($add); ?></td>
  </tr>
  


  <tr>
    <td><?php print render($creator); ?></td>
    <td><?php print render($created); ?></td>
    <td><?php print render($owner); ?></td>
    <td><?php print render($mail); ?></td>
  </tr>
  
  <tr>
    <td colspan="4"><?php print render($fields); ?></td>
  </tr>
</table>

<?php print render($send_mail); ?>
<?php print render($buttons); ?>
<?php print drupal_render_children($form); ?>

